/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import com.gs.gapp.metamodel.csharp.type.CSharpEnum;
import com.gs.gapp.metamodel.csharp.type.CSharpType;



/**
 * An enum entry can be part of an enum.
 *
 * @author mmt
 *
 */
public class CSharpEnumEntry extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = -4078841436685745938L;

	/**
	 * @param name
	 * @param owner
	 */
	public CSharpEnumEntry(String name, CSharpType owner) {
		super(name, owner, owner);

		if (owner instanceof CSharpEnum) {
			CSharpEnum csharpEnum = (CSharpEnum) owner;
			csharpEnum.addEntries(this);
		} else {
			throw new RuntimeException("Invalid owner type '" + owner.getClass() + "' used for the creation of an enum entry.");
		}
	}
}
