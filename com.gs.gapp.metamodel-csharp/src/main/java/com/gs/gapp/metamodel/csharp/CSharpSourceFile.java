package com.gs.gapp.metamodel.csharp;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.type.CSharpModelElement;
import com.gs.gapp.metamodel.csharp.type.CSharpUsingDirective;

public class CSharpSourceFile extends CSharpModelElement {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7491026586810503870L;
	
	private final String path;
	
	private final Set<CSharpUsingDirective> usingDirectives = new LinkedHashSet<CSharpUsingDirective>();
	
	private final Set<CSharpNamespaceUsage> namespaceUsages = new LinkedHashSet<CSharpNamespaceUsage>();
	
	public CSharpSourceFile(String name, String path) {
		super(name);
		this.path = path;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @return the usingDirectives
	 */
	public Set<CSharpUsingDirective> getUsingDirectives() {
		return usingDirectives;
	}

	/**
	 * @param usingDirectives
	 */
	public void addUsingDirectives(CSharpUsingDirective... usingDirectives) {
		if (usingDirectives != null) {
			this.usingDirectives.addAll(Arrays.asList(usingDirectives));
		}
	}

	/**
	 * @return the namespaceUsages
	 */
	public Set<CSharpNamespaceUsage> getNamespaceUsages() {
		return namespaceUsages;
	}
	
	/**
	 * @return
	 */
	public CSharpNamespaceUsage getFirstNamespaceUsage() {
		if (this.namespaceUsages.size() > 0) {
			return this.namespaceUsages.iterator().next();
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public Set<CSharpNamespaceUsage> getRemainingNamespaceUsages() {
		if (this.namespaceUsages.size() > 1) {
			Set<CSharpNamespaceUsage> remainingNamespaceUsages = null;
			for (CSharpNamespaceUsage aUsage : this.namespaceUsages) {
				if (remainingNamespaceUsages != null) {
					remainingNamespaceUsages.add(aUsage);
				} else {
					// ignore the first namespace usage and create the set object instead
					remainingNamespaceUsages = new LinkedHashSet<CSharpNamespaceUsage>();
				}
			}
			return remainingNamespaceUsages;
		}
		
		return new LinkedHashSet<CSharpNamespaceUsage>();
	}
	
	/**
	 * @param namespaceUsages
	 */
	public void addNamespaceUsages(CSharpNamespaceUsage... namespaceUsages) {
		if (namespaceUsages != null) {
			this.namespaceUsages.addAll(Arrays.asList(namespaceUsages));
		}
	}
	
	/**
	 * @return
	 */
	public CSharpNamespaceUsage getGlobalNamespaceUsage() {
		for (CSharpNamespaceUsage namespaceUsage : this.namespaceUsages) {
			if (CSharpNamespace.GLOBAL_NAMESPACE_NAME.equals(namespaceUsage.getNamespace().getName())) {
				return namespaceUsage;
			}
		}
		return null;
	}
	
	

}
