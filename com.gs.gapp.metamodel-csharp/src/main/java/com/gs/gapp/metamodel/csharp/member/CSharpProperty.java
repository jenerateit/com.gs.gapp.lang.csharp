/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.type.CSharpType;



/**
 * Properties can be used with interfaces, classes or structs.
 *
 * http://msdn.microsoft.com/en-us/library/x9fsa0sw.aspx
 *
 * @author mmt
 *
 */
public class CSharpProperty extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = -5758654823313113803L;


	private CSharpModifier accessModifierForGet;
	private CSharpModifier accessModifierForSet;
    private CSharpField relatedField;
    private boolean autoImplemented = false;
	private boolean readOnly = false;

	/**
	 * @param name
	 * @param owner
	 * @param type
	 */
	public CSharpProperty(String name, CSharpType type, CSharpType owner) {
		super(name, type, owner, (CSharpParameter[])null);

		validateOwner(owner);

		owner.addProperties(this);
	}

	private void validateOwner(CSharpType owner) {
		// TODO
//		if (owner instanceof CSharpInterface) {
//			throw new RuntimeException("type '" + owner.getName() + "' must not be the owner of a constructor");
//		}
	}

	/**
	 * An explicit access modifier for the getter can be specified.
	 *
	 * http://msdn.microsoft.com/en-us/library/75e8y5dd.aspx
	 *
	 * @return the accessModifierForGet
	 */
	public CSharpModifier getAccessModifierForGet() {
		return accessModifierForGet;
	}


	/**
	 * @param accessModifierForGet the accessModifierForGet to set
	 */
	public void setAccessModifierForGet(CSharpModifier accessModifierForGet) {
		this.accessModifierForGet = accessModifierForGet;
	}


	/**
	 * An explicit access modifier for the setter can be specified.
	 *
	 * http://msdn.microsoft.com/en-us/library/75e8y5dd.aspx
	 *
	 * @return the accessModifierForSet
	 */
	public CSharpModifier getAccessModifierForSet() {
		return accessModifierForSet;
	}


	/**
	 * @param accessModifierForSet the accessModifierForSet to set
	 */
	public void setAccessModifierForSet(CSharpModifier accessModifierForSet) {
		this.accessModifierForSet = accessModifierForSet;
	}


	/**
	 * @return the relatedField
	 */
	public CSharpField getRelatedField() {
		return relatedField;
	}


	/**
	 * @param relatedField the relatedField to set
	 */
	public void setRelatedField(CSharpField relatedField) {
		this.relatedField = relatedField;
	}


	/**
	 * If a property is auto-implemented, a private, anonymous backing field is created by the compiler.
	 * By default this flag is set to 'false'.
	 * http://msdn.microsoft.com/en-us/library/bb384054.aspx
	 *
	 * @return the autoImplemented
	 */
	public boolean isAutoImplemented() {
		return autoImplemented;
	}


	/**
	 * @param autoImplemented the autoImplemented to set
	 */
	public void setAutoImplemented(boolean autoImplemented) {
		this.autoImplemented = autoImplemented;
	}


	/**
	 * @return the readOnly
	 */
	public boolean isReadOnly() {
		return readOnly;
	}


	/**
	 * @param readOnly the readOnly to set
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
}
