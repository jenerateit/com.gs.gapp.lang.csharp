/**
 * 
 */
package com.gs.gapp.metamodel.csharp;

import com.gs.gapp.metamodel.basic.ModelElementCache;

/**
 * The CSharpModelElementCache is a facade to the basic ModelElementCache.
 * @author mmt
 *
 */
public class CSharpModelElementCache extends ModelElementCache {


	/**
	 * 
	 */
	public CSharpModelElementCache() {
		super();
	}
}
