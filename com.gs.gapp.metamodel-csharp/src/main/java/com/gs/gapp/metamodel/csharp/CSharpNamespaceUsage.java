/**
 *
 */
package com.gs.gapp.metamodel.csharp;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.type.CSharpModelElement;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.CSharpUsingDirective;

/**
 * @author mmt
 *
 */
public class CSharpNamespaceUsage extends CSharpModelElement {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5713633113307093864L;
	
	private final Set<CSharpUsingDirective> usingDirectives = new LinkedHashSet<CSharpUsingDirective>();
	
	private final CSharpNamespaceUsage owner;
	
	private final CSharpNamespace namespace;
	
	private final Set<CSharpNamespaceUsage> nestedNamespaceUsages = new LinkedHashSet<CSharpNamespaceUsage>();

	private final Set<CSharpType> types = new LinkedHashSet<CSharpType>();
	
	private final CSharpSourceFile sourceFile;
	
	/**
	 * @param namespace
	 * @param owner
	 */
	public CSharpNamespaceUsage(CSharpNamespace namespace, CSharpNamespaceUsage owner) {
		super(namespace == null ? null : namespace.getName());
		
		if (namespace == null) throw new NullPointerException("parameter 'namespace' must not be null");
		if (owner == null) throw new NullPointerException("parameter 'owner' must not be null");
		
		this.namespace = namespace;
		this.sourceFile = null;
		this.owner = owner;
		
		owner.addNestedNamespaceUsages(this);
	}
	
	/**
	 * @param namespace
	 * @param sourceFile
	 */
	public CSharpNamespaceUsage(CSharpNamespace namespace, CSharpSourceFile sourceFile) {
        super(namespace == null ? null : namespace.getName());
		
		if (namespace == null) throw new NullPointerException("parameter 'namespace' must not be null");
		if (sourceFile == null) throw new NullPointerException("parameter 'sourceFile' must not be null");
		
		this.namespace = namespace;
		this.sourceFile = sourceFile;
		this.owner = null;
		
		sourceFile.addNamespaceUsages(this);
	}

	/**
	 * @return the namespace
	 */
	public CSharpNamespace getNamespace() {
		return namespace;
	}

	/**
	 * @return the nestedNamespaceUsages
	 */
	public Set<CSharpNamespaceUsage> getNestedNamespaceUsages() {
		return nestedNamespaceUsages;
	}
	
	/**
	 * @param nestedNamespaceUsages
	 */
	public void addNestedNamespaceUsages(CSharpNamespaceUsage... nestedNamespaceUsages) {
		if (nestedNamespaceUsages != null) {
			this.nestedNamespaceUsages.addAll(Arrays.asList(nestedNamespaceUsages));
		}
	}

	/**
	 * @return the types
	 */
	public Set<CSharpType> getTypes() {
		return types;
	}
	
	/**
	 * @return
	 */
	public CSharpType getFirstType() {
		if (this.types.size() > 0) {
			return this.types.iterator().next();
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public Set<CSharpType> getRemainingTypes() {
		if (this.types.size() > 1) {
			Set<CSharpType> remainingTypes = null;
			for (CSharpType type : this.types) {
				if (remainingTypes != null) {
					remainingTypes.add(type);
				} else {
					// ignore the first namespace usage and create the set object instead
					remainingTypes = new LinkedHashSet<CSharpType>();
				}
			}
			return remainingTypes;
		}
		
		return new LinkedHashSet<CSharpType>();
	}

	/**
	 * Using this method implicitely adds the types to the namespace-usage's types collection.
	 * 
	 * @param types
	 */
	public void addTypes(CSharpType... types) {
		if (types != null) {
			this.types.addAll(Arrays.asList(types));
            this.namespace.addTypes(types);
		}
	}

	/**
	 * @return the usingDirectives
	 */
	public Set<CSharpUsingDirective> getUsingDirectives() {
		return usingDirectives;
	}

	/**
	 * @param usingDirectives
	 */
	public void addUsingDirectives(CSharpUsingDirective... usingDirectives) {
		if (usingDirectives != null) {
			this.usingDirectives.addAll(Arrays.asList(usingDirectives));
		}
	}

	/**
	 * @return the sourceFile
	 */
	public CSharpSourceFile getSourceFile() {
		return sourceFile;
	}

	/**
	 * @return the owner
	 */
	public CSharpNamespaceUsage getOwner() {
		return owner;
	}
}
