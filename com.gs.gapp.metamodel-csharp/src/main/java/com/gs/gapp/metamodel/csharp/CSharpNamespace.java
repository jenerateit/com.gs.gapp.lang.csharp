/**
 *
 */
package com.gs.gapp.metamodel.csharp;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.type.CSharpModelElement;
import com.gs.gapp.metamodel.csharp.type.CSharpType;

/**
 * @author mmt
 *
 */
public class CSharpNamespace extends CSharpModelElement {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5713633113307093864L;

	public static final String GLOBAL_NAMESPACE_NAME = "_GLOBAL_";
	public static final CSharpNamespace GLOBAL_NAMESPACE = new CSharpNamespace(GLOBAL_NAMESPACE_NAME, null);
	
	private final CSharpNamespace namespace;

	private final Set<CSharpType> types = new LinkedHashSet<CSharpType>();
	
	
	public CSharpNamespace(String name, CSharpNamespace namespace) {
		super(name);
		this.namespace = namespace;
	}
	
	/**
	 * @param delimiter
	 * @return
	 */
	public String getQualifiedName(String delimiter) {
		if (delimiter == null) { throw new NullPointerException("Parameter delimiter must not be null"); }
		if (delimiter.length() == 0) { throw new IllegalArgumentException("Parameter delimiter has zero length"); }

		StringBuilder result = new StringBuilder(this == GLOBAL_NAMESPACE ? "" : getName());
		CSharpNamespace nsp = this.getNamespace();

		while (nsp != null) {
			result.insert(0, delimiter).insert(0, nsp.getName());
			nsp = nsp.getNamespace();
		}

		return result.toString();
	}

	/**
	 * @return the namespace
	 */
	public CSharpNamespace getNamespace() {
		return namespace;
	}

	/**
	 * @return the types
	 */
	public Set<CSharpType> getTypes() {
		return types;
	}

	/**
	 * @param types
	 */
	public void addTypes(CSharpType... types) {
		if (types != null) {
			this.types.addAll(Arrays.asList(types));
		}
	}
}
