/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;



/**
 * Operators can be used in classes.
 *
 * http://msdn.microsoft.com/en-us/library/8edha89s.aspx
 *
 * @author mmt
 *
 */
public class CSharpOperator extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = 5827759298211907581L;

	/**
	 * @param name the operator (often this is a symbol like '+', '-', etc.)
	 * @param owner
	 * @param operatorType
	 */
	public CSharpOperator(String name, CSharpType owner,
			OperatorType operatorType) {
		super(name, owner, owner, getOperatorParameters(operatorType, owner));

        validateOwner(owner);

		owner.addOperators(this);
	}

	private void validateOwner(CSharpType owner) {
		if (owner instanceof CSharpInterface) {
			throw new RuntimeException("type '" + owner.getName() + "' must not be the owner of an operator");
		}
	}

	private static CSharpParameter[] getOperatorParameters(
			OperatorType operatorType, CSharpType type) {
		if (operatorType == null) throw new NullPointerException("parameter 'operatorType' must not be null");

		CSharpParameter p1 = null;
		CSharpParameter p2 = null;

		switch (operatorType) {
		case BINARY:
			p1 = new CSharpParameter("p1", type, (CSharpParameterAttribute[])null);
			p2 = new CSharpParameter("p2", type, (CSharpParameterAttribute[])null);
			return new CSharpParameter[] {p1, p2};
		case UNARY:
			p1 = new CSharpParameter("p1", type, (CSharpParameterAttribute[])null);
			return new CSharpParameter[] {p1};
		default:
			throw new RuntimeException("unhandled operator type found:" + operatorType);
		}
	}

	/**
	 * @author mmt
	 *
	 */
	public enum OperatorType {
		UNARY,
		BINARY,
		;
	}
}
