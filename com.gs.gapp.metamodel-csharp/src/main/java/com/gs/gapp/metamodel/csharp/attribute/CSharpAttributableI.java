package com.gs.gapp.metamodel.csharp.attribute;

import java.util.Set;

public interface CSharpAttributableI {
	
	Set<CSharpAttributeUsage> getAttributeUsages();
	void addAttributeUsages(CSharpAttributeUsage... attributes);
	
}
