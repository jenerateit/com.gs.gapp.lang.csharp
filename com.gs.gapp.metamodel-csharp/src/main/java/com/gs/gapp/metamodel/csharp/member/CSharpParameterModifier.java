/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.csharp.member;


/**
 * @author hrr
 *
 */
public enum CSharpParameterModifier {
	
	NONE	(""),
	PARAMS	("params"),
	REF		("ref"),
	OUT		("out"),
	;
	
	private String name;
	
	private CSharpParameterModifier(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}
}
