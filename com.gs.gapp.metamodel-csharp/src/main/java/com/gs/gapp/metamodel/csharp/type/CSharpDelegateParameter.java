/**
 *
 */
package com.gs.gapp.metamodel.csharp.type;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributableI;
import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributeUsage;
import com.gs.gapp.metamodel.csharp.member.CSharpParameterAttribute;

/**
 * @author mmt
 *
 */
public class CSharpDelegateParameter extends CSharpModelElement implements CSharpAttributableI {

	/**
	 *
	 */
	private static final long serialVersionUID = -1844543606001152702L;

	private final CSharpType type;
	private CSharpType owner;
	private String defaultValue;
	private final Set<CSharpParameterAttribute> parameterAttributes = new LinkedHashSet<CSharpParameterAttribute>();
	private final Set<CSharpAttributeUsage> attributes = new LinkedHashSet<CSharpAttributeUsage>();

	/**
	 * @param name
	 * @param type
	 * @param parameterAttributes
	 */
	public CSharpDelegateParameter(String name, CSharpType type, CSharpParameterAttribute... parameterAttributes) {
		super(name);
		this.type = type;
		if (parameterAttributes != null) {
			this.parameterAttributes.addAll(Arrays.asList(parameterAttributes));
		}
	}

	/**
	 * @return the attributes
	 */
	public Set<CSharpAttributeUsage> getAttributeUsages() {
		return attributes;
	}

	/**
	 * @param attributes
	 */
	public void addAttributeUsages(CSharpAttributeUsage... attributes) {
		if (attributes != null) {
		    this.attributes.addAll(Arrays.asList(attributes));
		}
	}

	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the parameterAttributes
	 */
	public Set<CSharpParameterAttribute> getParameterAttributes() {
		return parameterAttributes;
	}

	void setOwner(CSharpType owner) {
		if (this.owner != null && owner == null) throw new IllegalArgumentException("an existing owner of this parameter must not be set to null");
		if (this.owner != null && this.owner.equals(owner) == false) throw new IllegalArgumentException("an existing owner of this parameter must not be set to a different owner");
		this.owner = owner;
	}

	/**
	 * @return the owner
	 */
	public CSharpType getOwner() {
		return owner;
	}

	/**
	 * @return the type
	 */
	public CSharpType getType() {
		return type;
	}
}
