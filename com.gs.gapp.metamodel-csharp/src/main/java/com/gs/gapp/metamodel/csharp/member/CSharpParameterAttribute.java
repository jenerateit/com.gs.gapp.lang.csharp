package com.gs.gapp.metamodel.csharp.member;

public enum CSharpParameterAttribute {

	HAS_DEFAULT ("HasDefault"),
	HAS_FIELD_MARSHAL ("HasFieldMarshal"),
	IN ("In"),
	LCID ("Lcid"),
	NONE ("None"),
	OPTIONAL ("Optional"),
	OUT ("Out"),
	RESERVED_MASK ("ReservedMask"),
	RETVAL ("Retval"),
	;
	
	private final String name;
	
	private CSharpParameterAttribute(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
