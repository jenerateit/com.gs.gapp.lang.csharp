/**
 *
 */
package com.gs.gapp.metamodel.csharp.type;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;

/**
 * @author mmt
 *
 */
public class CSharpUsingDirective extends CSharpModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = -44583598922620164L;

	private final CSharpNamespace namespace;
    private final CSharpNamespace aliasedNamespace;
    private final CSharpType aliasedType;
    private final String rawUsingDirective;

	public CSharpUsingDirective(String rawUsingDirective) {
		super(rawUsingDirective);

		if (rawUsingDirective == null || rawUsingDirective.length() == 0) throw new IllegalArgumentException("parameter 'rawUsingDirective' must not be null and have a length > 0");

		this.rawUsingDirective = rawUsingDirective;
		this.namespace = null;
		this.aliasedNamespace = null;
		this.aliasedType = null;
	}

	public CSharpUsingDirective(CSharpNamespace namespace) {
		super(namespace == null ? null : namespace.getQualifiedName("."));

		if (namespace == null) throw new NullPointerException("parameter 'namespace' must not be null");

		this.rawUsingDirective = null;
		this.namespace = namespace;
		this.aliasedNamespace = null;
		this.aliasedType = null;
	}

	public CSharpUsingDirective(String name, CSharpNamespace aliasedNamespace) {
		super(name);

		if (aliasedNamespace == null) throw new NullPointerException("parameter 'aliasedNamespace' must not be null");

		this.rawUsingDirective = null;
		this.namespace = null;
		this.aliasedNamespace = aliasedNamespace;
		this.aliasedType = null;
	}

	public CSharpUsingDirective(String name, CSharpType aliasedType) {
		super(name);

		if (aliasedType == null) throw new NullPointerException("parameter 'aliasedType' must not be null");

		this.rawUsingDirective = null;
		this.namespace = null;
		this.aliasedNamespace = null;
		this.aliasedType = aliasedType;
	}

	/**
	 * @return the namespace
	 */
	public CSharpNamespace getNamespace() {
		return namespace;
	}

	/**
	 * @return the aliasedNamespace
	 */
	public CSharpNamespace getAliasedNamespace() {
		return aliasedNamespace;
	}

	/**
	 * @return the aliasedType
	 */
	public CSharpType getAliasedType() {
		return aliasedType;
	}

	/**
	 * @return the rawUsingDirective
	 */
	public String getRawUsingDirective() {
		return rawUsingDirective;
	}

	/**
	 * @return
	 */
	public String getDirectiveAsString() {
		if (this.rawUsingDirective != null && this.rawUsingDirective.length() > 0) {
			return this.rawUsingDirective;
		} else if (this.namespace != null) {
			return this.namespace.getQualifiedName(".");
		} else if (this.aliasedNamespace != null) {
			return this.getName() + " = " + this.aliasedNamespace.getQualifiedName(".");
		} else if (this.aliasedType != null) {
			return this.getName() + " = " + this.aliasedType.getQualifiedName(".");
		} else {
			return null;
		}
	}
}
