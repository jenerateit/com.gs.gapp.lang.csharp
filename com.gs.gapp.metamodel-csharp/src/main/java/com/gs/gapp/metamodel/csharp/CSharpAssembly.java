/**
 *
 */
package com.gs.gapp.metamodel.csharp;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributableI;
import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributeUsage;
import com.gs.gapp.metamodel.csharp.type.CSharpModelElement;
import com.gs.gapp.metamodel.csharp.type.CSharpType;

/**
 * @author mmt
 *
 */
public class CSharpAssembly extends CSharpModelElement implements CSharpAttributableI {

	/**
	 *
	 */
	private static final long serialVersionUID = -44583598922620164L;

	private final Set<CSharpType> types = new LinkedHashSet<CSharpType>();
	private final Set<CSharpAttributeUsage> attributes = new LinkedHashSet<CSharpAttributeUsage>();

	public CSharpAssembly(String name) {
		super(name);
	}

	/**
	 * @return the attributes
	 */
	public Set<CSharpAttributeUsage> getAttributeUsages() {
		return attributes;
	}

	/**
	 * @param attributes
	 */
	public void addAttributeUsages(CSharpAttributeUsage... attributes) {
		if (attributes != null) {
		    this.attributes.addAll(Arrays.asList(attributes));
		}
	}

	/**
	 * @return the types
	 */
	public Set<CSharpType> getTypes() {
		return types;
	}

	/**
	 * @param types
	 */
	public void addTypes(CSharpType ...types) {
		if (types != null) {
			this.types.addAll(Arrays.asList(types));
		}
	}
}
