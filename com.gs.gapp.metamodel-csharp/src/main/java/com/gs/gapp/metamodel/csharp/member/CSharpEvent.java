/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import com.gs.gapp.metamodel.csharp.type.CSharpType;



/**
 * Events can be used in interfaces and classes.
 *
 * @author mmt
 *
 */
public class CSharpEvent extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = -5376364660064265245L;

	/**
	 * @param name
	 * @param owner
	 * @param type
	 */
	public CSharpEvent(String name, CSharpType type, CSharpType owner) {
		super(name, type, owner, (CSharpParameter[])null);

		validateOwner(owner);

		owner.addEvents(this);
	}

	private void validateOwner(CSharpType owner) {
		// TODO add validation to check for an allowed owner type (type needs to be a CSharpDelegate)
//		if (owner instanceof CSharpInterface) {
//			throw new RuntimeException("type '" + owner.getClass() + "' must not be the owner of a constructor");
//		}
	}
}
