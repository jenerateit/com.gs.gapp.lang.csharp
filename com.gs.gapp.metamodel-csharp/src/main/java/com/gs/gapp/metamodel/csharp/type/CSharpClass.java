/**
 *
 */
package com.gs.gapp.metamodel.csharp.type;

import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;
import com.gs.gapp.metamodel.csharp.member.CSharpConstructor;
import com.gs.gapp.metamodel.csharp.member.CSharpDestructor;
import com.gs.gapp.metamodel.csharp.member.CSharpEvent;
import com.gs.gapp.metamodel.csharp.member.CSharpField;
import com.gs.gapp.metamodel.csharp.member.CSharpIndexer;
import com.gs.gapp.metamodel.csharp.member.CSharpMethod;
import com.gs.gapp.metamodel.csharp.member.CSharpOperator;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;


/**
 * @author mmt
 *
 */
public class CSharpClass extends CSharpReferenceType {

	/**
	 *
	 */
	private static final long serialVersionUID = 6611533100165715466L;
	
	/**
	 * Use this constructor when the type is a predefined type that is not going to be written to a source file
	 * by the codegeneration (isGenerated() == false).
	 * 
	 * @param name
	 * @param namespace
	 */
	public CSharpClass(String name, CSharpNamespace namespace) {
		super(name, namespace);
	}
	
	

	
	/**
	 * @param name
	 * @param namespaceUsage
	 */
	public CSharpClass(String name, CSharpNamespaceUsage namespaceUsage) {
		super(name, namespaceUsage);
	}




	/**
	 * @param name
	 * @param owningType
	 */
	public CSharpClass(String name, CSharpType owningType) {
		super(name, owningType);
	}




	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getConstructors()
	 */
	@Override
	public Set<CSharpConstructor> getConstructors() {
		return super.getConstructors();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addConstructors(com.gs.gapp.metamodel.csharp.member.CSharpConstructor[])
	 */
	@Override
	public void addConstructors(CSharpConstructor... constructors) {
		super.addConstructors(constructors);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getDestructor()
	 */
	@Override
	public CSharpDestructor getDestructor() {
		return super.getDestructor();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#setDestructor(com.gs.gapp.metamodel.csharp.member.CSharpDestructor)
	 */
	@Override
	public void setDestructor(CSharpDestructor destructor) {
		super.setDestructor(destructor);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getEvents()
	 */
	@Override
	public Set<CSharpEvent> getEvents() {
		return super.getEvents();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addEvents(com.gs.gapp.metamodel.csharp.member.CSharpEvent[])
	 */
	@Override
	public void addEvents(CSharpEvent... events) {
		super.addEvents(events);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getFields()
	 */
	@Override
	public Set<CSharpField> getFields() {
		return super.getFields();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addFields(com.gs.gapp.metamodel.csharp.member.CSharpField[])
	 */
	@Override
	public void addFields(CSharpField... fields) {
		super.addFields(fields);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getIndexers()
	 */
	@Override
	public Set<CSharpIndexer> getIndexers() {
		return super.getIndexers();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addIndexers(com.gs.gapp.metamodel.csharp.member.CSharpIndexer[])
	 */
	@Override
	public void addIndexers(CSharpIndexer... indexers) {
		super.addIndexers(indexers);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getMethods()
	 */
	@Override
	public Set<CSharpMethod> getMethods() {
		return super.getMethods();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addMethods(com.gs.gapp.metamodel.csharp.member.CSharpMethod[])
	 */
	@Override
	public void addMethods(CSharpMethod... methods) {
		super.addMethods(methods);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getOperators()
	 */
	@Override
	public Set<CSharpOperator> getOperators() {
		return super.getOperators();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addOperators(com.gs.gapp.metamodel.csharp.member.CSharpOperator[])
	 */
	@Override
	public void addOperators(CSharpOperator... operators) {
		super.addOperators(operators);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getProperties()
	 */
	@Override
	public Set<CSharpProperty> getProperties() {
		return super.getProperties();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addProperties(com.gs.gapp.metamodel.csharp.member.CSharpProperty[])
	 */
	@Override
	public void addProperties(CSharpProperty... properties) {
		super.addProperties(properties);
	}
	
}
