package com.gs.gapp.metamodel.csharp.type.generics;

import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.member.CSharpEvent;
import com.gs.gapp.metamodel.csharp.member.CSharpMethod;
import com.gs.gapp.metamodel.csharp.type.CSharpType;

public class CSharpTypeParameter extends CSharpType {

    /**
	 *
	 */
	private static final long serialVersionUID = 7898422613987258018L;

	private final Set<CSharpType> genericParameterConstraints = new LinkedHashSet<CSharpType>();
	private final Set<CSharpGenericParameterAttributes> genericParameterAttributes;
	private CSharpMethod method;
	private CSharpEvent event;

	/**
	 * @param name
	 * @param owner
	 * @param genericParameterConstraints
	 * @param genericParameterAttributes
	 */
	public CSharpTypeParameter(String name, CSharpType owner, Set<CSharpType> genericParameterConstraints, Set<CSharpGenericParameterAttributes> genericParameterAttributes) {
		super(name, owner);

		if (genericParameterConstraints != null) {
		    this.genericParameterConstraints.addAll(genericParameterConstraints);
		}

		this.genericParameterAttributes = genericParameterAttributes == null ? EnumSet.of(CSharpGenericParameterAttributes.NONE) : genericParameterAttributes;
		this.method = null;
		this.event = null;

		owner.addGenericArguments(this);
	}

	/**
	 * @param name
	 * @param owner
	 */
	public CSharpTypeParameter(String name, CSharpType owner) {
		this(name, owner, null, null);
	}

	/**
	 * @return the genericParameterConstraints
	 */
	public Set<CSharpType> getGenericParameterConstraints() {
		return genericParameterConstraints;
	}


	/**
	 * @return the genericParameterAttributes
	 */
	public Set<CSharpGenericParameterAttributes> getGenericParameterAttributes() {
		return genericParameterAttributes;
	}

	/**
	 * @return the method
	 */
	public CSharpMethod getMethod() {
		return method;
	}

	/**
	 * @param method the method to set
	 */
	public void setMethod(CSharpMethod method) {
		if (this.method != null && this.method != method) {
			throw new RuntimeException("once a type parameter has a method set, that method must neither be replaced nor be set to null:" + method);
		}
		this.method = method;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#isGenericParameter()
	 */
	@Override
	public boolean isGenericParameter() {
    	return true;
    }


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#isGenericType()
	 */
	@Override
	public boolean isGenericType() {
		return false;
	}

	/**
	 * @return the event
	 */
	public CSharpEvent getEvent() {
		return event;
	}

	/**
	 * @param event the event to set
	 */
	public void setEvent(CSharpEvent event) {
		if (this.event != null && this.event != event) {
			throw new RuntimeException("once a type parameter has an event set, that event must neither be replaced nor be set to null:" + event);
		}
		this.event = event;
	}

}
