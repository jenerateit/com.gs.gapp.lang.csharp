/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;



/**
 * Methods can be used in classes, structs or interfaces.
 *
 * http://msdn.microsoft.com/en-us/library/ms173114.aspx
 *
 * @author mmt
 *
 */
public class CSharpMethod extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = 7639685239378599440L;

	private boolean extensionMethod = false;

	/**
	 * TODO include generic arguments in equals() and hashCode() because of method overloading: http://msdn.microsoft.com/en-us/library/twcad0zb.aspx
	 */
	private final Set<CSharpTypeParameter> genericArguments = new LinkedHashSet<CSharpTypeParameter>();
	
	private StringBuilder methodBody = new StringBuilder();

	/**
	 * @param name
	 * @param owner
	 * @param genericArguments
	 * @param parameters
	 */
	public CSharpMethod(String name, CSharpType type, CSharpType owner, Set<CSharpTypeParameter> genericArguments,
			CSharpParameter... parameters) {
		super(name, type, owner, parameters);

		if (genericArguments != null) {
			// we need to set the method first before we add the generic argument to the collection: the method field is relevant for equals() and hashCode()
			for (CSharpTypeParameter genericArgument : genericArguments) {
				genericArgument.setMethod(this);
			}
		    this.genericArguments.addAll(genericArguments);
		}

		validateOwner(owner);

		owner.addMethods(this);
	}

	/**
	 * @param name
	 * @param type
	 * @param owner
	 * @param parameters
	 */
	public CSharpMethod(String name, CSharpType type, CSharpType owner, CSharpParameter... parameters) {
		this(name, type, owner, null, parameters);

	}

	private void validateOwner(CSharpType owner) {
		// TODO
//		if (owner instanceof CSharpInterface) {
//			throw new RuntimeException("type '" + owner + "' must not be the owner of a constructor");
//		}
	}

	/**
	 * @return the genericArguments
	 */
	public Set<CSharpTypeParameter> getGenericArguments() {
		return genericArguments;
	}

	/**
	 * A C# extension method has the 'this' keyword preceeding the first parameter declaration.
	 * http://msdn.microsoft.com/en-us/library/bb383977.aspx
	 *
	 * @return the extensionMethod
	 */
	public boolean isExtensionMethod() {
		return extensionMethod;
	}

	/**
	 * @param extensionMethod the extensionMethod to set
	 */
	public void setExtensionMethod(boolean extensionMethod) {
		if (extensionMethod == true && this.isInInterface()) {
			throw new RuntimeException("an extension method must not be declared inside an interface (method:" + this + ")");
		}
		this.extensionMethod = extensionMethod;
	}

	/**
	 * @return
	 */
	public String getGenericTypeDeclaration() {
		if (genericArguments.size() > 0) {
			StringBuilder sb = new StringBuilder();
			String comma = "";
			for (CSharpTypeParameter typeParameter : genericArguments) {
	            sb.append(comma).append(typeParameter.getTypeAsString());
			}

			return sb.toString();
		}
		return null;
	}

	/**
	 * @return
	 */
	public String getMethodBody() {
		return methodBody.toString();
	}

	/**
	 * @param methodBody
	 */
	public void setMethodBody(String methodBody) {
		this.methodBody.setLength(0);
		this.methodBody.append(methodBody);
	}
	
	/**
	 * @param lineOfCode
	 */
	public void addToMethodBody(String lineOfCode) {
		this.methodBody.append(System.getProperty("line.separator"));
		if (lineOfCode != null && lineOfCode.length() > 0) {
		    this.methodBody.append(lineOfCode);
		}
	}
}
