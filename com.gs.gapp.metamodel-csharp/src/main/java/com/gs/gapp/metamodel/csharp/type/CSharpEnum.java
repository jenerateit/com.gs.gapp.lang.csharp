package com.gs.gapp.metamodel.csharp.type;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;
import com.gs.gapp.metamodel.csharp.member.CSharpEnumEntry;

public class CSharpEnum extends CSharpValueType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1831104300181475234L;

	private final Set<CSharpEnumEntry> entries = new LinkedHashSet<CSharpEnumEntry>();
	
	/**
	 * Use this constructor when the type is a predefined type that is not going to be written to a source file
	 * by the codegeneration (isGenerated() == false).
	 * 
	 * @param name
	 * @param namespace
	 */
	public CSharpEnum(String name, CSharpNamespace namespace) {
		super(name, namespace);
	}

	/**
	 * @param name
	 * @param namespaceUsage
	 */
	public CSharpEnum(String name, CSharpNamespaceUsage namespaceUsage) {
		super(name, namespaceUsage);
	}

	/**
	 * @param name
	 * @param owningType
	 */
	public CSharpEnum(String name, CSharpType owningType) {
		super(name, owningType);
	}

	/**
	 * @return
	 */
	public Set<CSharpEnumEntry> getEntries() {
		return entries;
	}
	
	/**
	 * @param entries
	 */
	public void addEntries(CSharpEnumEntry ...entries) {
		if (entries != null) {
			verifyMembers(entries);
			this.entries.addAll(Arrays.asList(entries));
		}
	}
}
