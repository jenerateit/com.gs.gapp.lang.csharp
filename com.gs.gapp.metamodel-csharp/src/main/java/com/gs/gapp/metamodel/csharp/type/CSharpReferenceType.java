package com.gs.gapp.metamodel.csharp.type;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;

public abstract class CSharpReferenceType extends CSharpType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2229547298546676327L;

	/**
	 * @param name
	 * @param namespace
	 */
	public CSharpReferenceType(String name, CSharpNamespace namespace) {
		super(name, namespace);
	}

	/**
	 * @param name
	 * @param namespaceUsage
	 */
	public CSharpReferenceType(String name,
			CSharpNamespaceUsage namespaceUsage) {
		super(name, namespaceUsage);
	}

	/**
	 * @param name
	 * @param owningType
	 */
	public CSharpReferenceType(String name, CSharpType owningType) {
		super(name, owningType);
	}
}
