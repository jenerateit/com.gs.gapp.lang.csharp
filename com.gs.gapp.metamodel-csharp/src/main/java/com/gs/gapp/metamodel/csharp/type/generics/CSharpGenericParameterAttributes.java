package com.gs.gapp.metamodel.csharp.type.generics;

import java.util.EnumSet;
import java.util.Set;

/**
 * http://msdn.microsoft.com/en-us/library/system.reflection.genericparameterattributes.aspx
 * 
 * @author mmt
 *
 */
public enum CSharpGenericParameterAttributes {
	
	CONTRAVARIANT,
	COVARIANT,
	DEFAULT_CONSTRUCTOR_CONSTRAINT,
	NONE,
	NOT_NULLABLE_VALUE_TYPE_CONSTRAINT,
	REFERENCE_TYPE_CONSTRAINT,
	;
	
	private final static Set<CSharpGenericParameterAttributes> specialConstraintMask;
	private final static Set<CSharpGenericParameterAttributes> varianceMask;
	
	static {
		specialConstraintMask = EnumSet.of(DEFAULT_CONSTRUCTOR_CONSTRAINT, REFERENCE_TYPE_CONSTRAINT, NOT_NULLABLE_VALUE_TYPE_CONSTRAINT);
		varianceMask = EnumSet.of(CONTRAVARIANT, COVARIANT);
	}
	
	/**
	 * @return
	 */
	public boolean hasVariance() {
		return varianceMask.contains(this);
	}
	
	/**
	 * @return
	 */
	public boolean hasSpecialConstraints() {
		return specialConstraintMask.contains(this);
	}
	

}
