package com.gs.gapp.metamodel.csharp;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.csharp.member.CSharpField;
import com.gs.gapp.metamodel.csharp.member.CSharpMethod;
import com.gs.gapp.metamodel.csharp.member.CSharpParameter;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpConstructedGenericType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;

public class CSharpModelElementCreator {

	private final Map<BoundTypeKey,CSharpConstructedGenericType> constructedGenericTypeCache =
			new LinkedHashMap<BoundTypeKey,CSharpConstructedGenericType>();

	/**
	 * @param type
	 * @param boundTypes
	 * @return
	 */
	public CSharpConstructedGenericType makeGenericType(CSharpType type, CSharpType... boundTypes) {
		CSharpConstructedGenericType result = null;

		if (type.isGenericTypeDefinition()) {
			synchronized (constructedGenericTypeCache) {

				BoundTypeKey key = new BoundTypeKey(type, boundTypes);
				result = constructedGenericTypeCache.get(key);

				if (result == null) {
					if (type.getOwningType() == null) {
						result = new CSharpConstructedGenericType(type, type.getNamespace(), boundTypes);
					} else {
						result = new CSharpConstructedGenericType(type, type.getOwningType(), boundTypes);
					}
					constructedGenericTypeCache.put(key, result);
				}
		    }
		}
		return result;
	}

	public static CSharpNamespace convertToNamespaceObject(String nsString) {
		CSharpNamespace result = null;
		String[] nsArr = nsString.split("[.]");
		for (String nsPart : nsArr) {
			result = new CSharpNamespace(nsPart, result);
		}
		return result;
	}

	/**
	 * @param field
	 * @return
	 */
	public CSharpProperty convertToProperty(CSharpField field) {
		CSharpProperty result = new CSharpProperty(StringTools.firstUpperCase(field.getName()), field.getType(), field.getOwner());
		result.setRelatedField(field);
		return result;
	}

	/**
	 * @param name
	 * @param namespace
	 * @return
	 */
	public CSharpSourceFile createClassSourceFile(String name, CSharpNamespace namespace) {
		CSharpClass csClass = new CSharpClass(name, createNamespaceUsage(name, namespace));
		return csClass.getNamespaceUsage().getSourceFile();
	}

	/**
	 * @param name
	 * @param namespace
	 * @return
	 */
	public CSharpSourceFile createInterfaceSourceFile(String name, CSharpNamespace namespace) {
		CSharpInterface csInterface = new CSharpInterface(name, createNamespaceUsage(name, namespace));
		return csInterface.getNamespaceUsage().getSourceFile();
	}

	/**
	 * @param name
	 * @param namespace
	 * @return
	 */
	public CSharpSourceFile createEnumSourceFile(String name, CSharpNamespace namespace) {
		CSharpEnum csEnum = new CSharpEnum(name, createNamespaceUsage(name, namespace));
		return csEnum.getNamespaceUsage().getSourceFile();
	}

	/**
	 * @param name
	 * @param namespace
	 * @return
	 */
	public CSharpSourceFile createStructSourceFile(String name, CSharpNamespace namespace) {
		CSharpStruct csStruct = new CSharpStruct(name, createNamespaceUsage(name, namespace));
		return csStruct.getNamespaceUsage().getSourceFile();
	}

	/**
	 * @param name
	 * @param namespace
	 * @return
	 */
	private CSharpNamespaceUsage createNamespaceUsage(String name, CSharpNamespace namespace) {
		if (namespace == null) namespace = CSharpNamespace.GLOBAL_NAMESPACE;
		CSharpSourceFile csSource = new CSharpSourceFile(name, namespace.getQualifiedName("/"));
		CSharpNamespaceUsage csNsUsage = new CSharpNamespaceUsage(namespace, csSource);
		return csNsUsage;
	}

	/**
	 * @param resultType
	 * @param modifiers
	 * @param name
	 * @param owner
	 * @param varargs
	 * @param paramInfos
	 * @return
	 */
	public CSharpMethod createMethod(CSharpType resultType, Set<CSharpModifier> modifiers, Set<CSharpTypeParameter> genericArguments,
			String name, CSharpType owner, boolean varargs, ParamInfo ...paramInfos) {

        Set<CSharpParameter> parameters = new LinkedHashSet<CSharpParameter>();
		if (paramInfos != null) {
			for (ParamInfo paramInfo : paramInfos) {
				CSharpParameter param = new CSharpParameter(paramInfo.getName(), paramInfo.getType());
				parameters.add(param);
			}
		}

		CSharpMethod method = new CSharpMethod(name, resultType, owner, genericArguments, parameters.toArray(new CSharpParameter[0]));
		method.setModifiers(modifiers);

		return method;
	}

	/**
	 * @param resultType
	 * @param name
	 * @param owner
	 * @param paramInfos
	 * @return
	 */
	public CSharpMethod createMethod(CSharpType resultType, Set<CSharpModifier> modifiers, String name, CSharpType owner, ParamInfo ...paramInfos) {
		return createMethod(resultType, modifiers, null, name, owner, paramInfos);
	}

	/**
	 * @param resultType
	 * @param modifiers
	 * @param genericArguments
	 * @param name
	 * @param owner
	 * @param paramInfos
	 * @return
	 */
	public CSharpMethod createMethod(CSharpType resultType, Set<CSharpModifier> modifiers, Set<CSharpTypeParameter> genericArguments, String name, CSharpType owner, ParamInfo ...paramInfos) {
		return createMethod(resultType, modifiers, genericArguments, name, owner, false, paramInfos);
	}

	/**
     * This is a helper class that allows for the creation of method objects by calling createMethod()
     * and passing type/name pairs. Otherwise the method parameter names are called arg0, arg1 ...
     * which is not so good for the readability of source code.
     *
     * @author mmt
     *
     */
    public static final class ParamInfo {

    	private final String name;
    	private final CSharpType type;
    	private final String doc;
    	private final Set<CSharpModifier> modifiers;

    	/**
    	 * @param type
    	 * @param name
    	 */
    	public ParamInfo(CSharpType type, String name) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = null;
    		this.modifiers = EnumSet.of(CSharpModifier.NONE);
    	}

    	/**
    	 * @param type
    	 * @param name
    	 * @param modifiers
    	 */
    	public ParamInfo(CSharpType type, String name, Set<CSharpModifier> modifiers) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = null;
    		this.modifiers = modifiers;
    	}

    	/**
    	 * @param type
    	 * @param name
    	 * @param doc
    	 */
    	public ParamInfo(CSharpType type, String name, String doc) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = doc;
    		this.modifiers = EnumSet.of(CSharpModifier.NONE);
    	}

    	/**
    	 * @param type
    	 * @param name
    	 * @param modifiers
    	 */
    	public ParamInfo(CSharpType type, String name, String doc, Set<CSharpModifier> modifiers) {
    		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
    		if (StringTools.isEmpty(name)) throw new IllegalArgumentException("parameter 'name' must neither be null nor be an empty string");

    		this.type = type;
    		this.name = name;
    		this.doc = doc;
    		this.modifiers = modifiers;
    	}

		/**
		 * @return the name
		 */
		protected final String getName() {
			return name;
		}

		/**
		 * @return the type
		 */
		protected final CSharpType getType() {
			return type;
		}



		/**
		 * @return the modifiers
		 */
		public Set<CSharpModifier> getModifiers() {
			return modifiers;
		}

		/**
		 * @return the doc
		 */
		public String getDoc() {
			return doc;
		}
    }



	/**
	 * @author mmt
	 *
	 */
	private final class BoundTypeKey {
		private final CSharpType genericTypeDefinition;
		private final LinkedHashSet<CSharpType> boundTypes = new LinkedHashSet<CSharpType>();

		/**
		 * @param genericTypeDefinition
		 * @param boundTypes
		 */
		public BoundTypeKey (CSharpType genericTypeDefinition, CSharpType... boundTypes) {
			if (genericTypeDefinition.isGenericTypeDefinition() == false) throw new IllegalArgumentException("parameter 'genericTypeDefinition' is not a generic type definition");
			if (boundTypes == null || boundTypes.length != genericTypeDefinition.getGenericArgumentsForTypeOnly().size()) throw new IllegalArgumentException("parameter 'boundTypes' is either null or the number of bound types does not match the number of generic arguments of the generic type definition");

			this.genericTypeDefinition = genericTypeDefinition;
			if (boundTypes != null && boundTypes.length > 0) {
				for (final CSharpType t : boundTypes) {
					if (t != null) {
						this.getBoundTypes().add(t);
					} else {
						throw new RuntimeException("tried to create a bound type key with one of its bound types being null");
					}
				}
			}
		}

		/**
		 * @return the genericTypeDefinition
		 */
		@SuppressWarnings("unused")
		public CSharpType getGenericTypeDefinition() {
			return genericTypeDefinition;
		}

		/**
		 * @return the boundTypes
		 */
		public LinkedHashSet<CSharpType> getBoundTypes() {
			return boundTypes;
		}
	}
}
