/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;



/**
 * There can be at most one destructor per class. Only classes can have a destructor.
 *
 * @author mmt
 *
 */
public class CSharpDestructor extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = -2037846454166775693L;

	/**
	 * @param owner
	 */
	public CSharpDestructor(CSharpType owner) {
		super(owner == null ? null : owner.getName(), owner, owner, (CSharpParameter[])null);

		validateOwner(owner);

		owner.setDestructor(this);
	}

	private void validateOwner(CSharpType owner) {
		if (owner instanceof CSharpInterface) {
			throw new RuntimeException("type '" + owner.getClass() + "' must not be the owner of a destructor");
		}
	}
}
