/**
 *
 */
package com.gs.gapp.metamodel.csharp.type;

import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;
import com.gs.gapp.metamodel.csharp.member.CSharpMethod;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;

/**
 * @author mmt
 *
 */
public class CSharpInterface extends CSharpReferenceType {

	/**
	 *
	 */
	private static final long serialVersionUID = 6611533100165715466L;

	/**
	 * Use this constructor when the type is a predefined type that is not going to be written to a source file
	 * by the codegeneration (isGenerated() == false).
	 * 
	 * @param name
	 * @param namespace
	 */
	public CSharpInterface(String name, CSharpNamespace namespace) {
		super(name, namespace);
	}
	
	/**
	 * @param name
	 * @param namespaceUsage
	 */
	public CSharpInterface(String name, CSharpNamespaceUsage namespaceUsage) {
		super(name, namespaceUsage);
	}

	/**
	 * @param name
	 * @param owningType
	 */
	public CSharpInterface(String name, CSharpType owningType) {
		super(name, owningType);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getMethods()
	 */
	@Override
	public Set<CSharpMethod> getMethods() {
		return super.getMethods();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addMethods(com.gs.gapp.metamodel.csharp.member.CSharpMethod[])
	 */
	@Override
	public void addMethods(CSharpMethod... methods) {
		super.addMethods(methods);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getProperties()
	 */
	@Override
	public Set<CSharpProperty> getProperties() {
		return super.getProperties();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#addProperties(com.gs.gapp.metamodel.csharp.member.CSharpProperty[])
	 */
	@Override
	public void addProperties(CSharpProperty... properties) {
		super.addProperties(properties);
	}

	
}
