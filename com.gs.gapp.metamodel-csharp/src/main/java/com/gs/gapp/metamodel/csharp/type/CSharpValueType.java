package com.gs.gapp.metamodel.csharp.type;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;

public abstract class CSharpValueType extends CSharpType {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8744017144308501230L;

	/**
	 * @param name
	 * @param namespace
	 */
	public CSharpValueType(String name, CSharpNamespace namespace) {
		super(name, namespace);
	}

	/**
	 * @param name
	 * @param namespaceUsage
	 */
	public CSharpValueType(String name, CSharpNamespaceUsage namespaceUsage) {
		super(name, namespaceUsage);
	}

	/**
	 * @param name
	 * @param owningType
	 */
	public CSharpValueType(String name, CSharpType owningType) {
		super(name, owningType);
	}
	
}
