package com.gs.gapp.metamodel.csharp.attribute;

import com.gs.gapp.metamodel.csharp.type.CSharpModelElement;

public class CSharpAttributeUsage extends CSharpModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = -8033548275644245993L;

	private final CSharpAttributableI owner;

	public CSharpAttributeUsage(String name, CSharpAttributableI owner) {
		super(name);
		this.owner = owner;
	}

	/**
	 * @return the owner
	 */
	public CSharpAttributableI getOwner() {
		return owner;
	}

}
