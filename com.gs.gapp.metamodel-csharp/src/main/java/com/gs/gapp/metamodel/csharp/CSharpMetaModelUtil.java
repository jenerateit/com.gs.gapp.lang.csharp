package com.gs.gapp.metamodel.csharp;

import com.gs.gapp.metamodel.csharp.type.CSharpType;


public class CSharpMetaModelUtil {
	
	public static final String SYSTEM_NAMESPACE_NAME = "System";

	private CSharpMetaModelUtil() {}

	/**
	 * @param csharpType
	 * @return
	 */
	public static String getFilePath(CSharpType csharpType) {
		StringBuilder sb = new StringBuilder();
		if (csharpType.getNamespace() != null) {
			sb.append(csharpType.getNamespace().getQualifiedName("/")).append("/");
		}

		sb.append(csharpType.getName());

		return sb.toString();
	}
}
