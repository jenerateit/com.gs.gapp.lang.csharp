package com.gs.gapp.metamodel.csharp.type;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;
import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributableI;
import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributeUsage;
import com.gs.gapp.metamodel.csharp.member.CSharpConstructor;
import com.gs.gapp.metamodel.csharp.member.CSharpDestructor;
import com.gs.gapp.metamodel.csharp.member.CSharpEvent;
import com.gs.gapp.metamodel.csharp.member.CSharpField;
import com.gs.gapp.metamodel.csharp.member.CSharpIndexer;
import com.gs.gapp.metamodel.csharp.member.CSharpMember;
import com.gs.gapp.metamodel.csharp.member.CSharpMethod;
import com.gs.gapp.metamodel.csharp.member.CSharpOperator;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpGenericParameterAttributes;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;

public abstract class CSharpType extends CSharpModelElement implements CSharpAttributableI {

	/**
	 *
	 */
	private static final long serialVersionUID = -1875207698408445122L;

	private static final Map<String, String> builtInTypeNameMap = new HashMap<String, String>();

    static {
    	builtInTypeNameMap.put("Boolean", "bool");
        builtInTypeNameMap.put("Byte", "byte");
        builtInTypeNameMap.put("SByte", "sbyte");
        builtInTypeNameMap.put("Char", "char");
        builtInTypeNameMap.put("Decimal", "decimal");
        builtInTypeNameMap.put("Double", "double");
        builtInTypeNameMap.put("Single", "float");
        builtInTypeNameMap.put("Int32", "int");
        builtInTypeNameMap.put("UInt32", "uint");
        builtInTypeNameMap.put("Int64", "long");
        builtInTypeNameMap.put("UInt64", "ulong");
        builtInTypeNameMap.put("Object", "object");
        builtInTypeNameMap.put("Int16", "short");
        builtInTypeNameMap.put("UInt16", "ushort");
        builtInTypeNameMap.put("String", "string");
    };


	private final CSharpNamespace namespace;
	private final CSharpType owningType;
	private Set<CSharpModifier> modifiers = EnumSet.of(CSharpModifier.NONE);
	private CSharpType baseType;
	private final Set<CSharpInterface> interfaces = new LinkedHashSet<CSharpInterface>();
	private final Set<CSharpAttributeUsage> attributes = new LinkedHashSet<CSharpAttributeUsage>();

	private final Set<CSharpConstructor> constructors = new LinkedHashSet<CSharpConstructor>();
	private CSharpDestructor destructor;
	private final Set<CSharpEvent> events = new LinkedHashSet<CSharpEvent>();
	private final Set<CSharpField> fields = new LinkedHashSet<CSharpField>();
	private final Set<CSharpIndexer> indexers = new LinkedHashSet<CSharpIndexer>();
	private final Set<CSharpMethod> methods = new LinkedHashSet<CSharpMethod>();
	private final Set<CSharpOperator> operators = new LinkedHashSet<CSharpOperator>();
	private final Set<CSharpProperty> properties = new LinkedHashSet<CSharpProperty>();

	private final Set<CSharpType> ownedTypes = new LinkedHashSet<CSharpType>();

	private final CSharpNamespaceUsage namespaceUsage;

	private boolean primitive = false;

	private final Set<CSharpType> genericArguments = new LinkedHashSet<CSharpType>();

	/**
	 * Use this constructor when the type is a nested type. In this case a namespace (or namespace usage)
	 * is not needed since the owning type provides the namespace.
	 *
	 * @param name
	 * @param owningType
	 */
	public CSharpType(String name, CSharpType owningType) {
		super(name);
		if (owningType == null) throw new NullPointerException("parameter 'owningType' must not be null");

		this.namespace = null;
		this.owningType = owningType;
		owningType.addOwnedTypes(this);
		this.namespaceUsage = null;
	}

	/**
	 * Use this constructor when the type is a predefined type that is not going to be written to a source file
	 * by the codegeneration (isGenerated() == false).
	 *
	 * @param name
	 * @param namespace
	 */
	public CSharpType(String name, CSharpNamespace namespace) {
		super(name);
		if (namespace == null) throw new NullPointerException("parameter 'namespace' must not be null");

		this.namespace = namespace;
		this.owningType = null;
		this.namespaceUsage = null;

		this.namespace.addTypes(this);
	}

	/**
	 * Use this constructor when the type has been created by a model-converter and
	 * is _not_ a nested type and needs to be written to a source file.
	 *
	 * by the codegeneration (isGenerated() == false).
	 *
	 * @param name
	 * @param namespaceUsage
	 */
	public CSharpType(String name, CSharpNamespaceUsage namespaceUsage) {
		super(name);
		if (namespaceUsage == null) throw new NullPointerException("parameter 'namespaceUsage' must not be null");

		this.namespace = namespaceUsage.getNamespace();
		this.owningType = null;
		this.namespaceUsage = namespaceUsage;

		this.namespaceUsage.addTypes(this);
	}

	/**
	 * @return
	 */
	public boolean isNestedType() {
		return this.owningType != null;
	}

	/**
	 * @return
	 */
	public boolean isPartOfSourceFile() {
		return this.namespaceUsage != null;
	}

	/**
	 * @return
	 */
	public boolean isInGlobelNamespace() {
		return this.namespace == CSharpNamespace.GLOBAL_NAMESPACE;
	}

	/**
	 * @return the namespace
	 */
	public CSharpNamespace getNamespace() {
		return namespace;
	}

	/**
	 * @return the attributes
	 */
	public Set<CSharpAttributeUsage> getAttributeUsages() {
		return attributes;
	}

	/**
	 * @param attributes
	 */
	public void addAttributeUsages(CSharpAttributeUsage... attributes) {
		if (attributes != null) {
		    this.attributes.addAll(Arrays.asList(attributes));
		}
	}

	/**
	 * @return the owningType
	 */
	public CSharpType getOwningType() {
		return owningType;
	}

	/**
	 * @param delimiter
	 * @return
	 */
	public String getQualifiedName(String delimiter) {
		if (delimiter == null) { throw new NullPointerException("parameter delimiter must not be null"); }
		if (delimiter.length() == 0) { throw new IllegalArgumentException("parameter delimiter has zero length"); }

		if (this.isPrimitive()) {
			return this.getTypeAsString();
		} else {
			String qualifiedPrefix = null;

			if (this.owningType != null) {
				qualifiedPrefix = this.owningType.getQualifiedName(delimiter);
			} else if (this.getNamespace() != null) {
			    qualifiedPrefix = this.getNamespace().getQualifiedName(delimiter);
			} else {
				throw new RuntimeException("neither owning type nor namespace found for type '" + this + "'");
			}

	        return new StringBuilder(qualifiedPrefix).append(delimiter).append(this.getName()).toString();
		}
	}



	/**
	 * @return the modifiers
	 */
	public Set<CSharpModifier> getModifiers() {
		return modifiers;
	}

	/**
	 * @return
	 */
	public CSharpModifier getAccessModifier() {
		for (CSharpModifier modifier : this.modifiers) {
			if (modifier.isAccessModifier()) return modifier;
		}
		return null;
	}

	/**
	 * @return
	 */
	public boolean isStatic() {

		if (this.modifiers.contains(CSharpModifier.STATIC)) return true;

		return false;
	}

	/**
	 * @return
	 */
	public boolean isPartial() {

		if (this.modifiers.contains(CSharpModifier.PARTIAL)) return true;

		return false;
	}

	/**
	 * @return
	 */
	public boolean isAbstract() {

		if (this.modifiers.contains(CSharpModifier.ABSTRACT)) return true;

		return false;
	}

	/**
	 * @return
	 */
	public boolean isSealed() {

		if (this.modifiers.contains(CSharpModifier.SEALED)) return true;

		return false;
	}

	/**
	 * @param modifiers the modifier to set
	 */
	public void setModifiers(Set<CSharpModifier> modifiers) {
		if (modifiers == null) throw new NullPointerException("parameter 'modifiers' must not be null");
		this.modifiers = modifiers;
	}

	/**
	 * @return the baseType
	 */
	public CSharpType getBaseType() {
		return baseType;
	}

	/**
	 * @param baseType the baseType to set
	 */
	public void setBaseType(CSharpType baseType) {
		this.baseType = baseType;
	}

	/**
	 * @return the interfaces
	 */
	public Set<CSharpInterface> getInterfaces() {
		return interfaces;
	}

	/**
	 * @param interfaces
	 */
	public void addInterfaces(CSharpInterface ...interfaces) {
		if (interfaces != null) {
			this.interfaces.addAll(Arrays.asList(interfaces));
		}
	}

	/**
	 * @return the constructors
	 */
	public Set<CSharpConstructor> getConstructors() {
		return constructors;
	}

	/**
	 * @param constructors
	 */
	public void addConstructors(CSharpConstructor ...constructors) {
		if (constructors != null) {
			verifyMembers(constructors);
			this.constructors.addAll(Arrays.asList(constructors));
		}
	}

	/**
	 * @return the destructor
	 */
	public CSharpDestructor getDestructor() {
		return destructor;
	}

	/**
	 * @param destructor the destructor to set
	 */
	public void setDestructor(CSharpDestructor destructor) {
		verifyMembers(destructor);
		this.destructor = destructor;
	}

	/**
	 * @return the events
	 */
	public Set<CSharpEvent> getEvents() {
		return events;
	}

	/**
	 * @param events
	 */
	public void addEvents(CSharpEvent ...events) {
		if (events != null) {
			verifyMembers(events);
			this.events.addAll(Arrays.asList(events));
		}
	}

	/**
	 * @return the fields
	 */
	public Set<CSharpField> getFields() {
		return fields;
	}

	/**
	 * @param fields
	 */
	public void addFields(CSharpField ...fields) {
		if (fields != null) {
			verifyMembers(fields);
			this.fields.addAll(Arrays.asList(fields));
		}
	}

	/**
	 * @return the indexers
	 */
	public Set<CSharpIndexer> getIndexers() {
		return indexers;
	}

	/**
	 * @param indexers
	 */
	public void addIndexers(CSharpIndexer ...indexers) {
		if (indexers != null) {
			verifyMembers(indexers);
			this.indexers.addAll(Arrays.asList(indexers));
		}
	}

	/**
	 * @return the methods
	 */
	public Set<CSharpMethod> getMethods() {
		return methods;
	}

	/**
	 * @param methods
	 */
	public void addMethods(CSharpMethod ...methods) {
		if (methods != null) {
			verifyMembers(methods);
			this.methods.addAll(Arrays.asList(methods));
		}
	}

	/**
	 * @return the operators
	 */
	public Set<CSharpOperator> getOperators() {
		return operators;
	}

	/**
	 * @param operators
	 */
	public void addOperators(CSharpOperator ...operators) {
		if (operators != null) {
			verifyMembers(operators);
			this.operators.addAll(Arrays.asList(operators));
		}
	}

	protected void verifyMembers(CSharpMember ...members) {
		if (members != null) {
			for (CSharpMember member : members) {
				if (member == null) throw new IllegalArgumentException("a null member has been provided in the array of members");
				if (member.getOwner() == null || member.getOwner() != this) throw new IllegalArgumentException("a member of type '" + member.getClass().getSimpleName() + "' has been provided that either has no owner set or has an owner set that is different from the one it is just being added to");
			}
		}
	}

	/**
	 * @return the properties
	 */
	public Set<CSharpProperty> getProperties() {
		return properties;
	}

	/**
	 * @param properties
	 */
	public void addProperties(CSharpProperty ...properties) {
		if (properties != null) {
			verifyMembers(properties);
			this.properties.addAll(Arrays.asList(properties));
		}
	}

	/**
	 * @return the namespaceUsage
	 */
	public CSharpNamespaceUsage getNamespaceUsage() {
		return namespaceUsage;
	}

	/**
	 * @return
	 */
	public String getTypeAsString() {
		StringBuilder result = new StringBuilder();
		if (this.isGenericTypeDefinition()) {
			String comma = "";
			result.append(this.getName());
			if (this.getGenericArgumentsForTypeOnly().size() > 0) {
				result.append("<");

				for (CSharpType genericParameterType : this.getGenericArgumentsForTypeOnly()) {
	                result.append(comma).append(genericParameterType.getTypeAsString());
	                comma = ", ";
				}
				result.append(">");
			}
		} else if (this.isGenericParameter()) {
			if (this.getOwningType().isGenericTypeDefinition()) {
				CSharpTypeParameter typeParameter = (CSharpTypeParameter) this;
				for (CSharpGenericParameterAttributes attr : typeParameter.getGenericParameterAttributes()) {
					switch (attr) {
					case CONTRAVARIANT:
						result.append("in ");
						break;
					case COVARIANT:
						result.append("out ");
						break;
					case DEFAULT_CONSTRUCTOR_CONSTRAINT:
					case NONE:
					case NOT_NULLABLE_VALUE_TYPE_CONSTRAINT:
					case REFERENCE_TYPE_CONSTRAINT:
						// this is not handled here but in getGenericConstraintsAsString()
						break;
					default:
						throw new RuntimeException("unhandled generic parameter attribute '" + attr + "' found");
					}
				}
			} else {
				// in this case the 'in' and 'out' qualifiers do not make sense
			}
			result.append(this.getName());
		} else if (this.isGenericType()) {
			String comma = "";
			result.append(this.getName()).append("<");
			for (CSharpType genericParameterType : this.getGenericArguments()) {
                result.append(comma).append(genericParameterType.getTypeAsString());
                comma = ", ";
			}
			result.append(">");
		} else {
			if (this.isPrimitive()) {
				String alias = builtInTypeNameMap.get(this.getName());
				if (alias != null && alias.length() > 0) {
					result.append(alias);
				} else {
					result.append(this.getName());
				}
			} else {
				if ("Void".equals(this.getName())) {
					result.append("void");
				} else {
			        result.append(this.getName());
				}
			}
		}
		return result.toString();
	}

	/**
	 * @return the primitive
	 */
	public boolean isPrimitive() {
		return primitive;
	}

	/**
	 * @param primitive the primitive to set
	 */
	public void setPrimitive(boolean primitive) {
		if (this.primitive == true && primitive == false) {
			throw new IllegalArgumentException("it is not allowed to set the 'primitive' flag to false once it has been set to true before");
		}
		this.primitive = primitive;
	}

	public Set<CSharpType> getGenericArgumentsForTypeOnly() {
		Set<CSharpType> result = new LinkedHashSet<CSharpType>();
		for (CSharpType type : this.genericArguments) {
			if (type instanceof CSharpTypeParameter) {
				CSharpTypeParameter typeParameter = (CSharpTypeParameter) type;
				if (typeParameter.getMethod() == null) result.add(type);
			}
		}
		return result;
	}

	/**
	 * @return the genericArguments
	 */
	public Set<CSharpType> getGenericArguments() {
		return genericArguments;
	}

	public String getGenericConstraintsAsString() {
		String result = "";
		if (isGenericTypeDefinition()) {
			StringBuilder sb = new StringBuilder("");

			for (CSharpType genericArgument : this.getGenericArgumentsForTypeOnly()) {
				String where = "where : ";
				String comma = "";
				String defaultConstructorConstraint = "";  // needs to be at the very end of all constraints
				if (genericArgument instanceof CSharpTypeParameter) {

					CSharpTypeParameter typeParameter = (CSharpTypeParameter) genericArgument;
					for (CSharpType parameterConstraint : typeParameter.getGenericParameterConstraints()) {
						sb.append(where).append(comma).append(parameterConstraint.getTypeAsString());
						where = "";
						comma = ", ";
					}

					for (CSharpGenericParameterAttributes attr : typeParameter.getGenericParameterAttributes()) {
						switch (attr) {
						case CONTRAVARIANT:
							// this is not handled in the 'where' clause but in the generic type declaration
							break;
						case COVARIANT:
							// this is not handled in the 'where' clause but in the generic type declaration
							break;
						case DEFAULT_CONSTRUCTOR_CONSTRAINT:
							defaultConstructorConstraint = "new()";
							break;
						case NONE:
							// ignore this
							break;
						case NOT_NULLABLE_VALUE_TYPE_CONSTRAINT:
							sb.append(where).append(comma).append("struct");
							break;
						case REFERENCE_TYPE_CONSTRAINT:
							sb.append(where).append(comma).append("class");
							break;
						default:
							throw new RuntimeException("unhandled generic parameter attribute '" + attr + "' found");
						}
						where = "";
						comma = ", ";
					}
				} // if it is a generic type parameter

				// the default constructor constraint needs to be at the very end of all constraints
				if (defaultConstructorConstraint.length() > 0) {
					sb.append(where).append(comma).append(defaultConstructorConstraint);
				}
			} // for all generic type parameters
			result = sb.toString();
		} else if (this.isGenericParameter()) {
			result = "";
		} else if (this.isGenericType()) {
			result = "";
		} else {
			result = "";
		}

		return result;
	}
	/**
	 * @param genericArguments
	 */
	public void addGenericArguments(CSharpType ...genericArguments) {
		if (genericArguments != null) {
			this.genericArguments.addAll(Arrays.asList(genericArguments));
		}
	}

	/**
	 * @return
	 */
	public boolean isGenericTypeDefinition() {
		boolean result = false;
		if (this.genericArguments.size() > 0) {
			result = true;
			for (CSharpType type : this.genericArguments) {
				if (!(type instanceof CSharpTypeParameter)) {
					result = false;
					break;
				}
			}
		}

		if (result == false && this.owningType != null) {
			result = this.owningType.isGenericTypeDefinition();
		}

		return result;
	}

    public boolean isGenericType() {
		return isGenericTypeDefinition() || getGenericTypeDefinition() != null || (this.owningType != null && this.owningType.isGenericType());
	}

    public boolean isConstructedType() {
    	return false;
    }

    /**
     * @return
     */
    public boolean isGenericParameter() {
    	return false;
    }

    public CSharpType getGenericTypeDefinition() {
		return null;
	}

    /**
     * @return
     */
    public boolean containsGenericParameters() {
    	boolean result = false;

    	CSharpTypeParameter firstGenericTypeParameter = findFirstGenericTypeParameter();
    	result = firstGenericTypeParameter != null;

    	if (result == false && this.owningType != null) {
    		result = this.owningType.containsGenericParameters();
    	}

    	return result;
    }

    private CSharpTypeParameter findFirstGenericTypeParameter() {
    	for (CSharpType genericArgument : this.genericArguments) {
    		if (genericArgument instanceof CSharpTypeParameter) {
    			CSharpTypeParameter cSharpTypeParameter = (CSharpTypeParameter) genericArgument;
				return cSharpTypeParameter;
    		}
    		CSharpTypeParameter result = genericArgument.findFirstGenericTypeParameter();
    		if (result != null) return result;
    	}

    	return null;  // nothing was found
    }

	/**
	 * @return the ownedTypes
	 */
	public Set<CSharpType> getOwnedTypes() {
		return ownedTypes;
	}

	/**
	 * @param ownedTypes
	 */
	protected void addOwnedTypes(CSharpType ...ownedTypes) {
		if (ownedTypes != null) {
			this.ownedTypes.addAll(Arrays.asList(ownedTypes));
		}
	}

	/**
	 * @return
	 */
	public String getGenericTypeDeclaration() {
		Set<CSharpType> genericArgumentsForTypeOnly = getGenericArgumentsForTypeOnly();
		if (isGenericTypeDefinition() && genericArgumentsForTypeOnly.size() > 0) {
			StringBuilder sb = new StringBuilder();
			String comma = "";
			for (CSharpType genericArgument : genericArgumentsForTypeOnly) {
	            sb.append(comma).append(genericArgument.getTypeAsString());
			}

			return sb.toString();
		}
		return null;
	}
}
