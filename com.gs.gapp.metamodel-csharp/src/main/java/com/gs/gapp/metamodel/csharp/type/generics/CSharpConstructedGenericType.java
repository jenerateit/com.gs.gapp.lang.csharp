package com.gs.gapp.metamodel.csharp.type.generics;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpType;

public class CSharpConstructedGenericType extends CSharpType {

    /**
	 *
	 */
	private static final long serialVersionUID = 7898422613987258018L;

	private final CSharpType genericTypeDefinition;

	/**
	 * @param genericTypeDefinition
	 * @param namespace
	 * @param genericArguments
	 */
	public CSharpConstructedGenericType(CSharpType genericTypeDefinition, CSharpNamespace namespace, CSharpType ...genericArguments) {
		super(genericTypeDefinition == null ? null : genericTypeDefinition.getName(), namespace);

		this.genericTypeDefinition = genericTypeDefinition;
        this.addGenericArguments(genericArguments);
	}

	/**
	 * @param genericTypeDefinition
	 * @param owningType
	 * @param genericArguments
	 */
	public CSharpConstructedGenericType(CSharpType genericTypeDefinition, CSharpType owningType, CSharpType ...genericArguments) {
		super(genericTypeDefinition == null ? null : genericTypeDefinition.getName(), owningType);

		this.genericTypeDefinition = genericTypeDefinition;
		this.addGenericArguments(genericArguments);
	}

	/*
	 * Within this instance initializer, we can check, wehther the constructed generic arguments fit to the generic arguments of the generic type definition.
	 */
	{
		// TODO verify settings of generic arguments here
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.csharp.type.CSharpType#getGenericTypeDefinition()
	 */
	@Override
	public CSharpType getGenericTypeDefinition() {
		return genericTypeDefinition;
	}

	/**
	 * @return
	 */
	@Override
	public boolean isGenericTypeDefinition() {
		return false;
	}

	public boolean isConstructedType() {
    	return true;
    }
}
