/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributableI;
import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributeUsage;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpModelElement;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;

/**
 * http://msdn.microsoft.com/en-us/library/ms173113.aspx
 * 
 * @author mmt
 *
 */
public abstract class CSharpMember extends CSharpModelElement implements CSharpAttributableI {

	private final CSharpType owner;
	private final CSharpType type;
	private Set<CSharpModifier> modifiers = EnumSet.of(CSharpModifier.NONE);
	private final Set<CSharpParameter> parameters = new LinkedHashSet<CSharpParameter>();
	private final Set<CSharpAttributeUsage> attributes = new LinkedHashSet<CSharpAttributeUsage>();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 292127337231499290L;

	public CSharpMember(String name, CSharpType type, CSharpType owner, CSharpParameter... parameters) {
		super(name);
		
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		if (owner == null) throw new NullPointerException("parameter 'owner' must not be null");
		
		this.type = type;
		this.owner = owner;
		if (parameters != null) {
			for (CSharpParameter parameter : parameters) {
				parameter.setOwner(this);  // this is the only place, where the onwer of a parameter gets set - needs to be set before the parameters are added to the collection since the owner is relevant for equality/hashCode
			}
			this.parameters.addAll(Arrays.asList(parameters));
		}
	}
	
	/**
	 * @return the attributes
	 */
	public Set<CSharpAttributeUsage> getAttributeUsages() {
		return attributes;
	}
	
	/**
	 * @param attributes
	 */
	public void addAttributeUsages(CSharpAttributeUsage... attributes) {
		if (attributes != null) {
		    this.attributes.addAll(Arrays.asList(attributes));
		}
	}

	/**
	 * @return the parameters
	 */
	public Set<CSharpParameter> getParameters() {
		return parameters;
	}

	/**
	 * @return the owner
	 */
	public CSharpType getOwner() {
		return owner;
	}
	
	/**
	 * @return the modifiers
	 */
	public Set<CSharpModifier> getModifiers() {
		return modifiers;
	}

	/**
	 * @param modifiers the modifier to set
	 */
	public void setModifiers(Set<CSharpModifier> modifiers) {
		if (modifiers == null) throw new NullPointerException("parameter 'modifers' must not be null");
		this.modifiers = modifiers;
	}
	
	/**
	 * @return
	 */
	public CSharpModifier getAccessModifier() {
		for (CSharpModifier modifier : this.modifiers) {
			if (modifier.isAccessModifier()) return modifier;
		}
		return null;
	}
	
	/**
	 * @return
	 */
	public boolean isStatic() {
		
		if (this.modifiers.contains(CSharpModifier.STATIC)) return true;
		
		return false;
	}
	
	/**
	 * @return
	 */
	public boolean isAbstract() {
		
		if (this.modifiers.contains(CSharpModifier.ABSTRACT)) return true;
		
		return false;
	}
	
	/**
	 * @return
	 */
	public boolean isSealed() {
		
		if (this.modifiers.contains(CSharpModifier.SEALED)) return true;
		
		return false;
	}

	/**
	 * @return the type
	 */
	public CSharpType getType() {
		return type;
	}
	
	/**
     * @return
     */
    public boolean isInInterface() {
    	return (this.getOwner() != null &&
    			this.getOwner() instanceof CSharpInterface);
    }
    
    /**
     * @return
     */
    public boolean isInClass() {
    	return (this.getOwner() != null &&
    			this.getOwner() instanceof CSharpClass);
    }
    
    /**
     * @return
     */
    public boolean isInEnum() {
    	return (this.getOwner() != null &&
    			this.getOwner() instanceof CSharpEnum);
    }
    
    /**
     * @return
     */
    public boolean isInStruct() {
    	return (this.getOwner() != null &&
    			this.getOwner() instanceof CSharpStruct);
    }
}
