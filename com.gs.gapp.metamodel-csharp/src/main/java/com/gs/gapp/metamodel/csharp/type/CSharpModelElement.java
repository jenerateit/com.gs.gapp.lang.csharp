package com.gs.gapp.metamodel.csharp.type;

import com.gs.gapp.metamodel.basic.ModelElement;

public abstract class CSharpModelElement extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1875207698408445122L;
	
	public CSharpModelElement(String name) {
		super(name);
	}
	
	
}
