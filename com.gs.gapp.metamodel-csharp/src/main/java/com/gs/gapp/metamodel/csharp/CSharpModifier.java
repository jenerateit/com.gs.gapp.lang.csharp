/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.csharp;

import java.util.EnumSet;
import java.util.Set;

/**
 * @author hrr
 *
 */
public enum CSharpModifier {
	
	NONE		(""),
	ABSTRACT	("abstract"),
	ASYNC		("async"),
	CONST		("const"),
	EVENT		("event"),
	EXTERN		("extern"),
	NEW			("new"),
	OVERRIDE	("override"),
	PARTIAL		("partial"),
	READONLY	("readonly"),
	SEALED		("sealed"),
	STATIC		("static"),
	UNSAFE		("unsafe"),
	VIRTUAL		("virtual"),
	VOLATILE	("volatile"),
	
	PUBLIC		("public"),
	PRIVATE		("private"),
	INTERNAL	("internal"),
	PROTECTED	("protected"),
	/**
	 * Note that this is not really a modifer by itself. Defining such a combined modifier eases the meta-model
	 * since it allows an access modifier to be a single enum entry.
	 */
	PROTECTED_INTERNAL	("protected internal"),
	;
	
	private final static Set<CSharpModifier> accessModifiers = EnumSet.of(PUBLIC, PRIVATE, INTERNAL, PROTECTED, PROTECTED_INTERNAL);
	
	private String name;
	
	private CSharpModifier(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public final String getName() {
		return name;
	}

	/**
	 * @return the accessmodifiers
	 */
	public static Set<CSharpModifier> getAccessmodifiers() {
		return accessModifiers;
	}
	
	/**
	 * @return
	 */
	public boolean isAccessModifier() {
		return accessModifiers.contains(this);
	}
}
