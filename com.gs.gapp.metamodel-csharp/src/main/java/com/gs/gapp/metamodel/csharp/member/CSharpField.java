/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;



/**
 * A field can be part of classes or structs.
 * For built-in types a field can be defined as a constant: http://msdn.microsoft.com/en-us/library/ms173119.aspx
 *
 * @author mmt
 *
 */
public class CSharpField extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = -4078841436685745938L;

	/**
	 * @param name
	 * @param owner
	 * @param type
	 */
	public CSharpField(String name, CSharpType type, CSharpType owner) {
		super(name, type, owner, (CSharpParameter[])null);

		validateOwner(owner);

		owner.addFields(this);
	}

	private void validateOwner(CSharpType owner) {
		if (owner instanceof CSharpInterface) {
			throw new RuntimeException("type '" + owner + "' must not be the owner of a field");
		}
	}
}
