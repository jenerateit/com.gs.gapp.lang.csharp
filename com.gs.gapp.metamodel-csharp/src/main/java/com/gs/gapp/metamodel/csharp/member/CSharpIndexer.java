/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import com.gs.gapp.metamodel.csharp.type.CSharpType;



/**
 * Indexers can be used for interfaces, classes or structs.
 * Indexers do always get the name 'this'.
 *
 * @author mmt
 *
 */
public class CSharpIndexer extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = 7824556696319416048L;

	private boolean readOnly = false;

	/**
	 * @param type
	 * @param owner
	 * @param parameters
	 */
	public CSharpIndexer(CSharpType type, CSharpType owner, CSharpParameter... parameters) {
		super("this", type, owner, parameters);

		validateOwner(owner);

		owner.addIndexers(this);
	}

	private void validateOwner(CSharpType owner) {
		// TODO
//		if (owner instanceof CSharpInterface) {
//			throw new RuntimeException("type '" + owner + "' must not be the owner of a constructor");
//		}
	}

	/**
	 * @return the readOnly
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/**
	 * @param readOnly the readOnly to set
	 */
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}
}
