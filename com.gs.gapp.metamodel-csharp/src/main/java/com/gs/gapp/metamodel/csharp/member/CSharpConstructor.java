/**
 *
 */
package com.gs.gapp.metamodel.csharp.member;

import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;



/**
 * Constructors can be part of classes or structs.
 *
 * @author mmt
 *
 */
public class CSharpConstructor extends CSharpMember {

	/**
	 *
	 */
	private static final long serialVersionUID = -1319911159502651108L;

	/**
	 * @param owner
	 * @param parameters
	 */
	public CSharpConstructor(CSharpType owner,
			CSharpParameter... parameters) {
		super(owner == null ? null : owner.getName(), owner, owner, parameters);

		// TODO add validation to check for an allowed owner type
		validateOwner(owner);

        owner.addConstructors(this);
	}

	private void validateOwner(CSharpType owner) {
		if (owner instanceof CSharpInterface) {
			throw new RuntimeException("type '" + owner.getName() + "' must not be the owner of a constructor");
		}
	}

}
