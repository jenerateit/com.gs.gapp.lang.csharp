/**
 *
 */
package com.gs.gapp.metamodel.csharp.type;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;

/**
 * @author mmt
 *
 */
public class CSharpDelegate extends CSharpReferenceType {

	/**
	 *
	 */
	private static final long serialVersionUID = 6611533100165715466L;

	private final CSharpType type;
	private final Set<CSharpDelegateParameter> parameters = new LinkedHashSet<CSharpDelegateParameter>();
	
	/**
	 * Use this constructor when the type is a predefined type that is not going to be written to a source file
	 * by the codegeneration (isGenerated() == false).
	 * 
	 * @param name
	 * @param namespace
	 */
	public CSharpDelegate(String name, CSharpType type, CSharpNamespace namespace, CSharpDelegateParameter... parameters) {
		super(name, namespace);
		this.type = type;
		init(parameters);
	}

	/**
	 * @param name
	 * @param namespaceUsage
	 */
	public CSharpDelegate(String name, CSharpType type, CSharpNamespaceUsage namespaceUsage, CSharpDelegateParameter... parameters) {
		super(name, namespaceUsage);
		this.type = type;
		init(parameters);
	}

	/**
	 * @param name
	 * @param owningType
	 */
	public CSharpDelegate(String name, CSharpType type, CSharpType owningType, CSharpDelegateParameter... parameters) {
		super(name, owningType);
		this.type = type;
		init(parameters);
	}
	
	private void init(CSharpDelegateParameter ...parameters) {
		if (parameters != null) {
			for (CSharpDelegateParameter parameter : parameters) {
				parameter.setOwner(this);  // this is the only place, where the onwer of a delegate parameter gets set - needs to be set before the parameters are added to the collection since the owner is relevant for equality/hashCode
			}
			this.parameters.addAll(Arrays.asList(parameters));
		}
	}

	/**
	 * @return the type
	 */
	public CSharpType getType() {
		return type;
	}
	
	/**
	 * @return the parameters
	 */
	public Set<CSharpDelegateParameter> getParameters() {
		return parameters;
	}
}
