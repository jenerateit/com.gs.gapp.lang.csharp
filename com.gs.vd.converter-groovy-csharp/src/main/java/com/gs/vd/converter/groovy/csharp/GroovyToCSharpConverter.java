package com.gs.vd.converter.groovy.csharp;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.csharp.CSharpModelElementCache;
import com.gs.vd.converter.groovy.any.GroovyToAnyConverter;

public class GroovyToCSharpConverter extends GroovyToAnyConverter {

	/**
	 * @param modelElementCache
	 */
	public GroovyToCSharpConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}
	
	/**
	 * 
	 */
	public GroovyToCSharpConverter() {
		super(new CSharpModelElementCache());
	}
}
