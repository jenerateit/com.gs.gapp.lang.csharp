/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 *
 */
package com.gs.gapp.generation.csharp.target;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.analytics.TargetModelizer;
import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.generation.csharp.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.csharp.CSharpSourceFile;

/**
 * {@link TargetI} implementation of a Java target.
 *
 * @author hrr
 */
public class CSharpSourceFileTarget extends BasicTextTarget<CSharpTargetDocument> {

	private static final TargetLifecycleListenerI CSHARP_USING_MERGER = new CSharpUsingMerger();
	private static final TargetLifecycleListenerI TARGET_MODELIZER = new TargetModelizer();

	@org.jenerateit.annotation.ModelElement
	private CSharpSourceFile csharpSourceFile;

	/**
	 *
	 */
	public CSharpSourceFileTarget() {
		super();
		addTargetLifecycleListener(CSHARP_USING_MERGER);
		addTargetLifecycleListener(TARGET_MODELIZER);
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetI#getTargetURI()
	 */
	@Override
	public URI getTargetURI() {
		StringBuilder sb = new StringBuilder();

		sb.append(getTargetRoot()).append("/").append(getTargetPrefix()).append("/");
		sb.append(this.csharpSourceFile.getPath() == null ? "" : this.csharpSourceFile.getPath()).append("/").append(this.csharpSourceFile.getName()).append(".cs");

		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, (TargetI<? extends TargetDocumentI>) this);
		}
	}
	
	/**
	 * @return
	 */
	public Set<DeveloperAreaTypeEnum> getActiveDeveloperAreaTypes() {
		return new LinkedHashSet<DeveloperAreaTypeEnum>( Arrays.asList( DeveloperAreaTypeEnum.values()) );
	}
	
	/**
	 * @return
	 */
	@Override
	public TargetSection getCurrentTransformationSection() {
		return super.getCurrentTransformationSection();
	}
}
