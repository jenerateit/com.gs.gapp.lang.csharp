/**
 *
 */
package com.gs.gapp.generation.csharp.type.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.csharp.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.generation.csharp.writer.CSharpWriter;
import com.gs.gapp.metamodel.csharp.member.CSharpConstructor;
import com.gs.gapp.metamodel.csharp.member.CSharpEvent;
import com.gs.gapp.metamodel.csharp.member.CSharpField;
import com.gs.gapp.metamodel.csharp.member.CSharpIndexer;
import com.gs.gapp.metamodel.csharp.member.CSharpMethod;
import com.gs.gapp.metamodel.csharp.member.CSharpOperator;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.CSharpType;


/**
 * @author mmt
 *
 */
public abstract class CSharpTypeWriter extends CSharpWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpType csharpType;


	@Override
	public void transform(TargetSection ts) {
        CSharpTargetSectionEnum targetSectionEnum = CSharpTargetSectionEnum.get(ts);

		if (targetSectionEnum == null) throw new IllegalArgumentException("no instance of CSharpTargetSectionEnum found for target section '" + ts + "'");

		switch (targetSectionEnum) {
		case USING_DIRECTIVES:
			// nothing to be done here
			break;
		case NAMESPACE:
			// nothing to be done here
			break;

		case TYPE:
			wNL();
			wType(ts);
			break;
		case ENUM_ENTRIES:
			wEnumEntries(ts);
			break;
		case FIELDS:
			if (csharpType.getFields().size() > 0) wNL();
			wFields(ts);
			break;
		case INDEXERS:
			if (csharpType.getIndexers().size() > 0) wNL();
			wIndexers(ts);
			break;
		case CONSTRUCTORS:
			if (csharpType.getConstructors().size() > 0) wNL();
			wConstructors(ts);
			break;
		case PROPERTIES:
			if (csharpType.getProperties().size() > 0) wNL();
			wProperties(ts);
			break;
		case METHODS:
			if (csharpType.getMethods().size() > 0) wNL();
			wMethods(ts);
			break;
		case OPERATORS:
			if (csharpType.getOperators().size() > 0) wNL();
			wOperators(ts);
			break;
		case EVENTS:
			if (csharpType.getEvents().size() > 0) wNL();
			wEvents(ts);
			break;
		case DESTRUCTOR:
			if (csharpType.getDestructor() != null) wNL();
			wDestructor(ts);
			break;
		case NESTED_TYPES:
			if (csharpType.getOwnedTypes().size() > 0) wNL();
			wNestedTypes(ts);
			break;
		case TYPE_END:
			wTypeEnd(ts);
			break;

		case ADDITIONAL_TYPES:
			// nothing to be done here
			break;
		case NESTED_NAMESPACES:
			// nothing to be done here
			break;
		case NAMESPACE_END:
			// nothing to be done here
			break;
		case ADDITIONAL_NAMESPACES:
			// nothing to be done here
			break;
		default:
			throw new WriterException("target section " + ts.getCode() + " is not handled", getTransformationTarget(), this);
		}

	}

	/**
	 * @param ts
	 */
	protected void wEnumEntries(TargetSection ts) {
		// by default nothing is being done here
	}

	/**
	 * @param ts
	 */
	protected void wTypeEnd(TargetSection ts) {
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_TYPES_IN_NAMESPACE_USAGE)) {
			wNL();
			StringBuilder sb = new StringBuilder(csharpType.getQualifiedName(".")).append(".").append(DeveloperAreaTypeEnum.ADDITIONAL_TYPES_IN_NAMESPACE_USAGE.getId());
			bDA(sb.toString());
			eDA();
		}
		
		wNL();
		w("}");
	}

	/**
	 * @param ts
	 */
	protected void wNestedTypes(TargetSection ts) {
		for (CSharpType ownedType : csharpType.getOwnedTypes()) {
			if (ownedType.isGenericParameter()) continue;
			WriterI ownedTypeWriter = this.getTransformationTarget().getWriterInstance(ownedType);
			if (ownedTypeWriter != null) {

				for (TargetSection innerTargetSection : getTransformationTarget().getTargetSections()) {
					CSharpTargetSectionEnum targetSectionEnum = CSharpTargetSectionEnum.get(innerTargetSection);

					if (targetSectionEnum == CSharpTargetSectionEnum.TYPE_END) outdent();

					switch (targetSectionEnum) {
					case USING_DIRECTIVES:
					case NAMESPACE:
					case ADDITIONAL_TYPES:
					case NESTED_NAMESPACES:
					case NAMESPACE_END:
					case ADDITIONAL_NAMESPACES:
						// nothing to be done here
						break;

					case TYPE:
					case TYPE_END:
					case ENUM_ENTRIES:
					case FIELDS:
					case INDEXERS:
					case CONSTRUCTORS:
					case PROPERTIES:
					case METHODS:
					case OPERATORS:
					case EVENTS:
					case DESTRUCTOR:
					case NESTED_TYPES:
						ownedTypeWriter.transform(innerTargetSection);
						break;
					default:
						throw new WriterException("target section " + ts.getCode() + " is not handled", getTransformationTarget(), this);
					}

					if (targetSectionEnum == CSharpTargetSectionEnum.TYPE) indent();
				}
			}
		}
	}

	/**
	 * @param ts
	 */
	protected void wConstructors(TargetSection ts) {
		for (CSharpConstructor constructor : csharpType.getConstructors()) {
			WriterI constructorWriter = this.getTransformationTarget().getWriterInstance(constructor);
			if (constructorWriter != null) constructorWriter.transform(ts);
		}
	}


	/**
	 * @param ts
	 */
	protected void wDestructor(TargetSection ts) {
		if (this.csharpType.getDestructor() != null) {
			WriterI destructorWriter = this.getTransformationTarget().getWriterInstance(this.csharpType.getDestructor());
			if (destructorWriter != null) destructorWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wEvents(TargetSection ts) {
		for (CSharpEvent event : csharpType.getEvents()) {
			WriterI eventWriter = this.getTransformationTarget().getWriterInstance(event);
			if (eventWriter != null) eventWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wOperators(TargetSection ts) {
		for (CSharpOperator operator : csharpType.getOperators()) {
			WriterI operatorWriter = this.getTransformationTarget().getWriterInstance(operator);
			if (operatorWriter != null) operatorWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wMethods(TargetSection ts) {
		for (CSharpMethod method : csharpType.getMethods()) {
			WriterI methodWriter = this.getTransformationTarget().getWriterInstance(method);
			if (methodWriter != null) methodWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wProperties(TargetSection ts) {
		for (CSharpProperty property : csharpType.getProperties()) {
			WriterI propertyWriter = this.getTransformationTarget().getWriterInstance(property);
			if (propertyWriter != null) propertyWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wIndexers(TargetSection ts) {
		for (CSharpIndexer indexer : csharpType.getIndexers()) {
			WriterI indexerWriter = this.getTransformationTarget().getWriterInstance(indexer);
			if (indexerWriter != null) indexerWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wFields(TargetSection ts) {
		for (CSharpField field : csharpType.getFields()) {
			WriterI fieldWriter = this.getTransformationTarget().getWriterInstance(field);
			if (fieldWriter != null) fieldWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wGenericTypeDeclaration(TargetSection ts) {
		wUsings(this.csharpType.getGenericArguments().toArray(new CSharpType[0]));
		String genericTypeDeclaration = this.csharpType.getGenericTypeDeclaration();
		if (genericTypeDeclaration != null) {
			w("<", genericTypeDeclaration, ">");
		}
	}

	protected abstract void wType(TargetSection ts);

}
