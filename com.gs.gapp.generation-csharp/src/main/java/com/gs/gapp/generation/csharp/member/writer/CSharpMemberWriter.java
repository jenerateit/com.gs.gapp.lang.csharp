/**
 * 
 */
package com.gs.gapp.generation.csharp.member.writer;

import com.gs.gapp.generation.csharp.writer.CSharpWriter;
import com.gs.gapp.metamodel.csharp.member.CSharpMember;


/**
 * @author mmt
 *
 */
abstract public class CSharpMemberWriter extends CSharpWriter {
	
	@org.jenerateit.annotation.ModelElement
	private CSharpMember csharpMember;
	
	
}
