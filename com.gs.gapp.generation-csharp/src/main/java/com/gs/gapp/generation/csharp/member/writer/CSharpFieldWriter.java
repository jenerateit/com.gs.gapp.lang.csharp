/**
 *
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;

import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.member.CSharpField;


/**
 * @author mmt
 *
 */
public class CSharpFieldWriter extends CSharpMemberWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpField csharpField;

	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.FIELDS.getTargetSection() == ts) {
	        String blank = "";

			wNL();

			// --- write documentation
			if (StringTools.isText(this.csharpField.getBody())) {
				wXmlTagComment(ts, this.csharpField.getBody());
			}
			
			// --- modifiers
			if (this.csharpField.getAccessModifier() != null) {
				w(blank, this.csharpField.getAccessModifier().getName());
				blank = " ";
			}

			if (this.csharpField.isStatic()) {
				w(blank, CSharpModifier.STATIC.getName());
				blank = " ";
			}

			// --- the field name
			wUsings(this.csharpField.getType());
			w(blank, this.csharpField.getType().getTypeAsString(), " ", this.csharpField.getName(), ";");
			blank = " ";
		}
	}
}
