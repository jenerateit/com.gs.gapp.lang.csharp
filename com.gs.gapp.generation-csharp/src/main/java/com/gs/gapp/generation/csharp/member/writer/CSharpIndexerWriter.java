/**
 * 
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.member.CSharpIndexer;
import com.gs.gapp.metamodel.csharp.member.CSharpParameter;


/**
 * @author mmt
 *
 */
public class CSharpIndexerWriter extends CSharpMemberWriter {
	
	@org.jenerateit.annotation.ModelElement
	private CSharpIndexer csharpIndexer;

	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.INDEXERS.getTargetSection() == ts) {
			wNL();
			wUsings(csharpIndexer.getType());
			
        	w("public ", csharpIndexer.getType().getTypeAsString(), " ", "this");
        	// --- parameters
			String comma = "";
			w("[");
			for (CSharpParameter parameter : this.csharpIndexer.getParameters()) {
				WriterI parameterWriter = this.getTransformationTarget().getWriterInstance(parameter);
				if (parameterWriter != null) {
					w(comma);parameterWriter.transform(ts);
					comma = ", ";
				}
			}
			wNL("]");
        				
        	// --- indexer getter and setter			
        	wNLI("{");
        	    if (this.csharpIndexer.isInInterface()) {
        	    	wNL("get;");
        	    	if (csharpIndexer.isReadOnly() == false) {
        	    	    wNL("set;");
        	    	}
        	    } else {
	        	    wNL("get");
	        	    wNLI("{");
	        	        wIndexerGetter(ts);
	        	    wONL("}");
	        	    if (csharpIndexer.isReadOnly() == false) {
	        	    	wNL("set");
		        	    wNLI("{");
		        	    wIndexerSetter(ts);
		        	    wONL("}");
	        	    }
        	    }
        	wONL("}");
	    }
	}

	protected void wIndexerSetter(TargetSection ts) {
		// TODO Auto-generated method stub
		
	}

	protected void wIndexerGetter(TargetSection ts) {
		// TODO Auto-generated method stub
		
	}
}
