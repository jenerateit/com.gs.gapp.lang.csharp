/**
 *
 */
package com.gs.gapp.generation.csharp.type.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;


/**
 * @author mmt
 *
 */
public class CSharpClassWriter extends CSharpTypeWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpClass csharpClass;


	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.csharp.type.writer.CSharpTypeWriter#wType(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected void wType(TargetSection ts) {
		String blank = "";

		wNL();

		// --- write documentation
		if (StringTools.isText(this.csharpClass.getBody())) {
			wXmlTagComment(ts, this.csharpClass.getBody());
		}
		
		// --- modifiers
		if (this.csharpClass.getAccessModifier() != null) {
			w(blank, this.csharpClass.getAccessModifier().getName());
			blank = " ";
		}

		if (this.csharpClass.isAbstract()) {
			w(blank, CSharpModifier.ABSTRACT.getName());
			blank = " ";
		}

		if (this.csharpClass.isStatic()) {
			w(blank, CSharpModifier.STATIC.getName());
			blank = " ";
		}

		// --- partial
		if (this.csharpClass.isPartial()) {
			w(blank, "partial");
			blank = " ";
		}

		// --- the keyword and name
		w(blank, "class", " ", this.csharpClass.getName());
		blank = " ";

		// --- generic type declarations
		wGenericTypeDeclaration(ts);

		// --- inheritance
		String comma = "";
		String colon = " : ";
		if (this.csharpClass.getBaseType() != null) {
			wUsings(this.csharpClass.getBaseType());
			w(colon, this.csharpClass.getBaseType().getName());
			comma = ", ";
			colon = "";
		}

		if (this.csharpClass.getInterfaces().size() > 0) {
			for (CSharpInterface anInterface : this.csharpClass.getInterfaces()) {
				wUsings(anInterface);
				w(colon, comma, anInterface.getName());
				comma = ", ";
				colon = "";
			}
		}

		wNL();
		w("{");

	}
}
