/**
 *
 */
package com.gs.gapp.generation.csharp.type.writer;

import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.csharp.writer.CSharpWriter;
import com.gs.gapp.metamodel.csharp.member.CSharpParameter;


/**
 * @author mmt
 *
 */
public class CSharpDelegateParameterWriter extends CSharpWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpParameter csharpDelegateParameter;

	@Override
	public void transform(TargetSection ts) {
		wUsings(this.csharpDelegateParameter.getType());
		w(csharpDelegateParameter.getName(), " ", csharpDelegateParameter.getType().getTypeAsString());

	}
}
