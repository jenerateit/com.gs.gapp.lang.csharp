/**
 * 
 */
package com.gs.gapp.generation.csharp.type.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.generation.csharp.writer.CSharpWriter;
import com.gs.gapp.metamodel.csharp.type.CSharpUsingDirective;


/**
 * @author mmt
 *
 */
public class CSharpUsingDirectiveWriter extends CSharpWriter {
	
	@org.jenerateit.annotation.ModelElement
	private CSharpUsingDirective csharpUsingDirective;

	@Override
	public void transform(TargetSection ts) {

		if (ts == CSharpTargetSectionEnum.USING_DIRECTIVES.getTargetSection()) {
			
			String directiveString = this.csharpUsingDirective.getDirectiveAsString();
			if (directiveString != null) {
				wNL();
				w("using ", directiveString, ";");
			} else {
				throw new WriterException("found a using-directive that results in no directive to be written to the C# source file", this.getTransformationTarget(), this);
			}
		}
	}
}
