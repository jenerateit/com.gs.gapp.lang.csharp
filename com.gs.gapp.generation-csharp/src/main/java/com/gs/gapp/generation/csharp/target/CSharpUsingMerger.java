/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */

package com.gs.gapp.generation.csharp.target;

import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;

/**
 * Helper class to merge using statements from a previous {@link CSharpTargetDocument} into a new {@link CSharpTargetDocument}.
 * 
 * @see AbstractTarget#addTargetLifecycleListener(TargetLifecycleListenerI)
 * @author hrr
 */
public final class CSharpUsingMerger implements TargetLifecycleListenerI {

	/**
	 * Not implemented.
	 * 
	 * @param target the target to work on
	 * @see org.jenerateit.target.TargetLifecycleListenerI#preTransform(org.jenerateit.target.TargetI)
	 */
	public void preTransform(TargetI<?> target) {

	}

	/**
	 * Merges the import statement from the given target.
	 * 
	 * @param target the target to work on
	 * @see org.jenerateit.target.TargetLifecycleListenerI#postTransform(org.jenerateit.target.TargetI)
	 */
	public void postTransform(TargetI<?> target) {
		
		/* TODO implement this for the CSharp Metamodel
		if (!JavaTarget.class.isInstance(target)) {
			throw new JenerateITException("Got a postTransform(TargetI) event with a target of type '" +
					target.getClass() + "' but I expect a target of type '" + JavaTarget.class + "'");
			
		} else {
			WriterI writer = target.getBaseWriter();
			if (!JavaWriter.class.isInstance(writer)) {
				throw new JenerateITException("Got a postTransform(TargetI) event with a writer of type '" +
						writer.getClass() + "' but I expect a target of type '" + JavaWriter.class + "'");

			} else {
				JavaTarget jt = (JavaTarget) target;
				JavaWriter jw = (JavaWriter) writer;
				JavaTargetDocument newJtd = jt.getNewTargetDocument();
				if (newJtd == null) {
					throw new JenerateITException("There is no new java target document to merge into");
					
				} else {
					JavaTargetDocument prevJtd = jt.getPreviousTargetDocument();
					if (prevJtd == null) {
						logger.warn("There is no previous java target document '" + jt.getTargetURI() + "' to merge from. Is this the first run?");
					
					} else {
						Set<String> prevImports = prevJtd.getImports();
						// check if there are any import statements in the previous file
						if (prevImports != null && !prevImports.isEmpty()) {
							Set<String> missingImports = new LinkedHashSet<String>(prevImports);
							// remove all known imports from the previous file imports
							missingImports.removeAll(newJtd.getImports());
							
							if (!missingImports.isEmpty()) {
								jw.wBlockComment(JavaTargetDocument.IMPORTS, "Imports from last generation");
								for (String i : missingImports) {
									jw.wImport(i);
								}
								jw.wNL(JavaTargetDocument.IMPORTS);
							}
						}
					}
				}
			}
		}
		*/

	}

	@Override
	public void onFound(TargetI<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onLoaded(TargetI<?> arg0) {
		// TODO Auto-generated method stub
		
	}
}
