/**
 *
 */
package com.gs.gapp.generation.csharp;

import java.util.Set;

import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.AbstractWriterLocator;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.generation.csharp.attribute.writer.CSharpAttributeUsageWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpConstructorWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpDestructorWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpEnumEntryWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpEventWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpFieldWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpIndexerWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpMethodWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpOperatorWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpParameterWriter;
import com.gs.gapp.generation.csharp.member.writer.CSharpPropertyWriter;
import com.gs.gapp.generation.csharp.target.CSharpSourceFileTarget;
import com.gs.gapp.generation.csharp.type.writer.CSharpClassWriter;
import com.gs.gapp.generation.csharp.type.writer.CSharpDelegateParameterWriter;
import com.gs.gapp.generation.csharp.type.writer.CSharpDelegateWriter;
import com.gs.gapp.generation.csharp.type.writer.CSharpEnumWriter;
import com.gs.gapp.generation.csharp.type.writer.CSharpInterfaceWriter;
import com.gs.gapp.generation.csharp.type.writer.CSharpStructWriter;
import com.gs.gapp.generation.csharp.type.writer.CSharpUsingDirectiveWriter;
import com.gs.gapp.generation.csharp.writer.CSharpNamespaceUsageWriter;
import com.gs.gapp.generation.csharp.writer.CSharpSourceFileWriter;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;
import com.gs.gapp.metamodel.csharp.CSharpSourceFile;
import com.gs.gapp.metamodel.csharp.attribute.CSharpAttributeUsage;
import com.gs.gapp.metamodel.csharp.member.CSharpConstructor;
import com.gs.gapp.metamodel.csharp.member.CSharpDestructor;
import com.gs.gapp.metamodel.csharp.member.CSharpEnumEntry;
import com.gs.gapp.metamodel.csharp.member.CSharpEvent;
import com.gs.gapp.metamodel.csharp.member.CSharpField;
import com.gs.gapp.metamodel.csharp.member.CSharpIndexer;
import com.gs.gapp.metamodel.csharp.member.CSharpMethod;
import com.gs.gapp.metamodel.csharp.member.CSharpOperator;
import com.gs.gapp.metamodel.csharp.member.CSharpParameter;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpDelegate;
import com.gs.gapp.metamodel.csharp.type.CSharpDelegateParameter;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpUsingDirective;

/**
 * @author mmt
 *
 */
public class WriterLocatorCSharp extends AbstractWriterLocator {

	public WriterLocatorCSharp(Set<Class<? extends TargetI<?>>> targetClasses) {
		super(targetClasses);

		// --- mappers for generation decision
	    addWriterMapperForGenerationDecision( new WriterMapper(CSharpSourceFile.class, CSharpSourceFileTarget.class, CSharpSourceFileWriter.class) );

	    // --- mappers for writer delegation
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpNamespaceUsage.class, CSharpSourceFileTarget.class, CSharpNamespaceUsageWriter.class) );

	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpClass.class, CSharpSourceFileTarget.class, CSharpClassWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpInterface.class, CSharpSourceFileTarget.class, CSharpInterfaceWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpDelegate.class, CSharpSourceFileTarget.class, CSharpDelegateWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpEnum.class, CSharpSourceFileTarget.class, CSharpEnumWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpStruct.class, CSharpSourceFileTarget.class, CSharpStructWriter.class) );

	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpConstructor.class, CSharpSourceFileTarget.class, CSharpConstructorWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpDestructor.class, CSharpSourceFileTarget.class, CSharpDestructorWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpEvent.class, CSharpSourceFileTarget.class, CSharpEventWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpEnumEntry.class, CSharpSourceFileTarget.class, CSharpEnumEntryWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpField.class, CSharpSourceFileTarget.class, CSharpFieldWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpIndexer.class, CSharpSourceFileTarget.class, CSharpIndexerWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpMethod.class, CSharpSourceFileTarget.class, CSharpMethodWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpOperator.class, CSharpSourceFileTarget.class, CSharpOperatorWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpProperty.class, CSharpSourceFileTarget.class, CSharpPropertyWriter.class) );

	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpDelegateParameter.class, CSharpSourceFileTarget.class, CSharpDelegateParameterWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpParameter.class, CSharpSourceFileTarget.class, CSharpParameterWriter.class) );
	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpAttributeUsage.class, CSharpSourceFileTarget.class, CSharpAttributeUsageWriter.class) );

	    addWriterMapperForWriterDelegation( new WriterMapper(CSharpUsingDirective.class, CSharpSourceFileTarget.class, CSharpUsingDirectiveWriter.class) );
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.AbstractWriterLocator#getWriterClass(java.io.Serializable, java.lang.Class)
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object element,
			Class<? extends TargetI<?>> targetClass) {
		Class<? extends WriterI> result = super.getWriterClass(element, targetClass);

		// TODO add logic here if there are elements where there should be something generated for, but not in a separate file
//		if (result != null && element instanceof JavaTypeI) {
//			if (((JavaTypeI)element).getOwningJavaType() != null) {
//				// inner class, inner interface, etc.
//				result = null;
//			}
//		}

		return result;
	}
}
