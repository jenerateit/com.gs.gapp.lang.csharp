/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.csharp;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;


/**
 * @author hrr
 *
 */
public class GenerationGroupCSharpProvider implements GenerationGroupProviderI {

	/**
	 * 
	 */
	public GenerationGroupCSharpProvider() {}

    
	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupCSharp();
	}
}
