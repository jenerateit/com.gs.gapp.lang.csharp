/**
 *
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.csharp.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.member.CSharpConstructor;
import com.gs.gapp.metamodel.csharp.member.CSharpParameter;


/**
 * @author mmt
 *
 */
public class CSharpConstructorWriter extends CSharpMemberWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpConstructor csharpConstructor;

	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.CONSTRUCTORS.getTargetSection() == ts) {
	        String blank = "";

			wNL();

			// --- write documentation
			if (StringTools.isText(this.csharpConstructor.getBody())) {
				wXmlTagComment(ts, this.csharpConstructor.getBody());
				for (final CSharpParameter parameter : this.csharpConstructor.getParameters()) {
					w(ts, "/// <param name=\"", parameter.getName(), "\">");
					if (StringTools.isText(parameter.getBody())) {
						w(ts, parameter.getBody());
					}
					wNL(ts, "</param>");
				}
			}
			
			// --- modifiers
			if (this.csharpConstructor.isStatic()) {
				w(blank, CSharpModifier.STATIC.getName());
				blank = " ";
			} else if (this.csharpConstructor.getAccessModifier() != null) {
				w(blank, this.csharpConstructor.getAccessModifier().getName());
				blank = " ";
			}

			// --- the constructor name
			w(blank, this.csharpConstructor.getName());
			blank = " ";

			// --- parameters
			String comma = "";
			w("(");
			if (this.csharpConstructor.isStatic() == false) {
				for (CSharpParameter parameter : this.csharpConstructor.getParameters()) {
					WriterI parameterWriter = this.getTransformationTarget().getWriterInstance(parameter);
					if (parameterWriter != null) {
						w(comma);parameterWriter.transform(ts);
						comma = ", ";
					} else {
						throw new WriterException("no writer found for parameter '" + parameter + "'", getTransformationTarget(), this);
					}
				}
			}
			w(")");

			// --- constructor body
			wNL();
			wNLI("{");
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.CONSTRUCTOR_BODY)) {
			    	StringBuffer sb = new StringBuffer(this.csharpConstructor.getOwner().getQualifiedName(".")).append(".").append(this.csharpConstructor.getName());
					for (CSharpParameter param : this.csharpConstructor.getParameters()) {
						sb.append(".").append(param.getType().getName());
					}
				    bDA(sb.toString().replace("[]", ".ARRAY"));
				    wConstructorBody(ts);
				    eDA();
			    } else {
			    	wConstructorBody(ts);
			    }
			wONL("}");
		}
	}

	/**
	 * @param ts
	 */
	protected void wConstructorBody(TargetSection ts) {
		// TODO Auto-generated method stub
	}
}
