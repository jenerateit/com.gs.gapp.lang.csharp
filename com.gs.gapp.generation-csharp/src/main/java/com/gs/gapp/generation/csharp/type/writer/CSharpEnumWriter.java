/**
 * 
 */
package com.gs.gapp.generation.csharp.type.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.csharp.DeveloperAreaTypeEnum;
import com.gs.gapp.metamodel.csharp.member.CSharpEnumEntry;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;


/**
 * @author mmt
 *
 */
public class CSharpEnumWriter extends CSharpTypeWriter {
	
	@org.jenerateit.annotation.ModelElement
	private CSharpEnum csharpEnum;

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.csharp.type.writer.CSharpTypeWriter#wType(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected void wType(TargetSection ts) {
		String blank = "";
		
		wNL();
		
		// --- write documentation
		if (StringTools.isText(this.csharpEnum.getBody())) {
			wXmlTagComment(ts, this.csharpEnum.getBody());
		}
		
		// --- modifiers
		if (this.csharpEnum.getAccessModifier() != null) {
			w(blank, this.csharpEnum.getAccessModifier().getName());
			blank = " ";
		}
		
		// --- the keyword and name
		w(blank, "enum", " ", this.csharpEnum.getName());
		blank = " ";
		
		wNL();
		w("{");
		
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.csharp.type.writer.CSharpTypeWriter#wEnumEntries(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected void wEnumEntries(TargetSection ts) {
		super.wEnumEntries(ts);
		
		int numberOfEnums = csharpEnum.getEntries().size();
		int counter = 0;
		for (CSharpEnumEntry entry : csharpEnum.getEntries()) {
			counter++;
			WriterI writer = this.getTransformationTarget().getWriterInstance(entry);
			if (writer != null) writer.transform(ts);
			if (counter < numberOfEnums) {
				w(",");
			} else {
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ENUMERATION_ENTRIES)) {
					bDA(csharpEnum.getName() + "." + DeveloperAreaTypeEnum.ENUMERATION_ENTRIES.getId());
					eDA();
				}
			}
		}	
	}
}
