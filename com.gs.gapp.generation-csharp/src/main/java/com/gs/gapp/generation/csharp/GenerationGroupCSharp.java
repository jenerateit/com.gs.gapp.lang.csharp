/**
 * 
 */
package com.gs.gapp.generation.csharp;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.WriterLocatorI;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.generation.csharp.target.CSharpSourceFileTarget;
import com.gs.gapp.metamodel.csharp.CSharpModelElementCache;

/**
 * @author rt
 *
 */
public class GenerationGroupCSharp extends AbstractGenerationGroup implements GenerationGroupConfigI {
	
	private final WriterLocatorI writerLocator;
	
	/**
	 * 
	 */
	public GenerationGroupCSharp() {
		super(new CSharpModelElementCache());
		
		addTargetClass(CSharpSourceFileTarget.class);
		
		writerLocator = new WriterLocatorCSharp(getAllTargets());
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.util.DetailsI#getName()
	 */
	@Override
	public String getName() {
		return "CSharp";
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.util.DetailsI#getDescription()
	 */
	@Override
	public String getDescription() {
		return getName();
	}

	@Override
	public WriterLocatorI getWriterLocator() {
		return this.writerLocator;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.AbstractGenerationGroup#getModelElementCache()
	 */
	@Override
	public CSharpModelElementCache getModelElementCache() {
        return (CSharpModelElementCache) super.getModelElementCache();
	}
}
