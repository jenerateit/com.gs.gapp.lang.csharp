package com.gs.gapp.generation.csharp;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.basic.options.GenerationGroupOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;

public class GenerationGroupCSharpOptions extends GenerationGroupOptions {
	
	public static final OptionDefinitionString OPTION_DEF_AREA_RULE =
			new OptionDefinitionString("developer-area-rule",
					                   "determines the rule for developer areas", false, null,
					                   DeveloperAreaRule.getNames());

	public GenerationGroupCSharpOptions(AbstractWriter writer) {
		super(writer);
	}

	public GenerationGroupCSharpOptions(BasicTextTarget<?> target) {
		super(target);
	}
	
	
	/**
	 * Affects the way developer area support is going to be gene generated.
	 * Using the option 'developer-area-rule', you can switch on or off all available developer areas.
	 * 
	 * @return
	 */
	public DeveloperAreaRule getDeveloperAreaRule() {
		DeveloperAreaRule defaultSetting = DeveloperAreaRule.DEFAULT;
		Serializable developerAreaRuleString = getOptionValue(OptionDefinitionEnum.OPTION_DEVELOPER_AREA_RULE.getName());
		
		if (developerAreaRuleString != null) {
			DeveloperAreaRule result = null;
			try {
		        result = DeveloperAreaRule.valueOf(developerAreaRuleString.toString().toUpperCase());
		        return result;
			} catch (IllegalArgumentException ex) {
				// eat it up - there is no enum entry that matches the given string
				return defaultSetting;
			}
		} else {
			return defaultSetting;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.GenerationGroupOptions#getOptionDefinitions()
	 */
	@Override
	public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
		Set<OptionDefinition<? extends Serializable>> result = super.getOptionDefinitions();
		result.addAll(OptionDefinitionEnum.getDefinitions());
		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.GenerationGroupOptions#getDefaultTargetPrefix()
	 */
	@Override
	protected String getDefaultTargetPrefix() {
		return "/";
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.GenerationGroupOptions#getDefaultTargetPrefixForTests()
	 */
	@Override
	protected String getDefaultTargetPrefixForTests() {
		return "/";
	}


	/**
	 * @author mmt
	 *
	 */
	public static enum OptionDefinitionEnum {

		
		OPTION_DEVELOPER_AREA_RULE ( OPTION_DEF_AREA_RULE ),
		;

		private static final Map<String, OptionDefinitionEnum> stringToEnum = new HashMap<String, OptionDefinitionEnum>();

		static {
			for (OptionDefinitionEnum m : values()) {
				stringToEnum.put(m.getName(), m);
			}
		}
		
		/**
		 * @return
		 */
		public static Set<OptionDefinition<? extends Serializable>> getDefinitions() {
			Set<OptionDefinition<? extends Serializable>> result = new LinkedHashSet<OptionDefinition<? extends Serializable>>();
			for (OptionDefinitionEnum m : values()) {
				result.add(m.getDefinition());
			}
			return result;
		}

		/**
		 * @param datatypeName
		 * @return
		 */
		public static OptionDefinitionEnum fromString(String datatypeName) {
			return stringToEnum.get(datatypeName);
		}

		private final OptionDefinition<? extends Serializable> definition;
		
		private OptionDefinitionEnum(OptionDefinition<? extends Serializable> definition) {
			this.definition = definition;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return this.getDefinition().getName();
		}

		public OptionDefinition<? extends Serializable> getDefinition() {
			return definition;
		}
	}
}
