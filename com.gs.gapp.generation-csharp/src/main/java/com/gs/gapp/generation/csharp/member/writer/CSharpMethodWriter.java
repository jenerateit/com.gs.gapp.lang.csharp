/**
 *
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.csharp.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.member.CSharpMethod;
import com.gs.gapp.metamodel.csharp.member.CSharpParameter;
import com.gs.gapp.metamodel.csharp.type.CSharpType;


/**
 * @author mmt
 *
 */
public class CSharpMethodWriter extends CSharpMemberWriter {
	
	public static String getDefaultValue(CSharpType type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");
		String result = null;
		
		if (type == com.gs.vd.metamodel.csharp.predef.System.String.TYPE) {
			result = "\"\"";
		} else if (type == com.gs.vd.metamodel.csharp.predef.System.Int16.TYPE ||
				type == com.gs.vd.metamodel.csharp.predef.System.Int32.TYPE ||
				type == com.gs.vd.metamodel.csharp.predef.System.Int64.TYPE ||
				type == com.gs.vd.metamodel.csharp.predef.System.UInt16.TYPE ||
				type == com.gs.vd.metamodel.csharp.predef.System.UInt32.TYPE ||
				type == com.gs.vd.metamodel.csharp.predef.System.UInt64.TYPE) {
			result = "0";
		} else if (type == com.gs.vd.metamodel.csharp.predef.System.Double.TYPE ||
				type == com.gs.vd.metamodel.csharp.predef.System.Decimal.TYPE ||
				type == com.gs.vd.metamodel.csharp.predef.System.Single.TYPE) {
			result = "0.0";
		} else if (type == com.gs.vd.metamodel.csharp.predef.System.DateTime.TYPE) {
			result = "DateTime.Today";
		} else if (type == com.gs.vd.metamodel.csharp.predef.System.Boolean.TYPE) {
			result = "true";
		} else if (type == com.gs.vd.metamodel.csharp.predef.System.Byte.TYPE ||
				type == com.gs.vd.metamodel.csharp.predef.System.SByte.TYPE) {
			result = "0";
		} else if (type == com.gs.vd.metamodel.csharp.predef.System.Char.TYPE) {
			result = "'0'";
		}
				
		
		return result;
	}

	@org.jenerateit.annotation.ModelElement
	private CSharpMethod csharpMethod;

	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.METHODS.getTargetSection() == ts) {

			wNL();

			// --- write documentation
			if (StringTools.isText(this.csharpMethod.getBody())) {
				wXmlTagComment(ts, this.csharpMethod.getBody());
				for (final CSharpParameter parameter : this.csharpMethod.getParameters()) {
					w(ts, "/// <param name=\"", parameter.getName(), "\">");
					if (StringTools.isText(parameter.getBody())) {
						w(ts, parameter.getBody());
					}
					wNL(ts, "</param>");
				}
				// TODO return type?
			}
			
			if (this.csharpMethod.isInInterface()) {
				// --- the type
				wUsings(this.csharpMethod.getType());
				w(this.csharpMethod.getType().getTypeAsString(), " ", this.csharpMethod.getName());

				// --- generic type parameters
				String genericTypeDeclaration = this.csharpMethod.getGenericTypeDeclaration();
				if (genericTypeDeclaration != null) {
					wUsings(this.csharpMethod.getGenericArguments().toArray(new CSharpType[0]));
				    w("<", genericTypeDeclaration, ">");
				}

				// --- parameters
				String comma = "";
				w("(");
				for (CSharpParameter parameter : this.csharpMethod.getParameters()) {
					WriterI parameterWriter = this.getTransformationTarget().getWriterInstance(parameter);
					if (parameterWriter != null) {
						w(comma);parameterWriter.transform(ts);
						comma = ", ";
					} else {
						throw new WriterException("no writer found for parameter '" + parameter + "'", getTransformationTarget(), this);
					}
				}
				w(");");
			} else {
				String blank = "";
				// --- modifiers
				if (this.csharpMethod.getAccessModifier() != null) {
					w(blank, this.csharpMethod.getAccessModifier().getName());
					blank = " ";
				}


				if (this.csharpMethod.isAbstract()) {
					w(blank, CSharpModifier.ABSTRACT.getName());
					blank = " ";
				}

				if (this.csharpMethod.isStatic() || this.csharpMethod.isExtensionMethod()) {
					w(blank, CSharpModifier.STATIC.getName());
					blank = " ";
				}

				// --- the type
				wUsings(this.csharpMethod.getType());
				w(blank, this.csharpMethod.getType().getTypeAsString());
				blank = " ";

				// --- the method name
				w(blank, this.csharpMethod.getName());
				blank = " ";

				// --- generic type parameters
				String genericTypeDeclaration = this.csharpMethod.getGenericTypeDeclaration();
				if (genericTypeDeclaration != null) {
					wUsings(this.csharpMethod.getGenericArguments().toArray(new CSharpType[0]));
				    w("<", genericTypeDeclaration, ">");
				}

				// --- parameters
				String comma = "";
				w("(", csharpMethod.isExtensionMethod() ? "this " : "");
				for (CSharpParameter parameter : this.csharpMethod.getParameters()) {
					WriterI parameterWriter = this.getTransformationTarget().getWriterInstance(parameter);
					if (parameterWriter != null) {
						w(comma);parameterWriter.transform(ts);
						comma = ", ";
					} else {
						throw new WriterException("no writer found for parameter '" + parameter + "'", getTransformationTarget(), this);
					}
				}
				w(")");

				// --- method body
				if (csharpMethod.isInInterface()) {
					wNL(";");
				} else {
					wNL();
					wNLI("{");
					    if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.METHOD_BODY)) {
					    	StringBuffer sb = new StringBuffer(this.csharpMethod.getOwner().getQualifiedName(".")).append(".").append(this.csharpMethod.getName());
							for (CSharpParameter param : this.csharpMethod.getParameters()) {
								sb.append(".").append(param.getType().getName());
							}
						    bDA(sb.toString().replace("[]", ".ARRAY"));
						    wMethodBody(ts);
						    eDA();
					    } else {
					    	wMethodBody(ts);
					    }
					wONL("}");
				}
			}
		}
	}

	/**
	 * @param ts
	 */
	protected void wMethodBody(TargetSection ts) {
		wDefaultImplementation(ts);
	}
	
	/**
	 * @param ts
	 * @return true if the method has written something to the method body
	 */
	protected boolean wDefaultImplementation(TargetSection ts) {
		boolean returnStatementWritten = false;
		
		// --- 0. check for method body content that was provided in a model converter
		if (csharpMethod.getMethodBody() != null && csharpMethod.getMethodBody().length() > 0) {
			wNL(csharpMethod.getMethodBody());
			returnStatementWritten = true;
		}
		
		// --- 1. check for return of default value
		if (returnStatementWritten == false) {
			if (csharpMethod.getType() == null ||
					csharpMethod.getType() == com.gs.vd.metamodel.csharp.predef.System.Void.TYPE) {
				wNL("return;");
			} else {
    			String defaultValue = getDefaultValue(csharpMethod.getType());
    			if (defaultValue == null || defaultValue.length() == 0) {
    			    wNL("return null;");
    			} else {
    				wNL("return ", defaultValue, ";");
    			}
			}
			returnStatementWritten = true;
		}
		
		return returnStatementWritten;
	}
}
