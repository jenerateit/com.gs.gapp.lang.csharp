/**
 *
 */
package com.gs.gapp.generation.csharp.writer;

import java.util.Set;

import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.target.TextTargetI;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.basic.writer.ModelElementWriter;
import com.gs.gapp.generation.csharp.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.csharp.GenerationGroupCSharpOptions;
import com.gs.gapp.generation.csharp.target.CSharpSourceFileTarget;
import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpModelElement;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpGenericParameterAttributes;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;

/**
 * @author mmt
 *
 */
public abstract class CSharpWriter extends ModelElementWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpModelElement csharpModelElement;

	private GenerationGroupCSharpOptions generationGroupOptions;

	private boolean exclude(CSharpType type) {
		boolean result = false;

		// TODO find out whether we really need an exclusion logic
//		if (type.isPrimitive()) {
//			result = true;
//		} else if (this.csharpModelElement == type || getTransformationTarget().getBaseWriter().getElement() == type) {
//            result = true;
//		}

		return result;
	}

	/**
	 * @param types
	 * @return
	 */
	public CSharpWriter wUsings(CSharpType... types) {
	    if (types != null) {
	    	for (CSharpType type : types) {

	    		if (exclude(type)) continue;

	    		CSharpNamespace ns = type.getNamespace();
	    		if (ns == null) {
	    			ns = type.getNamespaceUsage() == null ? null : type.getNamespaceUsage().getNamespace();
	    		}
	    		if (ns == null) continue;

	    		// TODO write test models for these cases
		    	if (type.isGenericParameter()) {
	    			if (type.getOwningType().isGenericTypeDefinition()) {
	    				CSharpTypeParameter typeParameter = (CSharpTypeParameter) type;
	    				for (@SuppressWarnings("unused") CSharpGenericParameterAttributes attr : typeParameter.getGenericParameterAttributes()) {
	    					// TODO write usings here?
	    				}
	    			}
		    	} else if (type.isGenericTypeDefinition()) {
	    			wUsing(ns.getQualifiedName("."));
	    			if (type.getGenericArgumentsForTypeOnly().size() > 0) {
	    				for (CSharpType genericParameterType : type.getGenericArgumentsForTypeOnly()) {
	    					wUsings(genericParameterType);
	    				}
	    			}
	    		} else if (type.isGenericType()) {
	    			wUsing(ns.getQualifiedName("."));
	    			for (CSharpType genericParameterType : type.getGenericArguments()) {
	    				wUsings(genericParameterType);
	    			}
	    		} else {
	    			wUsing(ns.getQualifiedName("."));
	    		}



	    		/* TODO maybe this snippet can help to improve the above code
	    		if (type.isGenericType()) {
	    			// TODO write test models for these cases
	    			if (type.isGenericTypeDefinition()) {
	    				wImport(type.getQualifiedName("."));

	    			} else if (type.isGenericParameter()) {
                        CSharpTypeParameter typeParameter = (CSharpTypeParameter) type;
                        wImport(type.getQualifiedName("."));
                        for (CSharpType genericParameterConstraintType : typeParameter.getGenericParameterConstraints()) {
                        	wUsings(genericParameterConstraintType);
                        }
	    			} else if (type.isConstructedType()) {
	    				CSharpConstructedGenericType constructedGenericType = (CSharpConstructedGenericType) type;
	    				for (CSharpType genericArgument : constructedGenericType.getGenericArguments()) {
	    					wUsings(genericArgument);
	    				}
	    			}
	    		} else {
	    		    wImport(type.getQualifiedName("."));
	    		}
	    		*/
	    	}
	    }

		return this;
	}


	/**
	 * @param usingDirectives
	 * @return
	 */
	public CSharpWriter wUsing(String... usingDirectives) {
		checkTarget();

		Set<String> currentUsingDirectives = CSharpSourceFileTarget.class.cast(getTextTransformationTarget()).getNewTargetDocument().getSectionUsing();

		for (String usingDirective : usingDirectives) {
			if (StringTools.isText(usingDirective)) {
				if (!currentUsingDirectives.contains(usingDirective)) {
					currentUsingDirectives.add(usingDirective);
					wNL(CSharpTargetSectionEnum.USING_DIRECTIVES.getTargetSection(), "using ", usingDirective, ";");
				}
			}
		}
		return this;
	}

	/**
	 * Checks if the current target in the transformation context is of type {@link CSharpSourceFileTarget}
	 */
	private void checkTarget() {
		if (!CSharpSourceFileTarget.class.isInstance(getTransformationTarget())) {
			throw new WriterException("The target '" + getTransformationTarget().getClass().getName() +
					"' in the TransformationContext is not of type '" +
					CSharpSourceFileTarget.class.getName() + "'", getTransformationTarget(), this);
		}
	}

	
	/**
	 * Write block comment(s) to the current working target section.
	 * <b>Note:</b> The block comment character will be added in front and the comment block.
	 * If the line has line separators each line will be added separately within the comment block.
	 *
	 * @param comments the comment(s) to write
	 * @return an instance of this writer object
	 */
	public CSharpWriter wBlockComment(final CharSequence... comments) {
		return this.wBlockComment(null, comments);
	}
	
	/**
	 * Write block comment(s) to the given target section ts.
	 * If ts is null the current working target section is used to write to.
	 * <b>Note:</b> The block comment character will be added in front and the comment block.
	 * If the line has line separators each line will be added separately within the comment block.
	 *
	 * @param comments the comment(s) to write
	 * @param ts the target section to write to
	 * @return an instance of this writer object
	 */
	public CSharpWriter wBlockComment(final TargetSection ts, final CharSequence... comments) {
		if (comments == null || comments.length == 0) {
			// nothing to be done here
		} else {
			final TargetI<?> t = getTransformationTarget();
			if (TextTargetI.class.isInstance(t)) {
				if (ts != null) {
					wNL(ts, "/*");
					for (final CharSequence c : comments) {
						for (CharSequence s : getLines(c)) {
							wNL(ts, " * ", s.toString().replace("/*", StringTools.EMPTY_STRING).replace("*/", StringTools.EMPTY_STRING));
						}
					}
					wNL(ts, " */");
				} else {
					wNL("/*");
					for (final CharSequence c : comments) {
						for (CharSequence s : getLines(c)) {
							wNL(" * ", s.toString().replace("/*", StringTools.EMPTY_STRING).replace("*/", StringTools.EMPTY_STRING));
						}
					}
					wNL(" */");
				}
			} else {
				throw new WriterException("Can not write a comment in a Target of type '" + t.getClass().getName() + "'", t, this);
			}
		}
		return this;
	}

	/**
	 * Write XML tag comment to the current active target section.
	 * <b>Note:</b> The XML tag comment sequence will be added.
	 * If the line has line separators each line will be added separately within the XML tag comment.
	 *
	 * @param comments the comment(s) to write
	 * @return an instance of this writer object
	 */
	public CSharpWriter wXmlTagComment(final CharSequence... comments) {
		return this.wXmlTagComment(null, comments);
	}
	
	/**
	 * Write XML tag comment to the given target section ts.
	 * If ts is null the current working target section is used to write to.
	 * <b>Note:</b> The XML tag comment sequence will be added.
	 * If the line has line separators each line will be added separately within the XML tag comment.
	 *
	 * @param comments the comment(s) to write
	 * @param ts the target section to write to
	 * @return an instance of this writer object
	 */
	public CSharpWriter wXmlTagComment(final TargetSection ts, final CharSequence... comments) {
		if (comments == null || comments.length == 0) {
			// nothing to be done here
		} else {
			final TargetI<?> t = getTransformationTarget();
			if (TextTargetI.class.isInstance(t)) {
				if (ts != null) {
					wNL(ts, "/// <summary>");
					for (final CharSequence c : comments) {
						for (CharSequence s : getLines(c)) {
							wNL(ts, "/// ", s);
						}
					}
					wNL(ts, "/// </summary>");
				} else {
					wNL("/// <summary>");
					for (final CharSequence c : comments) {
						for (CharSequence s : getLines(c)) {
							wNL("/// ", s);
						}
					}
					wNL("/// </summary>");
				}
			} else {
				throw new WriterException("Can not write a comment in a Target of type '" + t.getClass().getName() + "'", t, this);
			}
		}
		return this;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.writer.ModelElementWriter#getGenerationGroupOptions()
	 */
	@Override
	public GenerationGroupCSharpOptions getGenerationGroupOptions() {
		if (this.generationGroupOptions == null) {
			this.generationGroupOptions = new GenerationGroupCSharpOptions(this);
		}
		
		return this.generationGroupOptions;
	}
	
	/**
	 * @param devAreaType the developer area type for which it is checked, whether the currently generating target allowing to use it
	 * @return true, if the given developer area type is active according to the settings in the target
	 */
	protected boolean isDeveloperAreaTypeActive(DeveloperAreaTypeEnum devAreaType) {
		if (devAreaType == null) throw new NullPointerException("parameter 'devArea' must not be null");
		
		boolean result = false;
		
		switch (this.getGenerationGroupOptions().getDeveloperAreaRule()) {
		case ALL:
			result = true;
			break;
		case DEFAULT:
			if (getTransformationTarget() instanceof CSharpSourceFileTarget) {
				CSharpSourceFileTarget csharpTarget = (CSharpSourceFileTarget) getTransformationTarget();
				result = csharpTarget.getActiveDeveloperAreaTypes().contains(devAreaType);
			}
			break;
		case NONE:
			result = false;
			break;
		default:
			throw new WriterException("unhandled developer area rule '" + this.getGenerationGroupOptions().getDeveloperAreaRule() + "' found", getTransformationTarget(), this);
		}
		
		return result;
	}
}
