/**
 *
 */
package com.gs.gapp.generation.csharp.type.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.csharp.type.CSharpInterface;


/**
 * @author mmt
 *
 */
public class CSharpInterfaceWriter extends CSharpTypeWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpInterface csharpInterface;

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.csharp.type.writer.CSharpTypeWriter#wType(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected void wType(TargetSection ts) {
		String blank = "";

		wNL();

		// --- write documentation
		if (StringTools.isText(this.csharpInterface.getBody())) {
			wXmlTagComment(ts, this.csharpInterface.getBody());
		}
		
		// --- modifiers
		if (this.csharpInterface.getAccessModifier() != null) {
			w(blank, this.csharpInterface.getAccessModifier().getName());
			blank = " ";
		}

		// --- partial
		if (this.csharpInterface.isPartial()) {
			w(blank, "partial");
			blank = " ";
		}

		// --- the keyword and name
		w(blank, "interface", " ", this.csharpInterface.getName());
		blank = " ";

		// --- generic type declarations
        wGenericTypeDeclaration(ts);

		// --- inheritance
		String comma = "";
		String colon = " : ";
		if (this.csharpInterface.getInterfaces().size() > 0) {
			for (CSharpInterface anInterface : this.csharpInterface.getInterfaces()) {
				wUsings(anInterface);
				w(colon, comma, anInterface.getName());
				comma = ", ";
				colon = "";
			}
		}

		wNL();
		w("{");

	}
}
