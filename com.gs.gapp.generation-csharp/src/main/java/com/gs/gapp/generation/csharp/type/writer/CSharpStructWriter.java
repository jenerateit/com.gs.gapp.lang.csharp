/**
 *
 */
package com.gs.gapp.generation.csharp.type.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;


/**
 * @author mmt
 *
 */
public class CSharpStructWriter extends CSharpTypeWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpStruct csharpStruct;

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.csharp.type.writer.CSharpTypeWriter#wType(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected void wType(TargetSection ts) {
		String blank = "";

		wNL();

		// --- write documentation
		if (StringTools.isText(this.csharpStruct.getBody())) {
			wXmlTagComment(ts, this.csharpStruct.getBody());
		}
		
		// --- modifiers
		if (this.csharpStruct.getAccessModifier() != null) {
			w(blank, this.csharpStruct.getAccessModifier().getName());
			blank = " ";
		}

		// --- partial
		if (this.csharpStruct.isPartial()) {
			w(blank, "partial");
			blank = " ";
		}

		// --- the keyword and name
		w(blank, "struct", " ", this.csharpStruct.getName());
		blank = " ";

		// --- inheritance
		String comma = "";
		String colon = " : ";

		if (this.csharpStruct.getInterfaces().size() > 0) {
			for (CSharpInterface anInterface : this.csharpStruct.getInterfaces()) {
				wUsings(anInterface);
				w(colon, comma, anInterface.getName());
				comma = ", ";
				colon = "";
			}
		}

		wNL();
		w("{");

	}

}
