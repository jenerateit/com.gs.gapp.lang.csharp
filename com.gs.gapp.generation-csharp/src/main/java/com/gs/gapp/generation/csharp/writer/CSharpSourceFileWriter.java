/**
 *
 */
package com.gs.gapp.generation.csharp.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.csharp.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;
import com.gs.gapp.metamodel.csharp.CSharpSourceFile;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.CSharpUsingDirective;


/**
 * @author mmt
 *
 */
public class CSharpSourceFileWriter extends CSharpWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpSourceFile sourceFile;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {

        CSharpTargetSectionEnum targetSectionEnum = CSharpTargetSectionEnum.get(ts);

		if (targetSectionEnum == null) throw new IllegalArgumentException("no instance of CSharpTargetSectionEnum found for target section '" + ts + "'");

		switch (targetSectionEnum) {
		case USING_DIRECTIVES:
			wSourceFileHeader(ts);
			wUsingDirectives(ts);
			break;
		case NAMESPACE:
			wNamespace(ts);
			break;

		case TYPE:
		case ENUM_ENTRIES:
		case FIELDS:
		case INDEXERS:
		case CONSTRUCTORS:
		case PROPERTIES:
		case METHODS:
		case OPERATORS:
		case EVENTS:
		case DESTRUCTOR:
		case NESTED_TYPES:
		case TYPE_END:
			// those sections are being delegated to children of the CSharpTypeWriter class
			wType(ts);
			break;

		case ADDITIONAL_TYPES:
			wAdditionalTypes(ts);
			break;
		case NESTED_NAMESPACES:
			wNestedNamespaces(ts);
			break;
		case NAMESPACE_END:
			wNamespaceEnd(ts);
			break;
		case ADDITIONAL_NAMESPACES:
			wAdditionalNamespaces(ts);
			break;
		default:
			throw new WriterException("target section " + ts.getCode() + " is not handled", getTransformationTarget(), this);
		}
	}

	private void wNamespaceEnd(TargetSection ts) {
		CSharpNamespaceUsage firstNamespaceUsage = this.sourceFile.getFirstNamespaceUsage();
		if (firstNamespaceUsage != null) {
			WriterI namespaceWriter = this.getTransformationTarget().getWriterInstance(firstNamespaceUsage);
			if (namespaceWriter != null) namespaceWriter.transform(ts);
		}

	}

	/**
	 * @param ts
	 */
	private void wNestedNamespaces(TargetSection ts) {
		for (CSharpNamespaceUsage nestedNamespaceUsage : this.sourceFile.getFirstNamespaceUsage().getNestedNamespaceUsages()) {
			WriterI nestedNamespaceUsageWriter = this.getTransformationTarget().getWriterInstance(nestedNamespaceUsage);
			if (nestedNamespaceUsage != null) nestedNamespaceUsageWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	private void wAdditionalTypes(TargetSection ts) {

		for (CSharpType additionalType : this.sourceFile.getFirstNamespaceUsage().getRemainingTypes()) {
			WriterI typeWriter = this.getTextTransformationTarget().getWriterInstance(additionalType);
			if (typeWriter != null) {
				for (TargetSection simulatedTs : getTransformationTarget().getNewTargetDocument().getTargetSections()) {
					CSharpTargetSectionEnum tsEnum = CSharpTargetSectionEnum.get(simulatedTs);

					if (tsEnum == CSharpTargetSectionEnum.TYPE_END) outdent();

					switch (tsEnum) {
					case ADDITIONAL_NAMESPACES:
					case ADDITIONAL_TYPES:
					case NAMESPACE:
					case NAMESPACE_END:
					case NESTED_NAMESPACES:
					case USING_DIRECTIVES:
						// no need to do anything here
						break;
					case CONSTRUCTORS:
					case DESTRUCTOR:
					case ENUM_ENTRIES:
					case EVENTS:
					case FIELDS:
					case INDEXERS:
					case METHODS:
					case OPERATORS:
					case NESTED_TYPES:
					case PROPERTIES:
					case TYPE:
					case TYPE_END:
						typeWriter.transform(simulatedTs);
						break;
					default:
						throw new WriterException("unhandled target section '" + tsEnum + "' found", this.getTransformationTarget(), this);
					}

					if (tsEnum == CSharpTargetSectionEnum.TYPE) indent();
				}
			}
		}
		
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_TYPES_IN_NAMESPACE_USAGE)) {
			bDA(DeveloperAreaTypeEnum.ADDITIONAL_TYPES_IN_NAMESPACE_USAGE.getId());
			eDA();
		}
	}

	private void wType(TargetSection ts) {
		CSharpNamespaceUsage firstNamespaceUsage = this.sourceFile.getFirstNamespaceUsage();
		if (firstNamespaceUsage != null) {
			CSharpType firstType = firstNamespaceUsage.getFirstType();
			if (firstType != null) {
				try {
				    WriterI typeWriter = this.getTransformationTarget().getWriterInstance(firstType);
				    if (typeWriter != null) typeWriter.transform(ts);
				} catch (Throwable th) {
					th.printStackTrace();
				}
			}
		}
	}

	/**
	 * @param ts
	 */
	protected void wUsingDirectives(TargetSection ts) {
		for (CSharpUsingDirective usingDirective : this.sourceFile.getUsingDirectives()) {
			WriterI usingDirectiveWriter = this.getTransformationTarget().getWriterInstance(usingDirective);
			if (usingDirectiveWriter != null) usingDirectiveWriter.transform(ts);
		}
	}

	/**
	 * @param ts
	 */
	protected void wAdditionalNamespaces(TargetSection ts) {
		if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.ADDITIONAL_NAMESPACE_USAGE_IN_SOURCE)) {
			bDA(DeveloperAreaTypeEnum.ADDITIONAL_NAMESPACE_USAGE_IN_SOURCE.getId());
			eDA();
		}
	}

	/**
	 * @param ts
	 */
	protected void wNamespace(TargetSection ts) {
		CSharpNamespaceUsage firstNamespaceUsage = this.sourceFile.getFirstNamespaceUsage();
		if (firstNamespaceUsage != null) {
			WriterI namespaceWriter = this.getTransformationTarget().getWriterInstance(firstNamespaceUsage);
			if (namespaceWriter != null) namespaceWriter.transform(ts);
		}
	}

	protected void wSourceFileHeader(TargetSection ts) {
		if (StringTools.isText(this.sourceFile.getBody())) {
			wXmlTagComment(ts, this.sourceFile.getBody());
		}
	}
}
