/**
 *
 */
package com.gs.gapp.generation.csharp.type.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.metamodel.csharp.type.CSharpDelegate;
import com.gs.gapp.metamodel.csharp.type.CSharpDelegateParameter;

/**
 * @author mmt
 *
 */
public class CSharpDelegateWriter extends CSharpTypeWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpDelegate csharpDelegate;

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.csharp.type.writer.CSharpTypeWriter#wType(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected void wType(TargetSection ts) {
		String blank = "";

		wNL();

		// --- write documentation
		if (StringTools.isText(this.csharpDelegate.getBody())) {
			wXmlTagComment(ts, this.csharpDelegate.getBody());
			for (final CSharpDelegateParameter delegateParameter : this.csharpDelegate.getParameters()) {
				w(ts, "/// <param name=\"", delegateParameter.getName(), "\">");
				if (StringTools.isText(delegateParameter.getBody())) {
					w(ts, delegateParameter.getBody());
				}
				wNL(ts, "</param>");
			}
		}
		
		// --- modifiers
		if (this.csharpDelegate.getAccessModifier() != null) {
			w(blank, this.csharpDelegate.getAccessModifier().getName());
			blank = " ";
		}

		// --- the keyword
		w(blank, "delegate");
		blank = " ";

		// --- the type
		if (this.csharpDelegate.getType() != null) {
			wUsings(this.csharpDelegate.getType());
			w(blank, this.csharpDelegate.getTypeAsString());
		} else {
			w(blank, "void");
		}
		blank = " ";

        // --- the name
		w(blank, this.csharpDelegate.getName());
		blank = " ";

		// --- generic type declarations
		wGenericTypeDeclaration(ts);

		// --- parameters
		String comma = "";
		w("(");
		for (CSharpDelegateParameter delegateParameter : this.csharpDelegate.getParameters()) {
			WriterI parameterWriter = this.getTransformationTarget().getWriterInstance(delegateParameter);
			if (parameterWriter != null) {
				w(comma);parameterWriter.transform(ts);
				comma = ", ";
			} else {
				throw new WriterException("no writer found for delegateParameter '" + delegateParameter + "'", getTransformationTarget(), this);
			}
		}
		w(")");
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.csharp.type.writer.CSharpTypeWriter#wTypeEnd(org.jenerateit.target.TargetSection)
	 */
	@Override
	protected void wTypeEnd(TargetSection ts) {
		w(";");
	}
}
