/**
 * 
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;

import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.member.CSharpEnumEntry;


/**
 * @author mmt
 *
 */
public class CSharpEnumEntryWriter extends CSharpMemberWriter {
	
	@org.jenerateit.annotation.ModelElement
	private CSharpEnumEntry csharpEnumEntry;

	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.ENUM_ENTRIES.getTargetSection() == ts) {
	        String blank = "";
			
			wNL();
			
			// --- write documentation
			if (StringTools.isText(this.csharpEnumEntry.getBody())) {
				wXmlTagComment(ts, this.csharpEnumEntry.getBody());
			}
			
			// --- the enum entry name
			w(blank, this.csharpEnumEntry.getName());
			blank = " ";
		}
	}
}
