/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 * 
 */
package com.gs.gapp.generation.csharp.target;

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jenerateit.target.AbstractTargetDocument;
import org.jenerateit.target.AbstractTextTargetDocument;
import org.jenerateit.target.DeveloperAreaInfoI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterException;

/**
 * {@link AbstractTargetDocument} implementation of a Java target document.
 * 
 * @author hrr
 */
public class CSharpTargetDocument extends AbstractTextTargetDocument {

	/**
	 * the serial version UID.
	 */
	private static final long serialVersionUID = -1222683702652410945L;


	/** Pattern to search for import statements */
	protected static final Pattern USING_PATTERN = 
		Pattern.compile("^\\s*using\\s+(" + 
				DeveloperAreaInfoI.ID_PATTERN + "(\\.\\*)?)\\s*;\\s*$");
	
	private final Set<String> usingStatements = new LinkedHashSet<String>();
	
	/**
	 * Clear the using cache and call the super class method {@link AbstractTextTargetDocument#analyzeDocument()}
	 * 
	 * @see AbstractTextTargetDocument#analyzeDocument()
	 */
	@Override
	protected void analyzeDocument() {
		this.usingStatements.clear();
		
		super.analyzeDocument();
	}


	/**
	 * Calls the super class method to check for developer areas and 
	 * then check the given line for a using statement.
	 * 
	 * @param lineNumber number of the line of the current file
	 * @param line the line to check
	 * @param start the start position in the target document of the line
	 * @param end the end position in the target document of the line
	 * @see AbstractTextTargetDocument#analyzeLine(int, java.lang.String, int, int)
	 */
	@Override
	protected void analyzeLine(int lineNumber, String line, int start, int end) {
		super.analyzeLine(lineNumber, line, start, end);
		
		Matcher m = null;
		if (StringTools.isText(line) &&
				(m = USING_PATTERN.matcher(line)) != null && m.matches()) {
			this.usingStatements.add(m.group(1));
		}
	}

	/**
	 * Parse the {@link CSharpTargetDocument#USING_PATTERN} section for using statements only.
	 * 
	 * @return a list of import statements
	 */
	public Set<String> getSectionUsing() {
		Set<String> usings = new LinkedHashSet<String>();
		
		LineNumberReader lnr = new LineNumberReader(new StringReader(getUsingSection()));
		String line = null;
		Matcher m = null;
		try {
			while ((line = lnr.readLine()) != null) {
				if (StringTools.isText(line) &&
						(m = CSharpTargetDocument.USING_PATTERN.matcher(line)) != null &&
						m.matches()) {
					usings.add(m.group(1));
				}
			}
		} catch (IOException e) {
			throw new WriterException("Can not read the current usings");
		}
		return usings;
	}

	/**
	 * Getter to request all using statements.
	 * 
	 * @return a Set of using statements
	 */
	public Set<String> getUsings() {
		if (!isDocumentAnalyzed()) {
			analyzeDocument();
		}
		return this.usingStatements;
	}
	
	/**
	 * Returns the content of the {@link #USING_PATTERN} section as byte array.
	 * 
	 * @return the content of the {@link #USING_PATTERN} section
	 */
	protected String getUsingSection() {
		return new String(toByte(CSharpTargetSectionEnum.USING_DIRECTIVES.getTargetSection()));
	}
	
	/**
	 * Return an empty string.
	 * 
	 * @return the comment end sequence
	 * @see org.jenerateit.target.TextTargetDocumentI#getCommentEnd()
	 */
	public final String getCommentEnd() {
		return StringTools.EMPTY_STRING;
	}

	/**
	 * Returns the '//' sequence.
	 * 
	 * @return the comment start sequence
	 * @see org.jenerateit.target.TextTargetDocumentI#getCommentStart()
	 */
	public final String getCommentStart() {
		return "//";
	}

	/**
	 * Returns the target sections from this class.
	 * 
	 * @return a set of sections describing a Java file
	 * @see org.jenerateit.target.TextTargetDocumentI#getTargetSections()
	 */
	public final SortedSet<TargetSection> getTargetSections() {
		return CSharpTargetSectionEnum.getSections();
	}


	/**
	 * Getter for the indent character.
	 * 
	 * @return the indent character
	 */
	public char getPrefixChar() {
		return ' ';
	}

}
