package com.gs.gapp.generation.csharp.target;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jenerateit.target.TargetSection;

/**
 * @author mmt
 *
 */
public enum CSharpTargetSectionEnum {

	USING_DIRECTIVES ( new TargetSection("using-directives", 10) ),

	NAMESPACE ( new TargetSection("namespace", 20) ),

	TYPE ( new TargetSection("type", 30, CSharpTargetSectionEnum.ONE_TAB) ),
	// --- members of the type
	ENUM_ENTRIES ( new TargetSection("enum-entries", 35,	CSharpTargetSectionEnum.TWO_TABS) ),
	FIELDS ( new TargetSection("fields", 40,				CSharpTargetSectionEnum.TWO_TABS) ),
	INDEXERS ( new TargetSection("indexers", 50,			CSharpTargetSectionEnum.TWO_TABS) ),
	CONSTRUCTORS ( new TargetSection("constructors", 60,	CSharpTargetSectionEnum.TWO_TABS) ),
	PROPERTIES ( new TargetSection("properties", 70,		CSharpTargetSectionEnum.TWO_TABS) ),
	METHODS ( new TargetSection("methods", 80,				CSharpTargetSectionEnum.TWO_TABS) ),
	OPERATORS ( new TargetSection("operators", 90,			CSharpTargetSectionEnum.TWO_TABS) ),
	EVENTS ( new TargetSection("events", 100,				CSharpTargetSectionEnum.TWO_TABS) ),
	DESTRUCTOR ( new TargetSection("destructor", 110,		CSharpTargetSectionEnum.TWO_TABS) ),
	NESTED_TYPES ( new TargetSection("nested-types", 120,	CSharpTargetSectionEnum.TWO_TABS) ),
	TYPE_END ( new TargetSection("type-end", 130, CSharpTargetSectionEnum.ONE_TAB) ),

	ADDITIONAL_TYPES ( new TargetSection("additional-types", 140,	CSharpTargetSectionEnum.ONE_TAB) ),
	NESTED_NAMESPACES ( new TargetSection("nested-namespaces", 150,	CSharpTargetSectionEnum.ONE_TAB) ),
	NAMESPACE_END ( new TargetSection("namespace-end", 160) ),

	ADDITIONAL_NAMESPACES ( new TargetSection("additional-namespaces", 170) ),
	;

	private static final int TABSIZE = 4;
	@SuppressWarnings("unused")
	private static final int NO_TAB = 0;
	private static final int ONE_TAB = TABSIZE;
	private static final int TWO_TABS = TABSIZE*2;
	@SuppressWarnings("unused")
	private static final int THREE_TABS = TABSIZE*3;

	private final static Map<TargetSection, CSharpTargetSectionEnum> targetSectionToEnumMap =
			new LinkedHashMap<TargetSection, CSharpTargetSectionEnum>();

	private final static SortedSet<TargetSection> sections = new TreeSet<TargetSection>();

	static {
		for (CSharpTargetSectionEnum e : values()) {
			targetSectionToEnumMap.put(e.getTargetSection(), e);
			sections.add(e.getTargetSection());
		}
	}

	private final TargetSection targetSection;


	private CSharpTargetSectionEnum(TargetSection targetSection) {
		this.targetSection = targetSection;
	}

	/**
	 * @return the targetSection
	 */
	public TargetSection getTargetSection() {
		return targetSection;
	}

	/**
	 * @param targetSection
	 * @return
	 */
	public static CSharpTargetSectionEnum get(TargetSection targetSection) {
		return targetSectionToEnumMap.get(targetSection);
	}

	/**
	 * @return
	 */
	public static SortedSet<TargetSection> getSections() {
		return sections;
	}
}
