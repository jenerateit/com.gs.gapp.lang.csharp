/**
 * 
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;

import com.gs.gapp.generation.csharp.DeveloperAreaTypeEnum;
import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.member.CSharpDestructor;


/**
 * @author mmt
 *
 */
public class CSharpDestructorWriter extends CSharpMemberWriter {
	
	@org.jenerateit.annotation.ModelElement
	private CSharpDestructor csharpDestructor;

	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.DESTRUCTOR.getTargetSection() == ts) {
	        String blank = "";
			
			wNL();
			
			// --- write documentation
			if (StringTools.isText(this.csharpDestructor.getBody())) {
				wXmlTagComment(ts, this.csharpDestructor.getBody());
			}
			
			// --- the destructor name
			w(blank, "~ ", this.csharpDestructor.getName(), "()");
			blank = " ";
			
			// --- destructor body
			wNL();
			wNLI("{");
				if (isDeveloperAreaTypeActive(DeveloperAreaTypeEnum.DESTRUCTOR_BODY)) {
			    	StringBuffer sb = new StringBuffer(this.csharpDestructor.getOwner().getQualifiedName(".")).append(".").append(this.csharpDestructor.getName());
				    bDA(sb.toString());
				    wDestructorBody(ts);
				    eDA();
			    } else {
			    	wDestructorBody(ts);
			    }
			wONL("}");
		}
	}

	/**
	 * @param ts
	 */
	protected void wDestructorBody(TargetSection ts) {
	    // nothing to be done here	
	}
}
