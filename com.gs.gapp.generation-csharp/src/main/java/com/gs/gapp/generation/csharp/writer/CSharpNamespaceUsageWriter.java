/**
 *
 */
package com.gs.gapp.generation.csharp.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;
import com.gs.gapp.metamodel.csharp.type.CSharpType;


/**
 * @author mmt
 *
 */
public class CSharpNamespaceUsageWriter extends CSharpWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpNamespaceUsage csharpNamespaceUsage;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {

		if (this.csharpNamespaceUsage.getNamespace() != CSharpNamespace.GLOBAL_NAMESPACE) {
			if (ts == CSharpTargetSectionEnum.NAMESPACE.getTargetSection()) {
				wNamespace(ts);
			} else if (ts == CSharpTargetSectionEnum.NAMESPACE_END.getTargetSection()) {
				wNL();
				w("}");
			} else if (ts == CSharpTargetSectionEnum.NESTED_NAMESPACES.getTargetSection()) {
				// nested namespaced need to be handled differently

				wNL();
				this.transform(CSharpTargetSectionEnum.NAMESPACE.getTargetSection());

				// --- nested namespace's types
				for (CSharpType type : this.csharpNamespaceUsage.getTypes()) {
					WriterI typeWriter = this.getTextTransformationTarget().getWriterInstance(type);
					if (typeWriter != null) {
						indent();
						for (TargetSection simulatedTs : getTransformationTarget().getNewTargetDocument().getTargetSections()) {
							CSharpTargetSectionEnum tsEnum = CSharpTargetSectionEnum.get(simulatedTs);

							if (tsEnum == CSharpTargetSectionEnum.TYPE_END) outdent();

							switch (tsEnum) {
							case ADDITIONAL_NAMESPACES:
							case ADDITIONAL_TYPES:
							case NAMESPACE:
							case NAMESPACE_END:
							case NESTED_NAMESPACES:
							case USING_DIRECTIVES:
								// no need to do anything here
								break;
							case CONSTRUCTORS:
							case DESTRUCTOR:
							case ENUM_ENTRIES:
							case EVENTS:
							case FIELDS:
							case INDEXERS:
							case METHODS:
							case OPERATORS:
							case NESTED_TYPES:
							case PROPERTIES:
							case TYPE:
							case TYPE_END:
								typeWriter.transform(simulatedTs);
								break;
							default:
								throw new WriterException("unhandled target section '" + tsEnum + "' found", this.getTransformationTarget(), this);
							}

							if (tsEnum == CSharpTargetSectionEnum.TYPE) indent();
						}
						outdent();
					}
				}

				// --- here we handle additional nested namespace usages
				indent();
                for (CSharpNamespaceUsage nestedNamespaceUsage : this.csharpNamespaceUsage.getNestedNamespaceUsages()) {
                	WriterI nestedNamespaceUsageWriter = this.getTextTransformationTarget().getWriterInstance(nestedNamespaceUsage);
                	if (nestedNamespaceUsageWriter != null) nestedNamespaceUsageWriter.transform(ts);
				}
				outdent();
				this.transform(CSharpTargetSectionEnum.NAMESPACE_END.getTargetSection());


			}
		}
	}

	/**
	 * @param ts
	 */
	protected void wNamespace(TargetSection ts) {
		wNL();
		wNL("namespace ", this.csharpNamespaceUsage.getNamespace().getQualifiedName("."));
		w("{");
	}
}
