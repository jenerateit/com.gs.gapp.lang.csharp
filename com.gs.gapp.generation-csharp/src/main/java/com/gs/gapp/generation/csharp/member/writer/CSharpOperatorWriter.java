/**
 * 
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.member.CSharpOperator;
import com.gs.gapp.metamodel.csharp.member.CSharpParameter;


/**
 * @author mmt
 *
 */
public class CSharpOperatorWriter extends CSharpMemberWriter {
	
	@org.jenerateit.annotation.ModelElement
	private CSharpOperator csharpOperator;

	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.OPERATORS.getTargetSection() == ts) {
	        String blank = "";
			
			wNL();
			
			// --- write documentation
			if (StringTools.isText(this.csharpOperator.getBody())) {
				wXmlTagComment(ts, this.csharpOperator.getBody());
				for (final CSharpParameter parameter : this.csharpOperator.getParameters()) {
					w(ts, "/// <param name=\"", parameter.getName(), "\">");
					if (StringTools.isText(parameter.getBody())) {
						w(ts, parameter.getBody());
					}
					wNL(ts, "</param>");
				}
				// TODO return type?
			}
			
			// --- modifiers
			if (this.csharpOperator.getAccessModifier() != null) {
				w(blank, this.csharpOperator.getAccessModifier().getName());
				blank = " ";
			}
				
			if (this.csharpOperator.isStatic()) {
				w(blank, CSharpModifier.STATIC.getName());
				blank = " ";
			}
			
			// --- the type
			w(blank, this.csharpOperator.getType().getTypeAsString());
			blank = " ";
			
			// --- the operator name
			w(blank, "operator", " ", this.csharpOperator.getName());
			blank = " ";
			
			// --- parameters
			String comma = "";
			w("(");
			for (CSharpParameter parameter : this.csharpOperator.getParameters()) {
				WriterI parameterWriter = this.getTransformationTarget().getWriterInstance(parameter);
				if (parameterWriter != null) {
					w(comma);parameterWriter.transform(ts);
					comma = ", ";
				}
			}
			w(")");
			
			// --- operator body
			wNL();
			wNLI("{");
			    wOperatorBody(ts);
			wONL("}");
		}
	}

	protected void wOperatorBody(TargetSection ts) {
		// TODO Auto-generated method stub
		
	}

	
}
