/**
 *
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.csharp.writer.CSharpWriter;
import com.gs.gapp.metamodel.csharp.member.CSharpParameter;
import com.gs.gapp.metamodel.csharp.member.CSharpParameterModifier;


/**
 * @author mmt
 *
 */
public class CSharpParameterWriter extends CSharpWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpParameter csharpParameter;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		wUsings(this.csharpParameter.getType());
		w(csharpParameter.getParameterModifier().getName(), csharpParameter.getParameterModifier() != CSharpParameterModifier.NONE ? " " : "",
				csharpParameter.getType().getTypeAsString(), " ", csharpParameter.getName());
	}
}
