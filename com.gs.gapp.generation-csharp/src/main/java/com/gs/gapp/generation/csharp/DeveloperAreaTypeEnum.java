package com.gs.gapp.generation.csharp;



/**
 * 
 * @author mmt
 *
 */
public enum DeveloperAreaTypeEnum {

	/**
	 * Allows for manual modifications within a constructor body
	 */
	CONSTRUCTOR_BODY,
	
	/**
	 * Allows for manual modifications within a destructor body
	 */
	DESTRUCTOR_BODY,
	
	/**
	 * Allows for manual modifications within a function or procedure body
	 */
	METHOD_BODY,
	
	/**
	 * Allows for manual additions of enumeration entries in an enumeration
	 */
	ENUMERATION_ENTRIES,
	
	ADDITIONAL_MEMBERS_IN_TYPE,
	
	ADDITIONAL_TYPES_IN_NAMESPACE_USAGE,
	
	ADDITIONAL_NAMESPACE_USAGE_IN_SOURCE,
	;
	
	public String getId() {
		return this.name().toLowerCase();
	}
}
