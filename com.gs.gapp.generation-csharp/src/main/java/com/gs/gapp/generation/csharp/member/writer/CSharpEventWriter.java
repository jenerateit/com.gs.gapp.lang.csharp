/**
 *
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.member.CSharpEvent;


/**
 * @author mmt
 *
 */
public class CSharpEventWriter extends CSharpMemberWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpEvent csharpEvent;

	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.EVENTS.getTargetSection() == ts) {
	        String blank = "";

			wNL();

			// --- modifiers
			if (!csharpEvent.isInInterface()) {
				if (this.csharpEvent.getAccessModifier() != null) {
					w(blank, this.csharpEvent.getAccessModifier().getName());
					blank = " ";
				}

				if (this.csharpEvent.isAbstract()) {
					w(blank, CSharpModifier.ABSTRACT.getName());
					blank = " ";
				}

				if (this.csharpEvent.isStatic()) {
					w(blank, CSharpModifier.STATIC.getName());
					blank = " ";
				}
			}

			// --- the event name
			wUsings(this.csharpEvent.getType());
			w(blank, "event", " ", this.csharpEvent.getType().getTypeAsString(), " ", this.csharpEvent.getName(), ";");
			blank = " ";
		}
	}
}
