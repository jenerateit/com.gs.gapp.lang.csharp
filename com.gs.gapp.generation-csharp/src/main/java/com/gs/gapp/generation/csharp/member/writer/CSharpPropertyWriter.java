/**
 *
 */
package com.gs.gapp.generation.csharp.member.writer;

import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.csharp.target.CSharpTargetSectionEnum;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;


/**
 * @author mmt
 *
 */
public class CSharpPropertyWriter extends CSharpMemberWriter {

	@org.jenerateit.annotation.ModelElement
	private CSharpProperty property;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		if (CSharpTargetSectionEnum.PROPERTIES.getTargetSection() == ts) {
			wNL();
			wUsings(property.getType());

	        if (this.property.isAutoImplemented()) {
	        	wNL(this.property.isInInterface() ? "" : "public ", property.getType().getTypeAsString(), " ", property.getName(), " { get; set; }");
	        } else {
	        	if (this.property.isInInterface()) {
	        		wNL(property.getType().getTypeAsString(), " ", property.getName());
		        	wNLI("{");
		        	    wNL("get;");
		        	    if (property.isReadOnly() == false) {
		        	    	wNL("set;");
		        	    }
		        	wONL("}");
	        	} else {
	        		// TODO if this property is the implementation of a property in an interface, so-called explicit interface implementation might have to be used: http://msdn.microsoft.com/en-us/library/vstudio/ms173157.aspx
		        	wNL("public ", property.getType().getTypeAsString(), " ", property.getName());
		        	wNLI("{");
		        	    wNL(property.getAccessModifierForGet() != null ? property.getAccessModifierForGet().getName() : "", property.getAccessModifierForGet() != null ? " get" : "get");
		        	    wNLI("{");
		        	        wPropertyGetter(ts);
		        	    wONL("}");
		        	    if (property.isReadOnly() == false) {
		        	    	wNL(property.getAccessModifierForSet() != null ? property.getAccessModifierForSet().getName() : "", property.getAccessModifierForGet() != null ? " set" : "set");
		        	    	wNLI("{");
		        	    	    wPropertySetter(ts);
		            	    wONL("}");
		        	    }
		        	wONL("}");
	        	}
	        }
		}
	}

	/**
	 * @param ts
	 */
	protected void wPropertyGetter(TargetSection ts) {
		if (this.property.getRelatedField() != null) {
            wNL("return ", this.property.getRelatedField().getName(), ";");
		}
	}

	/**
	 * @param ts
	 */
	protected void wPropertySetter(TargetSection ts) {
        if (this.property.getRelatedField() != null) {
        	wNL(this.property.getRelatedField().getName(), " = value;");
		}
	}
}
