package com.gs.vd.metamodel.csharp.predef.System_Runtime_Caching_Configuration;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Configuration.ConfigurationElementCollection;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_Caching.Configuration_Namespace;

public class MemoryCacheSettingsCollection {
    // full name: System.Runtime.Caching, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Runtime.Caching\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Runtime.Caching.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Configuration_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("MemoryCacheSettingsCollection", declaringType);
        } else {
            result = new CSharpClass("MemoryCacheSettingsCollection", getNamespace());
        }
        result.setBaseType(ConfigurationElementCollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.ICollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        return result;
    }

}
