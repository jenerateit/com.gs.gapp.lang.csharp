package com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract_Collections;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;
import com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract.Collections_Namespace;

public class RemoteArgumentDictionaryEntry {
    // full name: System.AddIn.Contract, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.AddIn.Contract\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.AddIn.Contract.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Collections_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("RemoteArgumentDictionaryEntry", declaringType);
        } else {
            result = new CSharpStruct("RemoteArgumentDictionaryEntry", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        return result;
    }

}
