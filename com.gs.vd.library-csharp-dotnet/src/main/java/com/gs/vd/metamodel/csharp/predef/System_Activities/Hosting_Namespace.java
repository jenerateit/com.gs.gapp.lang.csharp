package com.gs.vd.metamodel.csharp.predef.System_Activities;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Activities_Namespace;

public class Hosting_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Hosting", Activities_Namespace.NAMESPACE);
    };


}
