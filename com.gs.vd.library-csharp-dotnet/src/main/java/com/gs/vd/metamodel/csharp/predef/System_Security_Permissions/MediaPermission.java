package com.gs.vd.metamodel.csharp.predef.System_Security_Permissions;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Security.CodeAccessPermission;
import com.gs.vd.metamodel.csharp.predef.System_Security.Permissions_Namespace;

public class MediaPermission {
    // full name: WindowsBase, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\WindowsBase\v4.0_4.0.0.0__31bf3856ad364e35\WindowsBase.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Permissions_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("MediaPermission", declaringType);
        } else {
            result = new CSharpClass("MediaPermission", getNamespace());
        }
        result.setBaseType(CodeAccessPermission.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.IPermission.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.ISecurityEncodable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.IStackWalk.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security_Permissions.IUnrestrictedPermission.TYPE);
        return result;
    }

}
