package com.gs.vd.metamodel.csharp.predef.System_Globalization;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Globalization_Namespace;
import com.gs.vd.metamodel.csharp.predef.System.Object;

public class CultureAndRegionInfoBuilder {
    // full name: sysglobl, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\sysglobl\v4.0_4.0.0.0__b03f5f7f11d50a3a\sysglobl.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Globalization_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("CultureAndRegionInfoBuilder", declaringType);
        } else {
            result = new CSharpClass("CultureAndRegionInfoBuilder", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        return result;
    }

}
