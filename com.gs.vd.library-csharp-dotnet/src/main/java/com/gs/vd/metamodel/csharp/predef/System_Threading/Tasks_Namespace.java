package com.gs.vd.metamodel.csharp.predef.System_Threading;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Threading_Namespace;

public class Tasks_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Tasks", Threading_Namespace.NAMESPACE);
    };


}
