package com.gs.vd.metamodel.csharp.predef.System_Activities_Statements;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Activities.NativeActivity;
import com.gs.vd.metamodel.csharp.predef.System_Activities.Statements_Namespace;

public class Interop {
    // full name: System.Workflow.Runtime, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Workflow.Runtime\v4.0_4.0.0.0__31bf3856ad364e35\System.Workflow.Runtime.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Statements_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("Interop", declaringType);
        } else {
            result = new CSharpClass("Interop", getNamespace());
        }
        result.setBaseType(NativeActivity.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Activities_DynamicUpdate.IInstanceUpdatable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ICustomTypeDescriptor.TYPE);
        return result;
    }

}
