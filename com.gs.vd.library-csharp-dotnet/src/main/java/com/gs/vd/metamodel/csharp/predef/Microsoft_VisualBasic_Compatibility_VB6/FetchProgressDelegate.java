package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_Compatibility_VB6;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_Compatibility.VB6_Namespace;
import com.gs.vd.metamodel.csharp.predef.System.MulticastDelegate;

public class FetchProgressDelegate {
    // full name: Microsoft.VisualBasic.Compatibility.Data, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.VisualBasic.Compatibility.Data\v4.0_10.0.0.0__b03f5f7f11d50a3a\Microsoft.VisualBasic.Compatibility.Data.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return VB6_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return ADODC.TYPE;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("FetchProgressDelegate", declaringType);
        } else {
            result = new CSharpClass("FetchProgressDelegate", getNamespace());
        }
        result.setBaseType(MulticastDelegate.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.ICloneable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.ISerializable.TYPE);
        return result;
    }

}
