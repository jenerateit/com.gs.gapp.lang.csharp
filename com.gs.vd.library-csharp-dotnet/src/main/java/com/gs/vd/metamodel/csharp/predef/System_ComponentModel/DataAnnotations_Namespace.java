package com.gs.vd.metamodel.csharp.predef.System_ComponentModel;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.ComponentModel_Namespace;

public class DataAnnotations_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("DataAnnotations", ComponentModel_Namespace.NAMESPACE);
    };


}
