package com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting_Metadata;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting.Metadata_Namespace;

public class W3cXsd2001_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("W3cXsd2001", Metadata_Namespace.NAMESPACE);
    };


}
