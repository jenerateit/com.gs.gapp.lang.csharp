package com.gs.vd.metamodel.csharp.predef.System_Speech;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Speech_Namespace;

public class Recognition_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Recognition", Speech_Namespace.NAMESPACE);
    };


}
