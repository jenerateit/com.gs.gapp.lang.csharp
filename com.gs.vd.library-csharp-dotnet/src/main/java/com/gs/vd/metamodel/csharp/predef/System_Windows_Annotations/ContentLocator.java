package com.gs.vd.metamodel.csharp.predef.System_Windows_Annotations;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Annotations_Namespace;

public class ContentLocator {
    // full name: PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\PresentationFramework\v4.0_4.0.0.0__31bf3856ad364e35\PresentationFramework.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Annotations_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ContentLocator", declaringType);
        } else {
            result = new CSharpClass("ContentLocator", getNamespace());
        }
        result.setBaseType(ContentLocatorBase.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.MS_Internal_Annotations.INotifyPropertyChanged2.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.INotifyPropertyChanged.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.MS_Internal_Annotations.IOwnedObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Xml_Serialization.IXmlSerializable.TYPE);
        return result;
    }

}
