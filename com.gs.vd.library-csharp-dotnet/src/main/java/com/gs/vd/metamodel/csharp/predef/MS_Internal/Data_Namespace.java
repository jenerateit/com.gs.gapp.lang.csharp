package com.gs.vd.metamodel.csharp.predef.MS_Internal;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.MS.Internal_Namespace;

public class Data_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Data", Internal_Namespace.NAMESPACE);
    };


}
