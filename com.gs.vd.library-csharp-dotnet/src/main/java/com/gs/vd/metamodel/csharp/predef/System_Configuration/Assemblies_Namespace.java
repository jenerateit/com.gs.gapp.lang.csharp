package com.gs.vd.metamodel.csharp.predef.System_Configuration;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Configuration_Namespace;

public class Assemblies_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Assemblies", Configuration_Namespace.NAMESPACE);
    };


}
