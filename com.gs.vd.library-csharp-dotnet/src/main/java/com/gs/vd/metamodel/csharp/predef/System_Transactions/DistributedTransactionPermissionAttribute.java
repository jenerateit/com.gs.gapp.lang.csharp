package com.gs.vd.metamodel.csharp.predef.System_Transactions;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Transactions_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Security_Permissions.CodeAccessSecurityAttribute;

public class DistributedTransactionPermissionAttribute {
    // full name: System.Transactions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Transactions\v4.0_4.0.0.0__b77a5c561934e089\System.Transactions.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Transactions_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DistributedTransactionPermissionAttribute", declaringType);
        } else {
            result = new CSharpClass("DistributedTransactionPermissionAttribute", getNamespace());
        }
        result.setBaseType(CodeAccessSecurityAttribute.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._Attribute.TYPE);
        return result;
    }

}
