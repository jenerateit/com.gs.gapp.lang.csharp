package com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting_Activation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting.Activation_Namespace;

public class IConstructionCallMessage {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Activation_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IConstructionCallMessage", declaringType);
        } else {
            result = new CSharpInterface("IConstructionCallMessage", getNamespace());
        }
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting_Messaging.IMethodCallMessage.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting_Messaging.IMethodMessage.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting_Messaging.IMessage.TYPE);
        return result;
    }

}
