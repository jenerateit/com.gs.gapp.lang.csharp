package com.gs.vd.metamodel.csharp.predef.System_Diagnostics_Contracts;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Diagnostics.Contracts_Namespace;

public class Internal_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Internal", Contracts_Namespace.NAMESPACE);
    };


}
