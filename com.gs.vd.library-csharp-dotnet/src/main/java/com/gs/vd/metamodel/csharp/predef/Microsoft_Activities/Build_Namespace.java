package com.gs.vd.metamodel.csharp.predef.Microsoft_Activities;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.Activities_Namespace;

public class Build_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Build", Activities_Namespace.NAMESPACE);
    };


}
