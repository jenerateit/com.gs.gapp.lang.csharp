package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_Activities;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic.Activities_Namespace;

public class VisualBasicValue {
    // full name: System.Activities, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Activities\v4.0_4.0.0.0__31bf3856ad364e35\System.Activities.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Activities_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("VisualBasicValue", declaringType);
        } else {
            result = new CSharpClass("VisualBasicValue", getNamespace());
        }
        // type has no base type
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Activities_XamlIntegration.IValueSerializableExpression.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Activities.IExpressionContainer.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Activities_Expressions.ITextExpression.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("TResult", result, null, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
