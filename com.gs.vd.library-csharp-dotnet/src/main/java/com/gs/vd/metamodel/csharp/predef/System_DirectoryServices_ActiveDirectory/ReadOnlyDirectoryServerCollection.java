package com.gs.vd.metamodel.csharp.predef.System_DirectoryServices_ActiveDirectory;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Collections.ReadOnlyCollectionBase;
import com.gs.vd.metamodel.csharp.predef.System_DirectoryServices.ActiveDirectory_Namespace;

public class ReadOnlyDirectoryServerCollection {
    // full name: System.DirectoryServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.DirectoryServices\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.DirectoryServices.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return ActiveDirectory_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ReadOnlyDirectoryServerCollection", declaringType);
        } else {
            result = new CSharpClass("ReadOnlyDirectoryServerCollection", getNamespace());
        }
        result.setBaseType(ReadOnlyCollectionBase.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.ICollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        return result;
    }

}
