package com.gs.vd.metamodel.csharp.predef.System_Windows_Forms;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Forms_Namespace;

public class Integration_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Integration", Forms_Namespace.NAMESPACE);
    };


}
