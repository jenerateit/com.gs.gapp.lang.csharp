package com.gs.vd.metamodel.csharp.predef.Microsoft_JScript;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft.JScript_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Reflection.FieldInfo;

public class COMFieldInfo {
    // full name: Microsoft.JScript, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.JScript\v4.0_10.0.0.0__b03f5f7f11d50a3a\Microsoft.JScript.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return JScript_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("COMFieldInfo", declaringType);
        } else {
            result = new CSharpClass("COMFieldInfo", getNamespace());
        }
        result.setBaseType(FieldInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Reflection.ICustomAttributeProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._MemberInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._FieldInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_JScript.MemberInfoInitializer.TYPE);
        return result;
    }

}
