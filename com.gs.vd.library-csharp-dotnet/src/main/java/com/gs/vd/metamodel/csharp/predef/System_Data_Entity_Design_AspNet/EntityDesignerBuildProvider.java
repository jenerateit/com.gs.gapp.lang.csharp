package com.gs.vd.metamodel.csharp.predef.System_Data_Entity_Design_AspNet;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Data_Entity_Design.AspNet_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Web_Compilation.BuildProvider;

public class EntityDesignerBuildProvider {
    // full name: System.Data.Entity.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Data.Entity.Design\v4.0_4.0.0.0__b77a5c561934e089\System.Data.Entity.Design.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return AspNet_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("EntityDesignerBuildProvider", declaringType);
        } else {
            result = new CSharpClass("EntityDesignerBuildProvider", getNamespace());
        }
        result.setBaseType(BuildProvider.TYPE);
        return result;
    }

}
