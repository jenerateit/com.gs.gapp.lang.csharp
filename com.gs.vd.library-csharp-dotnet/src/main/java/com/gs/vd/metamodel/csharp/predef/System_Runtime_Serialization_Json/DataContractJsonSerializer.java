package com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization_Json;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.Json_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.XmlObjectSerializer;

public class DataContractJsonSerializer {
    // full name: System.Runtime.Serialization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Runtime.Serialization\v4.0_4.0.0.0__b77a5c561934e089\System.Runtime.Serialization.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Json_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DataContractJsonSerializer", declaringType);
        } else {
            result = new CSharpClass("DataContractJsonSerializer", getNamespace());
        }
        result.setBaseType(XmlObjectSerializer.TYPE);
        return result;
    }

}
