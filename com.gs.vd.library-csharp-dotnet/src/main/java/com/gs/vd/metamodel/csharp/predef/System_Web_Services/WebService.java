package com.gs.vd.metamodel.csharp.predef.System_Web_Services;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.MarshalByValueComponent;
import com.gs.vd.metamodel.csharp.predef.System_Web.Services_Namespace;

public class WebService {
    // full name: System.Web.Services, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Web.Services\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Web.Services.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Services_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("WebService", declaringType);
        } else {
            result = new CSharpClass("WebService", getNamespace());
        }
        result.setBaseType(MarshalByValueComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IServiceProvider.TYPE);
        return result;
    }

}
