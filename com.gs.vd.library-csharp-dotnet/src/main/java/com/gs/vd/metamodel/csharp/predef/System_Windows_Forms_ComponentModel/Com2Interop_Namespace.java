package com.gs.vd.metamodel.csharp.predef.System_Windows_Forms_ComponentModel;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.ComponentModel_Namespace;

public class Com2Interop_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Com2Interop", ComponentModel_Namespace.NAMESPACE);
    };


}
