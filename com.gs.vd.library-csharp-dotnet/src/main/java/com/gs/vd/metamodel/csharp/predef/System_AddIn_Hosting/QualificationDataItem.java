package com.gs.vd.metamodel.csharp.predef.System_AddIn_Hosting;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;
import com.gs.vd.metamodel.csharp.predef.System_AddIn.Hosting_Namespace;

public class QualificationDataItem {
    // full name: System.AddIn, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.AddIn\v4.0_4.0.0.0__b77a5c561934e089\System.AddIn.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Hosting_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("QualificationDataItem", declaringType);
        } else {
            result = new CSharpStruct("QualificationDataItem", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        return result;
    }

}
