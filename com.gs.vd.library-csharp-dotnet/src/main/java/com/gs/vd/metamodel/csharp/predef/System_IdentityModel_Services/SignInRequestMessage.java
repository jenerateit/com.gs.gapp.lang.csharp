package com.gs.vd.metamodel.csharp.predef.System_IdentityModel_Services;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_IdentityModel.Services_Namespace;

public class SignInRequestMessage {
    // full name: System.IdentityModel.Services, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.IdentityModel.Services\v4.0_4.0.0.0__b77a5c561934e089\System.IdentityModel.Services.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Services_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("SignInRequestMessage", declaringType);
        } else {
            result = new CSharpClass("SignInRequestMessage", getNamespace());
        }
        result.setBaseType(WSFederationMessage.TYPE);
        return result;
    }

}
