package com.gs.vd.metamodel.csharp.predef.System_Windows_Forms_Design;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.Design_Namespace;

public class Behavior_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Behavior", Design_Namespace.NAMESPACE);
    };


}
