package com.gs.vd.metamodel.csharp.predef.System_Resources;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Resources_Namespace;

public class Tools_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Tools", Resources_Namespace.NAMESPACE);
    };


}
