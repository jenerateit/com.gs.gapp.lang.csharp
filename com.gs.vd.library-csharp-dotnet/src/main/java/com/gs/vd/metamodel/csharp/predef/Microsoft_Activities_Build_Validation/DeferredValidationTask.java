package com.gs.vd.metamodel.csharp.predef.Microsoft_Activities_Build_Validation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Activities_Build.Validation_Namespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Utilities.Task;

public class DeferredValidationTask {
    // full name: Microsoft.Activities.Build, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.Activities.Build\v4.0_4.0.0.0__31bf3856ad364e35\Microsoft.Activities.Build.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Validation_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DeferredValidationTask", declaringType);
        } else {
            result = new CSharpClass("DeferredValidationTask", getNamespace());
        }
        result.setBaseType(Task.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Framework.ITask.TYPE);
        return result;
    }

}
