package com.gs.vd.metamodel.csharp.predef.System_Web_Query;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.Query_Namespace;

public class Dynamic_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Dynamic", Query_Namespace.NAMESPACE);
    };


}
