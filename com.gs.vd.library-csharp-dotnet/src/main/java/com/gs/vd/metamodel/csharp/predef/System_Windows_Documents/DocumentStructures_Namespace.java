package com.gs.vd.metamodel.csharp.predef.System_Windows_Documents;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Documents_Namespace;

public class DocumentStructures_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("DocumentStructures", Documents_Namespace.NAMESPACE);
    };


}
