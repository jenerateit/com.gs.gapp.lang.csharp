package com.gs.vd.metamodel.csharp.predef.System_Diagnostics_Contracts_Internal;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Object;
import com.gs.vd.metamodel.csharp.predef.System_Diagnostics_Contracts.Internal_Namespace;

public class ContractHelper {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Internal_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ContractHelper", declaringType);
        } else {
            result = new CSharpClass("ContractHelper", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        return result;
    }

}
