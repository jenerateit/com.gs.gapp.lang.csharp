package com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization_Formatters;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.Formatters_Namespace;

public class Binary_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Binary", Formatters_Namespace.NAMESPACE);
    };


}
