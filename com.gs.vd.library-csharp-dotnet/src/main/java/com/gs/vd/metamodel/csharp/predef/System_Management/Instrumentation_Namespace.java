package com.gs.vd.metamodel.csharp.predef.System_Management;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Management_Namespace;

public class Instrumentation_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Instrumentation", Management_Namespace.NAMESPACE);
    };


}
