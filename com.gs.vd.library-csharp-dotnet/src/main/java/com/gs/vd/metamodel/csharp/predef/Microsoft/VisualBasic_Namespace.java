package com.gs.vd.metamodel.csharp.predef.Microsoft;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Namespace;

public class VisualBasic_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("VisualBasic", Microsoft_Namespace.NAMESPACE);
    };


}
