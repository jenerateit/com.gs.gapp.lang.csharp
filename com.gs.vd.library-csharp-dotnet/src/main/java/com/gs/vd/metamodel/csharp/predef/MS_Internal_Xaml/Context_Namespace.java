package com.gs.vd.metamodel.csharp.predef.MS_Internal_Xaml;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.MS_Internal.Xaml_Namespace;

public class Context_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Context", Xaml_Namespace.NAMESPACE);
    };


}
