package com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract_Collections;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract.Collections_Namespace;

public class IRemoteArgumentDictionaryContract {
    // full name: System.AddIn.Contract, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.AddIn.Contract\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.AddIn.Contract.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Collections_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IRemoteArgumentDictionaryContract", declaringType);
        } else {
            result = new CSharpInterface("IRemoteArgumentDictionaryContract", getNamespace());
        }
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract_Collections.IRemoteArgumentCollectionContract.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract_Collections.IRemoteArgumentEnumerableContract.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract.IContract.TYPE);
        return result;
    }

}
