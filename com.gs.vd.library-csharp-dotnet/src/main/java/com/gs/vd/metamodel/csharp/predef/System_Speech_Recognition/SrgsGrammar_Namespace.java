package com.gs.vd.metamodel.csharp.predef.System_Speech_Recognition;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Speech.Recognition_Namespace;

public class SrgsGrammar_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("SrgsGrammar", Recognition_Namespace.NAMESPACE);
    };


}
