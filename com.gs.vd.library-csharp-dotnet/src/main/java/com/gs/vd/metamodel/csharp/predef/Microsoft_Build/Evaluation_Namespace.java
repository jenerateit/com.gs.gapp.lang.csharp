package com.gs.vd.metamodel.csharp.predef.Microsoft_Build;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.Build_Namespace;

public class Evaluation_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Evaluation", Build_Namespace.NAMESPACE);
    };


}
