package com.gs.vd.metamodel.csharp.predef.System_Activities_Presentation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Activities.Presentation_Namespace;

public class PropertyEditing_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("PropertyEditing", Presentation_Namespace.NAMESPACE);
    };


}
