package com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_AddIn.Contract_Namespace;

public class Collections_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Collections", Contract_Namespace.NAMESPACE);
    };


}
