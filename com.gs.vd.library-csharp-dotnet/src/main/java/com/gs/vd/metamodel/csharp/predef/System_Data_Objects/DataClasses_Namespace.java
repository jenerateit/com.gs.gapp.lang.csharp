package com.gs.vd.metamodel.csharp.predef.System_Data_Objects;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data.Objects_Namespace;

public class DataClasses_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("DataClasses", Objects_Namespace.NAMESPACE);
    };


}
