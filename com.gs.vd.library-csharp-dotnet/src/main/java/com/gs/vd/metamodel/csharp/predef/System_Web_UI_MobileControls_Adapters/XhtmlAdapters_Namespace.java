package com.gs.vd.metamodel.csharp.predef.System_Web_UI_MobileControls_Adapters;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI_MobileControls.Adapters_Namespace;

public class XhtmlAdapters_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("XhtmlAdapters", Adapters_Namespace.NAMESPACE);
    };


}
