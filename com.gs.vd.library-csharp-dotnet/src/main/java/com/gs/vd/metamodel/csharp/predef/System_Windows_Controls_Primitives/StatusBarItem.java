package com.gs.vd.metamodel.csharp.predef.System_Windows_Controls_Primitives;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Controls.ContentControl;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Controls.Primitives_Namespace;

public class StatusBarItem {
    // full name: PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\PresentationFramework\v4.0_4.0.0.0__31bf3856ad364e35\PresentationFramework.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Primitives_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("StatusBarItem", declaringType);
        } else {
            result = new CSharpClass("StatusBarItem", getNamespace());
        }
        result.setBaseType(ContentControl.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Composition.IResource.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Animation.IAnimatable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IInputElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IFrameworkInputElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ISupportInitialize.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IHaveResources.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IQueryAmbient.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IAddChild.TYPE);
        return result;
    }

}
