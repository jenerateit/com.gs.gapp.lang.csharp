package com.gs.vd.metamodel.csharp.predef.System_Windows_Forms_Design;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.AccessibleObject;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.Design_Namespace;

public class ControlDesignerAccessibleObject {
    // full name: System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Design\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Design.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Design_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return ControlDesigner.TYPE;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ControlDesignerAccessibleObject", declaringType);
        } else {
            result = new CSharpClass("ControlDesignerAccessibleObject", getNamespace());
        }
        result.setBaseType(AccessibleObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Win32.IMarshal.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Reflection.IReflect.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Accessibility.IAccessible.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IEnumVariant.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleWindow.TYPE);
        return result;
    }

}
