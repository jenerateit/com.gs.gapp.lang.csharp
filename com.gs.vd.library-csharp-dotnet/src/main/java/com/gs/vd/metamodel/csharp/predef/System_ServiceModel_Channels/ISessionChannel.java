package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Channels_Namespace;

public class ISessionChannel {
    // full name: System.ServiceModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceModel\v4.0_4.0.0.0__b77a5c561934e089\System.ServiceModel.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Channels_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("ISessionChannel", declaringType);
        } else {
            result = new CSharpInterface("ISessionChannel", getNamespace());
        }
        CSharpTypeParameter typeParameter = null;
        Set<CSharpType> constraints = null;
        constraints = new LinkedHashSet<CSharpType>();
        constraints.add(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.ISession.TYPE);

        typeParameter = new CSharpTypeParameter("TSession", result, constraints, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
