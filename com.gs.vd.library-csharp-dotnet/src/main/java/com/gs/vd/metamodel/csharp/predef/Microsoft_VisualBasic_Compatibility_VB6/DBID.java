package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_Compatibility_VB6;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_Compatibility.VB6_Namespace;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;

public class DBID {
    // full name: Microsoft.VisualBasic.Compatibility.Data, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.VisualBasic.Compatibility.Data\v4.0_10.0.0.0__b03f5f7f11d50a3a\Microsoft.VisualBasic.Compatibility.Data.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return VB6_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("DBID", declaringType);
        } else {
            result = new CSharpStruct("DBID", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        return result;
    }

}
