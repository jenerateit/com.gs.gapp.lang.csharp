package com.gs.vd.metamodel.csharp.predef.System_Workflow_Activities_Rules;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Workflow_Activities.Rules_Namespace;

public class Design_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Design", Rules_Namespace.NAMESPACE);
    };


}
