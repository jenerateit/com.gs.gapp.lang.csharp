package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_MyServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic.MyServices_Namespace;

public class Internal_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Internal", MyServices_Namespace.NAMESPACE);
    };


}
