package com.gs.vd.metamodel.csharp.predef.System_Web_Security;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.Security_Namespace;

public class AntiXss_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("AntiXss", Security_Namespace.NAMESPACE);
    };


}
