package com.gs.vd.metamodel.csharp.predef.System_Security_Authentication_ExtendedProtection;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Security_Authentication.ExtendedProtection_Namespace;

public class Configuration_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Configuration", ExtendedProtection_Namespace.NAMESPACE);
    };


}
