package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels;

import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpGenericParameterAttributes;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Channels_Namespace;

public class ChannelListenerBase {
    // full name: System.ServiceModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceModel\v4.0_4.0.0.0__b77a5c561934e089\System.ServiceModel.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Channels_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ChannelListenerBase", declaringType);
        } else {
            result = new CSharpClass("ChannelListenerBase", getNamespace());
        }
        result.setBaseType(ChannelListenerBase.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel.ICommunicationObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel.IDefaultCommunicationTimeouts.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.IChannelListener.TYPE);
        CSharpTypeParameter typeParameter = null;
        Set<CSharpType> constraints = null;
        constraints = new LinkedHashSet<CSharpType>();
        constraints.add(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.IChannel.TYPE);

        typeParameter = new CSharpTypeParameter("TChannel", result, constraints, EnumSet.of(CSharpGenericParameterAttributes.REFERENCE_TYPE_CONSTRAINT));
        result.addGenericArguments(typeParameter);
        return result;
    }

}
