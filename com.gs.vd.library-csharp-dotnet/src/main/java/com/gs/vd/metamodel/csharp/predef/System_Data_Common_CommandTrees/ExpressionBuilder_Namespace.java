package com.gs.vd.metamodel.csharp.predef.System_Data_Common_CommandTrees;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data_Common.CommandTrees_Namespace;

public class ExpressionBuilder_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("ExpressionBuilder", CommandTrees_Namespace.NAMESPACE);
    };


}
