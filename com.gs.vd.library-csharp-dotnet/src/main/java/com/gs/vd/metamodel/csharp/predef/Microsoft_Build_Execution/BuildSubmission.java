package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Execution;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build.Execution_Namespace;
import com.gs.vd.metamodel.csharp.predef.System.Object;

public class BuildSubmission {
    // full name: Microsoft.Build, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.Build\v4.0_4.0.0.0__b03f5f7f11d50a3a\Microsoft.Build.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Execution_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("BuildSubmission", declaringType);
        } else {
            result = new CSharpClass("BuildSubmission", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        return result;
    }

}
