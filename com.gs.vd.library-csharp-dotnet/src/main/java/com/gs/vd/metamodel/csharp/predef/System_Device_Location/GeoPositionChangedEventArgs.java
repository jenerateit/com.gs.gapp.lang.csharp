package com.gs.vd.metamodel.csharp.predef.System_Device_Location;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System.EventArgs;
import com.gs.vd.metamodel.csharp.predef.System_Device.Location_Namespace;

public class GeoPositionChangedEventArgs {
    // full name: System.Device, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Device\v4.0_4.0.0.0__b77a5c561934e089\System.Device.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Location_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("GeoPositionChangedEventArgs", declaringType);
        } else {
            result = new CSharpClass("GeoPositionChangedEventArgs", getNamespace());
        }
        result.setBaseType(EventArgs.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("T", result, null, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
