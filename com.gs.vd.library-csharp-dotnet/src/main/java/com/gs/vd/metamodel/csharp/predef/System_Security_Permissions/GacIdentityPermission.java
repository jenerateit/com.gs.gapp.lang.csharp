package com.gs.vd.metamodel.csharp.predef.System_Security_Permissions;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Security.CodeAccessPermission;
import com.gs.vd.metamodel.csharp.predef.System_Security.Permissions_Namespace;

public class GacIdentityPermission {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Permissions_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("GacIdentityPermission", declaringType);
        } else {
            result = new CSharpClass("GacIdentityPermission", getNamespace());
        }
        result.setBaseType(CodeAccessPermission.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.IPermission.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.ISecurityEncodable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.IStackWalk.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security_Permissions.IBuiltInPermission.TYPE);
        return result;
    }

}
