package com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Attribute;
import com.gs.vd.metamodel.csharp.predef.System_Runtime.InteropServices_Namespace;

public class TypeLibFuncAttribute {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return InteropServices_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("TypeLibFuncAttribute", declaringType);
        } else {
            result = new CSharpClass("TypeLibFuncAttribute", getNamespace());
        }
        result.setBaseType(Attribute.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._Attribute.TYPE);
        return result;
    }

}
