package com.gs.vd.metamodel.csharp.predef.System_Web_UI_MobileControls_Adapters;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI_MobileControls.Adapters_Namespace;

public class HtmlPanelAdapter {
    // full name: System.Web.Mobile, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Web.Mobile\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Web.Mobile.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Adapters_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("HtmlPanelAdapter", declaringType);
        } else {
            result = new CSharpClass("HtmlPanelAdapter", getNamespace());
        }
        result.setBaseType(HtmlControlAdapter.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI_MobileControls.IControlAdapter.TYPE);
        return result;
    }

}
