package com.gs.vd.metamodel.csharp.predef.System_Net_PeerToPeer;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Net.PeerToPeer_Namespace;

public class Collaboration_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Collaboration", PeerToPeer_Namespace.NAMESPACE);
    };


}
