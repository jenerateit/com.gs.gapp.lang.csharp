package com.gs.vd.metamodel.csharp.predef.System_Data_Common_CommandTrees_ExpressionBuilder;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data_Common_CommandTrees.ExpressionBuilder_Namespace;

public class Spatial_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Spatial", ExpressionBuilder_Namespace.NAMESPACE);
    };


}
