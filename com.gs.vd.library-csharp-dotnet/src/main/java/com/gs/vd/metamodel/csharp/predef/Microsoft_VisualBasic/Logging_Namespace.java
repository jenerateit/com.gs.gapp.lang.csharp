package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.VisualBasic_Namespace;

public class Logging_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Logging", VisualBasic_Namespace.NAMESPACE);
    };


}
