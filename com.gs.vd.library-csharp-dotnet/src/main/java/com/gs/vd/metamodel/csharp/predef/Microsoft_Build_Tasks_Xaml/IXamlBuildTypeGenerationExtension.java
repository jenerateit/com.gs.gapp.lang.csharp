package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks_Xaml;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks.Xaml_Namespace;

public class IXamlBuildTypeGenerationExtension {
    // full name: XamlBuildTask, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\XamlBuildTask\v4.0_4.0.0.0__31bf3856ad364e35\XamlBuildTask.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Xaml_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IXamlBuildTypeGenerationExtension", declaringType);
        } else {
            result = new CSharpInterface("IXamlBuildTypeGenerationExtension", getNamespace());
        }
        return result;
    }

}
