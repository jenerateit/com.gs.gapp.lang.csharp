package com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Media3D;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Media.Media3D_Namespace;

public class Converters_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Converters", Media3D_Namespace.NAMESPACE);
    };


}
