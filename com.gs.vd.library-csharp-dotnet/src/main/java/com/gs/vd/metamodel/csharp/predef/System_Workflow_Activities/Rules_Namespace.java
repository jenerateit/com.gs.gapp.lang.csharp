package com.gs.vd.metamodel.csharp.predef.System_Workflow_Activities;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Workflow.Activities_Namespace;

public class Rules_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Rules", Activities_Namespace.NAMESPACE);
    };


}
