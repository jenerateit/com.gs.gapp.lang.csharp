package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks_Deployment_ManifestUtilities;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks_Deployment.ManifestUtilities_Namespace;
import com.gs.vd.metamodel.csharp.predef.System.Object;

public class FileReferenceCollection {
    // full name: Microsoft.Build.Tasks.v4.0, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.Build.Tasks.v4.0\v4.0_4.0.0.0__b03f5f7f11d50a3a\Microsoft.Build.Tasks.v4.0.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return ManifestUtilities_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("FileReferenceCollection", declaringType);
        } else {
            result = new CSharpClass("FileReferenceCollection", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        return result;
    }

}
