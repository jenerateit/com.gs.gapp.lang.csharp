package com.gs.vd.metamodel.csharp.predef.System_Deployment;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Deployment_Namespace;

public class Application_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Application", Deployment_Namespace.NAMESPACE);
    };


}
