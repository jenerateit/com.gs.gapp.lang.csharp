package com.gs.vd.metamodel.csharp.predef.System_Activities_Core;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Activities.Core_Namespace;

public class Presentation_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Presentation", Core_Namespace.NAMESPACE);
    };


}
