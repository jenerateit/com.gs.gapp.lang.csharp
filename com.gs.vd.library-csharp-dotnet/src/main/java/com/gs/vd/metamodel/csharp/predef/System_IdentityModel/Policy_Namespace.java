package com.gs.vd.metamodel.csharp.predef.System_IdentityModel;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.IdentityModel_Namespace;

public class Policy_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Policy", IdentityModel_Namespace.NAMESPACE);
    };


}
