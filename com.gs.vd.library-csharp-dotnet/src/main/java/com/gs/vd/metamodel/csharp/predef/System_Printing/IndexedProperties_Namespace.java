package com.gs.vd.metamodel.csharp.predef.System_Printing;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Printing_Namespace;

public class IndexedProperties_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("IndexedProperties", Printing_Namespace.NAMESPACE);
    };


}
