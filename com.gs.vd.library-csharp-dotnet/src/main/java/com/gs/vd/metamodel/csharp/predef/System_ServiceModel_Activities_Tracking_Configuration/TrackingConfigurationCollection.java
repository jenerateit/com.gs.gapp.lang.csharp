package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Activities_Tracking_Configuration;

import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpGenericParameterAttributes;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_Configuration.ConfigurationElementCollection;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Activities_Tracking.Configuration_Namespace;

public class TrackingConfigurationCollection {
    // full name: System.ServiceModel.Activities, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceModel.Activities\v4.0_4.0.0.0__31bf3856ad364e35\System.ServiceModel.Activities.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Configuration_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("TrackingConfigurationCollection", declaringType);
        } else {
            result = new CSharpClass("TrackingConfigurationCollection", getNamespace());
        }
        result.setBaseType(ConfigurationElementCollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.ICollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        CSharpTypeParameter typeParameter = null;
        Set<CSharpType> constraints = null;
        constraints = new LinkedHashSet<CSharpType>();
        constraints.add(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Activities_Tracking_Configuration.TrackingConfigurationElement.TYPE);

        typeParameter = new CSharpTypeParameter("TConfigurationElement", result, constraints, EnumSet.of(CSharpGenericParameterAttributes.DEFAULT_CONSTRUCTOR_CONSTRAINT));
        result.addGenericArguments(typeParameter);
        return result;
    }

}
