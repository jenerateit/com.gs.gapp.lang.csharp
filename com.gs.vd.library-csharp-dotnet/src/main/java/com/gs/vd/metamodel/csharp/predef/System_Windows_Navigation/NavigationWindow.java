package com.gs.vd.metamodel.csharp.predef.System_Windows_Navigation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Navigation_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Window;

public class NavigationWindow {
    // full name: PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\PresentationFramework\v4.0_4.0.0.0__31bf3856ad364e35\PresentationFramework.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Navigation_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("NavigationWindow", declaringType);
        } else {
            result = new CSharpClass("NavigationWindow", getNamespace());
        }
        result.setBaseType(Window.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Composition.IResource.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Animation.IAnimatable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IInputElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IFrameworkInputElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ISupportInitialize.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IHaveResources.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IQueryAmbient.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IAddChild.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IWindowService.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.MS_Internal_AppModel.INavigator.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.MS_Internal_AppModel.INavigatorBase.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.MS_Internal_AppModel.INavigatorImpl.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.MS_Internal_AppModel.IDownloader.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.MS_Internal_AppModel.IJournalNavigationScopeHost.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IUriContext.TYPE);
        return result;
    }

}
