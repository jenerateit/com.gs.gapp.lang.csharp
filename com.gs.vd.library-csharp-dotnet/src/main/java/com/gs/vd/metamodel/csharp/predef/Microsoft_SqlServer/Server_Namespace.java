package com.gs.vd.metamodel.csharp.predef.Microsoft_SqlServer;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.SqlServer_Namespace;

public class Server_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Server", SqlServer_Namespace.NAMESPACE);
    };


}
