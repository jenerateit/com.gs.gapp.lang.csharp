package com.gs.vd.metamodel.csharp.predef.System_Windows_Xps;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Xps_Namespace;

public class Packaging_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Packaging", Xps_Namespace.NAMESPACE);
    };


}
