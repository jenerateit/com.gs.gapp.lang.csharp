package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_Compatibility_VB6;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_Compatibility.VB6_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.ListBox;

public class DirListBox {
    // full name: Microsoft.VisualBasic.Compatibility, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.VisualBasic.Compatibility\v4.0_10.0.0.0__b03f5f7f11d50a3a\Microsoft.VisualBasic.Compatibility.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return VB6_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DirListBox", declaringType);
        } else {
            result = new CSharpClass("DirListBox", getNamespace());
        }
        result.setBaseType(ListBox.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleControl.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleInPlaceObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleInPlaceActiveObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleWindow.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IViewObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IViewObject2.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IPersist.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IPersistStreamInit.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IPersistPropertyBag.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IPersistStorage.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IQuickActivate.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.ISupportOleDropSource.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IDropTarget.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ISynchronizeInvoke.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IWin32Window.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms_Layout.IArrangedElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IBindableComponent.TYPE);
        return result;
    }

}
