package com.gs.vd.metamodel.csharp.predef.System_Data_SqlTypes;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;
import com.gs.vd.metamodel.csharp.predef.System_Data.SqlTypes_Namespace;

public class SqlDateTime {
    // full name: System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Data\v4.0_4.0.0.0__b77a5c561934e089\System.Data.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return SqlTypes_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("SqlDateTime", declaringType);
        } else {
            result = new CSharpStruct("SqlDateTime", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Data_SqlTypes.INullable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IComparable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Xml_Serialization.IXmlSerializable.TYPE);
        return result;
    }

}
