package com.gs.vd.metamodel.csharp.predef.System_Windows_Automation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Automation_Namespace;

public class Provider_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Provider", Automation_Namespace.NAMESPACE);
    };


}
