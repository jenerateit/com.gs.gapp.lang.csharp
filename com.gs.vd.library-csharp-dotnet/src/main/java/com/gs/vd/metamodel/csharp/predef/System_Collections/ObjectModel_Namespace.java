package com.gs.vd.metamodel.csharp.predef.System_Collections;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Collections_Namespace;

public class ObjectModel_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("ObjectModel", Collections_Namespace.NAMESPACE);
    };


}
