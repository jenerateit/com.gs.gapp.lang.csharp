package com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Composition_Hosting;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Composition.Hosting_Namespace;

public class CompositionContainer {
    // full name: System.ComponentModel.Composition, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ComponentModel.Composition\v4.0_4.0.0.0__b77a5c561934e089\System.ComponentModel.Composition.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Hosting_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("CompositionContainer", declaringType);
        } else {
            result = new CSharpClass("CompositionContainer", getNamespace());
        }
        result.setBaseType(ExportProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Composition.ICompositionService.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        return result;
    }

}
