package com.gs.vd.metamodel.csharp.predef.System_Web_UI_WebControls;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.CancelEventArgs;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI.WebControls_Namespace;

public class EntityDataSourceChangingEventArgs {
    // full name: System.Web.Entity, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Web.Entity\v4.0_4.0.0.0__b77a5c561934e089\System.Web.Entity.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return WebControls_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("EntityDataSourceChangingEventArgs", declaringType);
        } else {
            result = new CSharpClass("EntityDataSourceChangingEventArgs", getNamespace());
        }
        result.setBaseType(CancelEventArgs.TYPE);
        return result;
    }

}
