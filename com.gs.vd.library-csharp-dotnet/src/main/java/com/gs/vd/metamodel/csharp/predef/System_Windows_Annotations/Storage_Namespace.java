package com.gs.vd.metamodel.csharp.predef.System_Windows_Annotations;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Annotations_Namespace;

public class Storage_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Storage", Annotations_Namespace.NAMESPACE);
    };


}
