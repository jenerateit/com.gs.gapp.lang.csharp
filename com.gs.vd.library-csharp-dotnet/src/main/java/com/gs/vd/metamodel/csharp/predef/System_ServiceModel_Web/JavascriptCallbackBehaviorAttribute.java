package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Web;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Attribute;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Web_Namespace;

public class JavascriptCallbackBehaviorAttribute {
    // full name: System.ServiceModel.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceModel.Web\v4.0_4.0.0.0__31bf3856ad364e35\System.ServiceModel.Web.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Web_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("JavascriptCallbackBehaviorAttribute", declaringType);
        } else {
            result = new CSharpClass("JavascriptCallbackBehaviorAttribute", getNamespace());
        }
        result.setBaseType(Attribute.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._Attribute.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Description.IContractBehavior.TYPE);
        return result;
    }

}
