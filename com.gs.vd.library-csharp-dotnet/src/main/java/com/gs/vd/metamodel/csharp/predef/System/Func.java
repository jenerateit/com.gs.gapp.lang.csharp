package com.gs.vd.metamodel.csharp.predef.System;

import java.util.EnumSet;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpGenericParameterAttributes;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_Namespace;

public class Func {
    // full name: System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Core\v4.0_4.0.0.0__b77a5c561934e089\System.Core.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return System_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("Func", declaringType);
        } else {
            result = new CSharpClass("Func", getNamespace());
        }
        result.setBaseType(MulticastDelegate.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.ICloneable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.ISerializable.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("T1", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T2", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T3", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T4", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T5", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T6", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T7", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T8", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T9", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T10", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T11", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T12", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T13", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T14", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T15", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T16", result, null, EnumSet.of(CSharpGenericParameterAttributes.CONTRAVARIANT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TResult", result, null, EnumSet.of(CSharpGenericParameterAttributes.COVARIANT));
        result.addGenericArguments(typeParameter);
        return result;
    }

}
