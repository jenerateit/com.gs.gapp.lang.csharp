package com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Design_Serialization;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Design.Serialization_Namespace;

public class IDesignerLoaderHost {
    // full name: System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System\v4.0_4.0.0.0__b77a5c561934e089\System.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Serialization_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IDesignerLoaderHost", declaringType);
        } else {
            result = new CSharpInterface("IDesignerLoaderHost", getNamespace());
        }
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Design.IDesignerHost.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Design.IServiceContainer.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IServiceProvider.TYPE);
        return result;
    }

}
