package com.gs.vd.metamodel.csharp.predef.System_IdentityModel_Services;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_IdentityModel.Services_Namespace;

public class Tokens_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Tokens", Services_Namespace.NAMESPACE);
    };


}
