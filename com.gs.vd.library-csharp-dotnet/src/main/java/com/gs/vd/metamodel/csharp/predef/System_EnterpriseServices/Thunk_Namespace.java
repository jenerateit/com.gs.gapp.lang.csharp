package com.gs.vd.metamodel.csharp.predef.System_EnterpriseServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.EnterpriseServices_Namespace;

public class Thunk_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Thunk", EnterpriseServices_Namespace.NAMESPACE);
    };


}
