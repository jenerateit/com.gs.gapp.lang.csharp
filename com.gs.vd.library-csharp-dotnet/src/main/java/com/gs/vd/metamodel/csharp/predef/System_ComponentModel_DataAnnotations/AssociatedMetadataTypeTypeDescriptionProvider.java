package com.gs.vd.metamodel.csharp.predef.System_ComponentModel_DataAnnotations;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.DataAnnotations_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.TypeDescriptionProvider;

public class AssociatedMetadataTypeTypeDescriptionProvider {
    // full name: System.ComponentModel.DataAnnotations, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ComponentModel.DataAnnotations\v4.0_4.0.0.0__31bf3856ad364e35\System.ComponentModel.DataAnnotations.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return DataAnnotations_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("AssociatedMetadataTypeTypeDescriptionProvider", declaringType);
        } else {
            result = new CSharpClass("AssociatedMetadataTypeTypeDescriptionProvider", getNamespace());
        }
        result.setBaseType(TypeDescriptionProvider.TYPE);
        return result;
    }

}
