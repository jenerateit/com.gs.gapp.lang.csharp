package com.gs.vd.metamodel.csharp.predef.System_Net_Http;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Net.Http_Namespace;

public class Headers_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Headers", Http_Namespace.NAMESPACE);
    };


}
