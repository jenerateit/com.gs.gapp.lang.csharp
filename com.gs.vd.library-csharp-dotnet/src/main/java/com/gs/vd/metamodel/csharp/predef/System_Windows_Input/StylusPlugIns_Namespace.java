package com.gs.vd.metamodel.csharp.predef.System_Windows_Input;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Input_Namespace;

public class StylusPlugIns_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("StylusPlugIns", Input_Namespace.NAMESPACE);
    };


}
