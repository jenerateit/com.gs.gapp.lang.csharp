package com.gs.vd.metamodel.csharp.predef.System_Web_UI;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Web.UI_Namespace;

public class ScriptManager {
    // full name: System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Web.Extensions\v4.0_4.0.0.0__31bf3856ad364e35\System.Web.Extensions.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return UI_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ScriptManager", declaringType);
        } else {
            result = new CSharpClass("ScriptManager", getNamespace());
        }
        result.setBaseType(Control.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IParserAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IUrlResolutionService.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IDataBindingsAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IControlBuilderAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IControlDesignerAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IExpressionsAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IPostBackDataHandler.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IPostBackEventHandler.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IControl.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IClientUrlResolver.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IScriptManager.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IScriptManagerInternal.TYPE);
        return result;
    }

}
