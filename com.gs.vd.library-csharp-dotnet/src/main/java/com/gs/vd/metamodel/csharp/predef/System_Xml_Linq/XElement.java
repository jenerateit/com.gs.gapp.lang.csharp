package com.gs.vd.metamodel.csharp.predef.System_Xml_Linq;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Xml.Linq_Namespace;

public class XElement {
    // full name: System.Xml.Linq, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Xml.Linq\v4.0_4.0.0.0__b77a5c561934e089\System.Xml.Linq.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Linq_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("XElement", declaringType);
        } else {
            result = new CSharpClass("XElement", getNamespace());
        }
        result.setBaseType(XContainer.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Xml.IXmlLineInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Xml_Serialization.IXmlSerializable.TYPE);
        return result;
    }

}
