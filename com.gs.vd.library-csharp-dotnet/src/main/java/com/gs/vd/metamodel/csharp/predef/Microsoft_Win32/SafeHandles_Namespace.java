package com.gs.vd.metamodel.csharp.predef.Microsoft_Win32;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.Win32_Namespace;

public class SafeHandles_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("SafeHandles", Win32_Namespace.NAMESPACE);
    };


}
