package com.gs.vd.metamodel.csharp.predef.System_Data_Metadata;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data.Metadata_Namespace;

public class Edm_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Edm", Metadata_Namespace.NAMESPACE);
    };


}
