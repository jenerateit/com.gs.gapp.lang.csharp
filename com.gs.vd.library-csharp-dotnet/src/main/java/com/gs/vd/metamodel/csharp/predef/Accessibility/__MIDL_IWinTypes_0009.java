package com.gs.vd.metamodel.csharp.predef.Accessibility;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Accessibility_Namespace;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;

public class __MIDL_IWinTypes_0009 {
    // full name: Accessibility, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Accessibility\v4.0_4.0.0.0__b03f5f7f11d50a3a\Accessibility.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Accessibility_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("__MIDL_IWinTypes_0009", declaringType);
        } else {
            result = new CSharpStruct("__MIDL_IWinTypes_0009", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        return result;
    }

}
