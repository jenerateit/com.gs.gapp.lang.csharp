package com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Runtime.Serialization_Namespace;

public class Formatters_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Formatters", Serialization_Namespace.NAMESPACE);
    };


}
