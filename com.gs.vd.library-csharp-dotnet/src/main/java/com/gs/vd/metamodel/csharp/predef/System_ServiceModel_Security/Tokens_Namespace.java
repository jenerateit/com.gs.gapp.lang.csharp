package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Security;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Security_Namespace;

public class Tokens_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Tokens", Security_Namespace.NAMESPACE);
    };


}
