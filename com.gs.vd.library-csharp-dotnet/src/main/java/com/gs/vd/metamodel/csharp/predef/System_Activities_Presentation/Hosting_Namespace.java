package com.gs.vd.metamodel.csharp.predef.System_Activities_Presentation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Activities.Presentation_Namespace;

public class Hosting_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Hosting", Presentation_Namespace.NAMESPACE);
    };


}
