package com.gs.vd.metamodel.csharp.predef.System_DirectoryServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.DirectoryServices_Namespace;

public class AccountManagement_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("AccountManagement", DirectoryServices_Namespace.NAMESPACE);
    };


}
