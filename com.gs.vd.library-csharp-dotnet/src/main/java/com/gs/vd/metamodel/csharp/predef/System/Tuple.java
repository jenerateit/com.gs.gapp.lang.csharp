package com.gs.vd.metamodel.csharp.predef.System;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_Namespace;

public class Tuple {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return System_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("Tuple", declaringType);
        } else {
            result = new CSharpClass("Tuple", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IStructuralEquatable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IStructuralComparable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IComparable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.ITuple.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("T1", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T2", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T3", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T4", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T5", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T6", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("T7", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TRest", result, null, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
