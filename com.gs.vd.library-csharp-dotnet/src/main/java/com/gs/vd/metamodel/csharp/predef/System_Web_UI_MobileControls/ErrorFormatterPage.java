package com.gs.vd.metamodel.csharp.predef.System_Web_UI_MobileControls;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI.MobileControls_Namespace;

public class ErrorFormatterPage {
    // full name: System.Web.Mobile, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Web.Mobile\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Web.Mobile.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return MobileControls_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ErrorFormatterPage", declaringType);
        } else {
            result = new CSharpClass("ErrorFormatterPage", getNamespace());
        }
        result.setBaseType(MobilePage.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IParserAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IUrlResolutionService.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IDataBindingsAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IControlBuilderAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IControlDesignerAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IExpressionsAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.INamingContainer.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IFilterResolutionService.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web.IHttpHandler.TYPE);
        return result;
    }

}
