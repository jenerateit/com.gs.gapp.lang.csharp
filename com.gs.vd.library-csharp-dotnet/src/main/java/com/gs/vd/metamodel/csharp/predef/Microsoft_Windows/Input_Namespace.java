package com.gs.vd.metamodel.csharp.predef.Microsoft_Windows;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.Windows_Namespace;

public class Input_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Input", Windows_Namespace.NAMESPACE);
    };


}
