package com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Composition;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.Composition_Namespace;

public class ReflectionModel_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("ReflectionModel", Composition_Namespace.NAMESPACE);
    };


}
