package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Discovery;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Discovery_Namespace;

public class VersionCD1_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("VersionCD1", Discovery_Namespace.NAMESPACE);
    };


}
