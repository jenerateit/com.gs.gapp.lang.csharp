package com.gs.vd.metamodel.csharp.predef.System_Reflection_Emit;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Reflection.Emit_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Reflection.FieldInfo;

public class FieldBuilder {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Emit_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("FieldBuilder", declaringType);
        } else {
            result = new CSharpClass("FieldBuilder", getNamespace());
        }
        result.setBaseType(FieldInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Reflection.ICustomAttributeProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._MemberInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._FieldInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._FieldBuilder.TYPE);
        return result;
    }

}
