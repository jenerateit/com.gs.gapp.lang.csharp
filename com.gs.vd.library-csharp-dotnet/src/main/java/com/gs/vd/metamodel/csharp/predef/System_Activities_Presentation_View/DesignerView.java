package com.gs.vd.metamodel.csharp.predef.System_Activities_Presentation_View;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Activities_Presentation.View_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Controls.UserControl;

public class DesignerView {
    // full name: System.Activities.Presentation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Activities.Presentation\v4.0_4.0.0.0__31bf3856ad364e35\System.Activities.Presentation.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return View_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DesignerView", declaringType);
        } else {
            result = new CSharpClass("DesignerView", getNamespace());
        }
        result.setBaseType(UserControl.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Composition.IResource.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Animation.IAnimatable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IInputElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IFrameworkInputElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ISupportInitialize.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IHaveResources.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IQueryAmbient.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IAddChild.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IComponentConnector.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IStyleConnector.TYPE);
        return result;
    }

}
