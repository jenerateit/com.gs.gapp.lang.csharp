package com.gs.vd.metamodel.csharp.predef.System_Deployment_Internal;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Deployment.Internal_Namespace;

public class CodeSigning_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("CodeSigning", Internal_Namespace.NAMESPACE);
    };


}
