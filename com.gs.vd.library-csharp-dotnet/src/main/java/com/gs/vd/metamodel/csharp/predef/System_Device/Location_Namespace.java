package com.gs.vd.metamodel.csharp.predef.System_Device;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Device_Namespace;

public class Location_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Location", Device_Namespace.NAMESPACE);
    };


}
