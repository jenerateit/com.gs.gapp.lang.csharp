package com.gs.vd.metamodel.csharp.predef.System_Activities_Presentation_View;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Activities_Presentation.View_Namespace;

public class OutlineView_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("OutlineView", View_Namespace.NAMESPACE);
    };


}
