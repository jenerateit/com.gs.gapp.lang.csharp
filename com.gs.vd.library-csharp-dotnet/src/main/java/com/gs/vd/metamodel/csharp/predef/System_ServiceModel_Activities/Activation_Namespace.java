package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Activities;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Activities_Namespace;

public class Activation_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Activation", Activities_Namespace.NAMESPACE);
    };


}
