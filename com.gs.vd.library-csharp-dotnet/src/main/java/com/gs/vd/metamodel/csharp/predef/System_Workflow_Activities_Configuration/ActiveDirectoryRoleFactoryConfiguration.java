package com.gs.vd.metamodel.csharp.predef.System_Workflow_Activities_Configuration;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Configuration.ConfigurationSection;
import com.gs.vd.metamodel.csharp.predef.System_Workflow_Activities.Configuration_Namespace;

public class ActiveDirectoryRoleFactoryConfiguration {
    // full name: System.Workflow.Activities, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Workflow.Activities\v4.0_4.0.0.0__31bf3856ad364e35\System.Workflow.Activities.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Configuration_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ActiveDirectoryRoleFactoryConfiguration", declaringType);
        } else {
            result = new CSharpClass("ActiveDirectoryRoleFactoryConfiguration", getNamespace());
        }
        result.setBaseType(ConfigurationSection.TYPE);
        return result;
    }

}
