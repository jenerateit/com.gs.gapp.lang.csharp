package com.gs.vd.metamodel.csharp.predef.System_Web_DynamicData;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.DynamicData_Namespace;

public class ModelProviders_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("ModelProviders", DynamicData_Namespace.NAMESPACE);
    };


}
