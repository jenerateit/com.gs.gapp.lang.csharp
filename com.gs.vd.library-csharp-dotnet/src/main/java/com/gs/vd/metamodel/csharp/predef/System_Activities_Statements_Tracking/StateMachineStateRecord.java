package com.gs.vd.metamodel.csharp.predef.System_Activities_Statements_Tracking;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Activities_Statements.Tracking_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Activities_Tracking.CustomTrackingRecord;

public class StateMachineStateRecord {
    // full name: System.Activities, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Activities\v4.0_4.0.0.0__31bf3856ad364e35\System.Activities.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Tracking_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("StateMachineStateRecord", declaringType);
        } else {
            result = new CSharpClass("StateMachineStateRecord", getNamespace());
        }
        result.setBaseType(CustomTrackingRecord.TYPE);
        return result;
    }

}
