package com.gs.vd.metamodel.csharp.predef.System_IdentityModel_Services_Configuration;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.EventArgs;
import com.gs.vd.metamodel.csharp.predef.System_IdentityModel_Services.Configuration_Namespace;

public class FederationConfigurationCreatedEventArgs {
    // full name: System.IdentityModel.Services, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.IdentityModel.Services\v4.0_4.0.0.0__b77a5c561934e089\System.IdentityModel.Services.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Configuration_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("FederationConfigurationCreatedEventArgs", declaringType);
        } else {
            result = new CSharpClass("FederationConfigurationCreatedEventArgs", getNamespace());
        }
        result.setBaseType(EventArgs.TYPE);
        return result;
    }

}
