package com.gs.vd.metamodel.csharp.predef.System_Security;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Security_Namespace;

public class Cryptography_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Cryptography", Security_Namespace.NAMESPACE);
    };


}
