package com.gs.vd.metamodel.csharp.predef.System_Runtime_Caching;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Runtime.Caching_Namespace;

public class Hosting_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Hosting", Caching_Namespace.NAMESPACE);
    };


}
