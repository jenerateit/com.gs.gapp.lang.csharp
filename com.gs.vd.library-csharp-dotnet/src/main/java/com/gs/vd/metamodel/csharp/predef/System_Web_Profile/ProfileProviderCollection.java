package com.gs.vd.metamodel.csharp.predef.System_Web_Profile;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Configuration.SettingsProviderCollection;
import com.gs.vd.metamodel.csharp.predef.System_Web.Profile_Namespace;

public class ProfileProviderCollection {
    // full name: System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Web\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Web.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Profile_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ProfileProviderCollection", declaringType);
        } else {
            result = new CSharpClass("ProfileProviderCollection", getNamespace());
        }
        result.setBaseType(SettingsProviderCollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.ICollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        return result;
    }

}
