package com.gs.vd.metamodel.csharp.predef.System_Security;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Security_Namespace;

public class Policy_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Policy", Security_Namespace.NAMESPACE);
    };


}
