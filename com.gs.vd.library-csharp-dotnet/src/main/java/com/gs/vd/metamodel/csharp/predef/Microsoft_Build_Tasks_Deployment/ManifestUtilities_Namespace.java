package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks_Deployment;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks.Deployment_Namespace;

public class ManifestUtilities_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("ManifestUtilities", Deployment_Namespace.NAMESPACE);
    };


}
