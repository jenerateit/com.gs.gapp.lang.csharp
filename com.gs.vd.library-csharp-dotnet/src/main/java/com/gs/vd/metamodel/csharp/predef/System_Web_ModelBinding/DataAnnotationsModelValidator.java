package com.gs.vd.metamodel.csharp.predef.System_Web_ModelBinding;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_Web.ModelBinding_Namespace;

public class DataAnnotationsModelValidator {
    // full name: System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Web\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Web.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return ModelBinding_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DataAnnotationsModelValidator", declaringType);
        } else {
            result = new CSharpClass("DataAnnotationsModelValidator", getNamespace());
        }
        result.setBaseType(DataAnnotationsModelValidator.TYPE);
        CSharpTypeParameter typeParameter = null;
        Set<CSharpType> constraints = null;
        constraints = new LinkedHashSet<CSharpType>();
        constraints.add(com.gs.vd.metamodel.csharp.predef.System_ComponentModel_DataAnnotations.ValidationAttribute.TYPE);

        typeParameter = new CSharpTypeParameter("TAttribute", result, constraints, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
