package com.gs.vd.metamodel.csharp.predef.System_Xaml;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Xaml_Namespace;

public class Permissions_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Permissions", Xaml_Namespace.NAMESPACE);
    };


}
