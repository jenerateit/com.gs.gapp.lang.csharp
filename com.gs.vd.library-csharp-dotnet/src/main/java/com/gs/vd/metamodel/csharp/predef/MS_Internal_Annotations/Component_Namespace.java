package com.gs.vd.metamodel.csharp.predef.MS_Internal_Annotations;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.MS_Internal.Annotations_Namespace;

public class Component_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Component", Annotations_Namespace.NAMESPACE);
    };


}
