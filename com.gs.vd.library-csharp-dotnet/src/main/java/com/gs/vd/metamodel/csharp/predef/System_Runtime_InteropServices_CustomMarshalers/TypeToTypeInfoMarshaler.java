package com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices_CustomMarshalers;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Object;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices.CustomMarshalers_Namespace;

public class TypeToTypeInfoMarshaler {
    // full name: CustomMarshalers, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\CustomMarshalers\v4.0_4.0.0.0__b03f5f7f11d50a3a\CustomMarshalers.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return CustomMarshalers_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("TypeToTypeInfoMarshaler", declaringType);
        } else {
            result = new CSharpClass("TypeToTypeInfoMarshaler", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices.ICustomMarshaler.TYPE);
        return result;
    }

}
