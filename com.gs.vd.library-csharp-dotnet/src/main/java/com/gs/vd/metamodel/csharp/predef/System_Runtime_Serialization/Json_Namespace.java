package com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Runtime.Serialization_Namespace;

public class Json_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Json", Serialization_Namespace.NAMESPACE);
    };


}
