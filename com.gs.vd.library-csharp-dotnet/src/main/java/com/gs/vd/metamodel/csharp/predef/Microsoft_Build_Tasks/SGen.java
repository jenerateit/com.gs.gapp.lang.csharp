package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build.Tasks_Namespace;

public class SGen {
    // full name: Microsoft.Build.Tasks.v4.0, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.Build.Tasks.v4.0\v4.0_4.0.0.0__b03f5f7f11d50a3a\Microsoft.Build.Tasks.v4.0.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Tasks_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("SGen", declaringType);
        } else {
            result = new CSharpClass("SGen", getNamespace());
        }
        result.setBaseType(ToolTaskExtension.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Framework.ITask.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Framework.ICancelableTask.TYPE);
        return result;
    }

}
