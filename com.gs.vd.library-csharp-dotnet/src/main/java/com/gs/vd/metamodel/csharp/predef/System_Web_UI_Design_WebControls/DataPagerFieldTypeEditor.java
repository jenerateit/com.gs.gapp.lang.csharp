package com.gs.vd.metamodel.csharp.predef.System_Web_UI_Design_WebControls;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Drawing_Design.UITypeEditor;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI_Design.WebControls_Namespace;

public class DataPagerFieldTypeEditor {
    // full name: System.Web.Extensions.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Web.Extensions.Design\v4.0_4.0.0.0__31bf3856ad364e35\System.Web.Extensions.Design.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return WebControls_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DataPagerFieldTypeEditor", declaringType);
        } else {
            result = new CSharpClass("DataPagerFieldTypeEditor", getNamespace());
        }
        result.setBaseType(UITypeEditor.TYPE);
        return result;
    }

}
