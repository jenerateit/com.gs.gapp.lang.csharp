package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualC_StlClr;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.Microsoft_VisualC.StlClr_Namespace;

public class DequeEnumerator {
    // full name: Microsoft.VisualC.STLCLR, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.VisualC.STLCLR\v4.0_2.0.0.0__b03f5f7f11d50a3a\Microsoft.VisualC.STLCLR.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return StlClr_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DequeEnumerator", declaringType);
        } else {
            result = new CSharpClass("DequeEnumerator", getNamespace());
        }
        // type has no base type
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerator.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("TValue", result, null, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
