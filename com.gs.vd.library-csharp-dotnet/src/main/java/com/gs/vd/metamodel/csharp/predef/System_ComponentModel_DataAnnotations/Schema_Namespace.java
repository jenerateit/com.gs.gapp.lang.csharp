package com.gs.vd.metamodel.csharp.predef.System_ComponentModel_DataAnnotations;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.DataAnnotations_Namespace;

public class Schema_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Schema", DataAnnotations_Namespace.NAMESPACE);
    };


}
