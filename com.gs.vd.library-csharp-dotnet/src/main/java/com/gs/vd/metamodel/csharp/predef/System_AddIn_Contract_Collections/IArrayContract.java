package com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract_Collections;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract.Collections_Namespace;

public class IArrayContract {
    // full name: System.AddIn.Contract, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.AddIn.Contract\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.AddIn.Contract.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Collections_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IArrayContract", declaringType);
        } else {
            result = new CSharpInterface("IArrayContract", getNamespace());
        }
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract.IContract.TYPE);
        CSharpTypeParameter typeParameter = null;
        Set<CSharpType> constraints = null;
        constraints = new LinkedHashSet<CSharpType>();
        constraints.add(com.gs.vd.metamodel.csharp.predef.System_AddIn_Contract.IContract.TYPE);

        typeParameter = new CSharpTypeParameter("C", result, constraints, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
