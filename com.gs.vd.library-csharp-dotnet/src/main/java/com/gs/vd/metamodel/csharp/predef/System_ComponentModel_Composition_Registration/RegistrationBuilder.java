package com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Composition_Registration;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Composition.Registration_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Reflection_Context.CustomReflectionContext;

public class RegistrationBuilder {
    // full name: System.ComponentModel.Composition.Registration, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ComponentModel.Composition.Registration\v4.0_4.0.0.0__b77a5c561934e089\System.ComponentModel.Composition.Registration.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Registration_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("RegistrationBuilder", declaringType);
        } else {
            result = new CSharpClass("RegistrationBuilder", getNamespace());
        }
        result.setBaseType(CustomReflectionContext.TYPE);
        return result;
    }

}
