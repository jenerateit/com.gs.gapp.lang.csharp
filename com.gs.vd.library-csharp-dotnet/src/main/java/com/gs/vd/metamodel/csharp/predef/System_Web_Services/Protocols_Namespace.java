package com.gs.vd.metamodel.csharp.predef.System_Web_Services;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.Services_Namespace;

public class Protocols_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Protocols", Services_Namespace.NAMESPACE);
    };


}
