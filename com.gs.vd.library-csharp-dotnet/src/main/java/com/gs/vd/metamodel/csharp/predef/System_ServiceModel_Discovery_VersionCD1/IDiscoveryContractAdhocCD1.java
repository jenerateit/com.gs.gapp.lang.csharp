package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Discovery_VersionCD1;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Discovery.VersionCD1_Namespace;

public class IDiscoveryContractAdhocCD1 {
    // full name: System.ServiceModel.Discovery, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceModel.Discovery\v4.0_4.0.0.0__31bf3856ad364e35\System.ServiceModel.Discovery.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return VersionCD1_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IDiscoveryContractAdhocCD1", declaringType);
        } else {
            result = new CSharpInterface("IDiscoveryContractAdhocCD1", getNamespace());
        }
        return result;
    }

}
