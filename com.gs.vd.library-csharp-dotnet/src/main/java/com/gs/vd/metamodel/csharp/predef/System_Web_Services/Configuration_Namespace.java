package com.gs.vd.metamodel.csharp.predef.System_Web_Services;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.Services_Namespace;

public class Configuration_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Configuration", Services_Namespace.NAMESPACE);
    };


}
