package com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Runtime.InteropServices_Namespace;

public class ComTypes_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("ComTypes", InteropServices_Namespace.NAMESPACE);
    };


}
