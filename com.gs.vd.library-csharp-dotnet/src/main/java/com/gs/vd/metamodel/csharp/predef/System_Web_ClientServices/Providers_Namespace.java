package com.gs.vd.metamodel.csharp.predef.System_Web_ClientServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.ClientServices_Namespace;

public class Providers_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Providers", ClientServices_Namespace.NAMESPACE);
    };


}
