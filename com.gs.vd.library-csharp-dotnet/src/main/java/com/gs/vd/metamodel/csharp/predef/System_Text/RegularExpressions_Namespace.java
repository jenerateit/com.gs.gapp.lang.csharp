package com.gs.vd.metamodel.csharp.predef.System_Text;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Text_Namespace;

public class RegularExpressions_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("RegularExpressions", Text_Namespace.NAMESPACE);
    };


}
