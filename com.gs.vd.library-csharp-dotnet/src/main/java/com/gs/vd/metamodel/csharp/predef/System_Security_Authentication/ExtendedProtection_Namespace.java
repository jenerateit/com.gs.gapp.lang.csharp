package com.gs.vd.metamodel.csharp.predef.System_Security_Authentication;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Security.Authentication_Namespace;

public class ExtendedProtection_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("ExtendedProtection", Authentication_Namespace.NAMESPACE);
    };


}
