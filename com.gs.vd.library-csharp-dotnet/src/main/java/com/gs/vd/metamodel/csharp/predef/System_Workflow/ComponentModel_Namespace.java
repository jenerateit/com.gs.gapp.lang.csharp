package com.gs.vd.metamodel.csharp.predef.System_Workflow;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Workflow_Namespace;

public class ComponentModel_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("ComponentModel", Workflow_Namespace.NAMESPACE);
    };


}
