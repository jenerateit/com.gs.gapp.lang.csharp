package com.gs.vd.metamodel.csharp.predef.System_Data_Services;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data.Services_Namespace;

public class Design_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Design", Services_Namespace.NAMESPACE);
    };


}
