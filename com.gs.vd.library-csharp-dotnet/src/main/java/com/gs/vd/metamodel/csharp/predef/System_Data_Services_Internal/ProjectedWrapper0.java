package com.gs.vd.metamodel.csharp.predef.System_Data_Services_Internal;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Data_Services.Internal_Namespace;

public class ProjectedWrapper0 {
    // full name: System.Data.Services, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Data.Services\v4.0_4.0.0.0__b77a5c561934e089\System.Data.Services.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Internal_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ProjectedWrapper0", declaringType);
        } else {
            result = new CSharpClass("ProjectedWrapper0", getNamespace());
        }
        result.setBaseType(ProjectedWrapper.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Data_Services.IProjectedResult.TYPE);
        return result;
    }

}
