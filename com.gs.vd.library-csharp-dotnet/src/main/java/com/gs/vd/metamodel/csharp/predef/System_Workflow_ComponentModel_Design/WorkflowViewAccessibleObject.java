package com.gs.vd.metamodel.csharp.predef.System_Workflow_ComponentModel_Design;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.ControlAccessibleObject;
import com.gs.vd.metamodel.csharp.predef.System_Workflow_ComponentModel.Design_Namespace;

public class WorkflowViewAccessibleObject {
    // full name: System.Workflow.ComponentModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Workflow.ComponentModel\v4.0_4.0.0.0__31bf3856ad364e35\System.Workflow.ComponentModel.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Design_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("WorkflowViewAccessibleObject", declaringType);
        } else {
            result = new CSharpClass("WorkflowViewAccessibleObject", getNamespace());
        }
        result.setBaseType(ControlAccessibleObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Win32.IMarshal.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Reflection.IReflect.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Accessibility.IAccessible.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IEnumVariant.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleWindow.TYPE);
        return result;
    }

}
