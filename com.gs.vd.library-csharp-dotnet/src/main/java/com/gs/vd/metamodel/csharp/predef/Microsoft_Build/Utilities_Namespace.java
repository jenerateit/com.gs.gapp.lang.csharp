package com.gs.vd.metamodel.csharp.predef.Microsoft_Build;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.Build_Namespace;

public class Utilities_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Utilities", Build_Namespace.NAMESPACE);
    };


}
