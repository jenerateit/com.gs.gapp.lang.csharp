package com.gs.vd.metamodel.csharp.predef.Microsoft_JScript;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.JScript_Namespace;

public class Vsa_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Vsa", JScript_Namespace.NAMESPACE);
    };


}
