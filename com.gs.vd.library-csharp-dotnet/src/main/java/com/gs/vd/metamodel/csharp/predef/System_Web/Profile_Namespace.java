package com.gs.vd.metamodel.csharp.predef.System_Web;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Web_Namespace;

public class Profile_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Profile", Web_Namespace.NAMESPACE);
    };


}
