package com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting_Messaging;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.MulticastDelegate;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting.Messaging_Namespace;

public class MessageSurrogateFilter {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Messaging_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("MessageSurrogateFilter", declaringType);
        } else {
            result = new CSharpClass("MessageSurrogateFilter", getNamespace());
        }
        result.setBaseType(MulticastDelegate.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.ICloneable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.ISerializable.TYPE);
        return result;
    }

}
