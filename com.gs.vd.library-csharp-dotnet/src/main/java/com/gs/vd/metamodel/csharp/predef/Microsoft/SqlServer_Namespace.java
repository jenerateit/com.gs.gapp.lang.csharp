package com.gs.vd.metamodel.csharp.predef.Microsoft;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Namespace;

public class SqlServer_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("SqlServer", Microsoft_Namespace.NAMESPACE);
    };


}
