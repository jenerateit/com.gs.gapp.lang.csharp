package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks_Deployment;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks.Deployment_Namespace;

public class Bootstrapper_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Bootstrapper", Deployment_Namespace.NAMESPACE);
    };


}
