package com.gs.vd.metamodel.csharp.predef.System_Activities_Core_Presentation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Activities_Core.Presentation_Namespace;

public class Themes_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Themes", Presentation_Namespace.NAMESPACE);
    };


}
