package com.gs.vd.metamodel.csharp.predef.System_Diagnostics;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Diagnostics_Namespace;

public class SymbolStore_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("SymbolStore", Diagnostics_Namespace.NAMESPACE);
    };


}
