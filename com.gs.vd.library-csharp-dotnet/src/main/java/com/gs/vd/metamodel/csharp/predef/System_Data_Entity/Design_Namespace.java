package com.gs.vd.metamodel.csharp.predef.System_Data_Entity;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data.Entity_Namespace;

public class Design_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Design", Entity_Namespace.NAMESPACE);
    };


}
