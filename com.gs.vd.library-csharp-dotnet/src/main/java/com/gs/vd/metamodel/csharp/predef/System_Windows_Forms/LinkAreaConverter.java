package com.gs.vd.metamodel.csharp.predef.System_Windows_Forms;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.TypeConverter;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Forms_Namespace;

public class LinkAreaConverter {
    // full name: System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Windows.Forms\v4.0_4.0.0.0__b77a5c561934e089\System.Windows.Forms.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Forms_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return LinkArea.TYPE;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("LinkAreaConverter", declaringType);
        } else {
            result = new CSharpClass("LinkAreaConverter", getNamespace());
        }
        result.setBaseType(TypeConverter.TYPE);
        return result;
    }

}
