package com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Imaging;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Media.ImageMetadata;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Media.Imaging_Namespace;

public class BitmapMetadata {
    // full name: PresentationCore, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\PresentationCore\v4.0_4.0.0.0__31bf3856ad364e35\PresentationCore.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Imaging_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("BitmapMetadata", declaringType);
        } else {
            result = new CSharpClass("BitmapMetadata", getNamespace());
        }
        result.setBaseType(ImageMetadata.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.ISealable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        return result;
    }

}
