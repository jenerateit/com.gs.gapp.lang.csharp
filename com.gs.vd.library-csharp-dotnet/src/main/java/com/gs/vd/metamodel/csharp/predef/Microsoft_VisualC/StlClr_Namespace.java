package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualC;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.VisualC_Namespace;

public class StlClr_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("StlClr", VisualC_Namespace.NAMESPACE);
    };


}
