package com.gs.vd.metamodel.csharp.predef.System_Data_Common;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data.Common_Namespace;

public class EntitySql_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("EntitySql", Common_Namespace.NAMESPACE);
    };


}
