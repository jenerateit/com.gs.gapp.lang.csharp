package com.gs.vd.metamodel.csharp.predef.System_IdentityModel_Protocols;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_IdentityModel.Protocols_Namespace;

public class WSTrust_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("WSTrust", Protocols_Namespace.NAMESPACE);
    };


}
