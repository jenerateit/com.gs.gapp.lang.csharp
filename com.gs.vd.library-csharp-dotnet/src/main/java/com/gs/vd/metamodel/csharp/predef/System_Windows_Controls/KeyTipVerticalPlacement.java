package com.gs.vd.metamodel.csharp.predef.System_Windows_Controls;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Enum;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Controls_Namespace;

public class KeyTipVerticalPlacement {
    // full name: System.Windows.Controls.Ribbon, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Windows.Controls.Ribbon\v4.0_4.0.0.0__b77a5c561934e089\System.Windows.Controls.Ribbon.dll
    public static final CSharpEnum TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Controls_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpEnum getType() {
        CSharpEnum result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpEnum("KeyTipVerticalPlacement", declaringType);
        } else {
            result = new CSharpEnum("KeyTipVerticalPlacement", getNamespace());
        }
        result.setBaseType(Enum.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IComparable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IFormattable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IConvertible.TYPE);
        return result;
    }

}
