package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic_Compatibility;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_VisualBasic.Compatibility_Namespace;

public class VB6_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("VB6", Compatibility_Namespace.NAMESPACE);
    };


}
