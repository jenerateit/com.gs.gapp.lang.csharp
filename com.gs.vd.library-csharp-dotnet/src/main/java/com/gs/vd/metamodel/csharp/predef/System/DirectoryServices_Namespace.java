package com.gs.vd.metamodel.csharp.predef.System;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Namespace;

public class DirectoryServices_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("DirectoryServices", System_Namespace.NAMESPACE);
    };


}
