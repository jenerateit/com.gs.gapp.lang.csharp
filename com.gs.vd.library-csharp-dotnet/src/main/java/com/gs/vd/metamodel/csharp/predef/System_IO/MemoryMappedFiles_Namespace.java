package com.gs.vd.metamodel.csharp.predef.System_IO;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.IO_Namespace;

public class MemoryMappedFiles_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("MemoryMappedFiles", IO_Namespace.NAMESPACE);
    };


}
