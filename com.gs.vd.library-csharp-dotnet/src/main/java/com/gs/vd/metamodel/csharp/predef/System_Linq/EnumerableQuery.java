package com.gs.vd.metamodel.csharp.predef.System_Linq;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System.Linq_Namespace;

public class EnumerableQuery {
    // full name: System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Core\v4.0_4.0.0.0__b77a5c561934e089\System.Core.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Linq_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("EnumerableQuery", declaringType);
        } else {
            result = new CSharpClass("EnumerableQuery", getNamespace());
        }
        result.setBaseType(EnumerableQuery.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Linq.IQueryable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Linq.IOrderedQueryable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Linq.IQueryProvider.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("T", result, null, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
