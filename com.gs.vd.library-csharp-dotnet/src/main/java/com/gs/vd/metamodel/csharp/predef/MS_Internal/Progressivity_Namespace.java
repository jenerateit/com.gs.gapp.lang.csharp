package com.gs.vd.metamodel.csharp.predef.MS_Internal;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.MS.Internal_Namespace;

public class Progressivity_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Progressivity", Internal_Namespace.NAMESPACE);
    };


}
