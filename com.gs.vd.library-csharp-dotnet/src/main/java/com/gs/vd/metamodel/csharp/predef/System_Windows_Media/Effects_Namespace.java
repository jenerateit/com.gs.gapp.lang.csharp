package com.gs.vd.metamodel.csharp.predef.System_Windows_Media;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Media_Namespace;

public class Effects_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Effects", Media_Namespace.NAMESPACE);
    };


}
