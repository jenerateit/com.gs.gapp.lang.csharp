package com.gs.vd.metamodel.csharp.predef.System_Data_Services_Internal;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_Data_Services.Internal_Namespace;

public class ExpandedWrapper {
    // full name: System.Data.Services, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Data.Services\v4.0_4.0.0.0__b77a5c561934e089\System.Data.Services.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Internal_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ExpandedWrapper", declaringType);
        } else {
            result = new CSharpClass("ExpandedWrapper", getNamespace());
        }
        // type has no base type
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Data_Services.IExpandedResult.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("TExpandedElement", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty0", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty1", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty2", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty3", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty4", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty5", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty6", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty7", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty8", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty9", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty10", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TProperty11", result, null, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
