package com.gs.vd.metamodel.csharp.predef.System_Web_Configuration;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.Configuration_Namespace;

public class Internal_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Internal", Configuration_Namespace.NAMESPACE);
    };


}
