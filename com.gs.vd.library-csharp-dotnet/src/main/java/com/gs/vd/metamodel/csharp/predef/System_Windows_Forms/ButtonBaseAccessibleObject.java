package com.gs.vd.metamodel.csharp.predef.System_Windows_Forms;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Forms_Namespace;

public class ButtonBaseAccessibleObject {
    // full name: System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Windows.Forms\v4.0_4.0.0.0__b77a5c561934e089\System.Windows.Forms.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Forms_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return ButtonBase.TYPE;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ButtonBaseAccessibleObject", declaringType);
        } else {
            result = new CSharpClass("ButtonBaseAccessibleObject", getNamespace());
        }
        result.setBaseType(ControlAccessibleObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Win32.IMarshal.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Reflection.IReflect.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Accessibility.IAccessible.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IEnumVariant.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleWindow.TYPE);
        return result;
    }

}
