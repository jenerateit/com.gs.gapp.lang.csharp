package com.gs.vd.metamodel.csharp.predef.System_Data;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Data_Namespace;

public class Design_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Design", Data_Namespace.NAMESPACE);
    };


}
