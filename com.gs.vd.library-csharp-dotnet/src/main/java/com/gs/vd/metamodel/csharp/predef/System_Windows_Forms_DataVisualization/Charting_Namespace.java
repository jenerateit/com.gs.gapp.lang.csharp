package com.gs.vd.metamodel.csharp.predef.System_Windows_Forms_DataVisualization;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.DataVisualization_Namespace;

public class Charting_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Charting", DataVisualization_Namespace.NAMESPACE);
    };


}
