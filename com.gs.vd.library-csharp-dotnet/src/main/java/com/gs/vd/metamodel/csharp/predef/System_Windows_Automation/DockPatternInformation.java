package com.gs.vd.metamodel.csharp.predef.System_Windows_Automation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Automation_Namespace;

public class DockPatternInformation {
    // full name: UIAutomationClient, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\UIAutomationClient\v4.0_4.0.0.0__31bf3856ad364e35\UIAutomationClient.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Automation_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return DockPattern.TYPE;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("DockPatternInformation", declaringType);
        } else {
            result = new CSharpStruct("DockPatternInformation", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        return result;
    }

}
