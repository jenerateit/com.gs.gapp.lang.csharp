package com.gs.vd.metamodel.csharp.predef.System_Web_UI_Design;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI.Design_Namespace;

public class MobileControls_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("MobileControls", Design_Namespace.NAMESPACE);
    };


}
