package com.gs.vd.metamodel.csharp.predef.System_Windows_Controls_Ribbon;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Controls.Ribbon_Namespace;

public class Primitives_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Primitives", Ribbon_Namespace.NAMESPACE);
    };


}
