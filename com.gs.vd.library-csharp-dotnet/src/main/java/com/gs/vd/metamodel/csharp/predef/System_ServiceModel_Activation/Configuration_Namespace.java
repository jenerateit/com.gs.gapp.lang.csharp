package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Activation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Activation_Namespace;

public class Configuration_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Configuration", Activation_Namespace.NAMESPACE);
    };


}
