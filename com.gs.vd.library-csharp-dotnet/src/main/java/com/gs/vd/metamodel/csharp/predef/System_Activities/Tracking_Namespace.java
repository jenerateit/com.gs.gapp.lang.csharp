package com.gs.vd.metamodel.csharp.predef.System_Activities;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Activities_Namespace;

public class Tracking_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Tracking", Activities_Namespace.NAMESPACE);
    };


}
