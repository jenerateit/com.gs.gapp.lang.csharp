package com.gs.vd.metamodel.csharp.predef.System_Data_Services_BuildProvider;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Data_Services.BuildProvider_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Web_Compilation.BuildProvider;

public class DataServiceBuildProvider {
    // full name: System.Data.Services.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Data.Services.Design\v4.0_4.0.0.0__b77a5c561934e089\System.Data.Services.Design.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return BuildProvider_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DataServiceBuildProvider", declaringType);
        } else {
            result = new CSharpClass("DataServiceBuildProvider", getNamespace());
        }
        result.setBaseType(BuildProvider.TYPE);
        return result;
    }

}
