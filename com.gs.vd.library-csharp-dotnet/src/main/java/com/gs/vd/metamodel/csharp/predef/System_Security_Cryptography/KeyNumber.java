package com.gs.vd.metamodel.csharp.predef.System_Security_Cryptography;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Enum;
import com.gs.vd.metamodel.csharp.predef.System_Security.Cryptography_Namespace;

public class KeyNumber {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpEnum TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Cryptography_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpEnum getType() {
        CSharpEnum result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpEnum("KeyNumber", declaringType);
        } else {
            result = new CSharpEnum("KeyNumber", getNamespace());
        }
        result.setBaseType(Enum.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IComparable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IFormattable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IConvertible.TYPE);
        return result;
    }

}
