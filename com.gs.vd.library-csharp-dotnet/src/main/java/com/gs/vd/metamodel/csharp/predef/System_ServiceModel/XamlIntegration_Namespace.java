package com.gs.vd.metamodel.csharp.predef.System_ServiceModel;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.ServiceModel_Namespace;

public class XamlIntegration_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("XamlIntegration", ServiceModel_Namespace.NAMESPACE);
    };


}
