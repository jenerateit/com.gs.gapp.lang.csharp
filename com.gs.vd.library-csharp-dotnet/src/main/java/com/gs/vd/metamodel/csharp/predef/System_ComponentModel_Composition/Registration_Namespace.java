package com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Composition;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.Composition_Namespace;

public class Registration_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Registration", Composition_Namespace.NAMESPACE);
    };


}
