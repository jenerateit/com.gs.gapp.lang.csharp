package com.gs.vd.metamodel.csharp.predef.System_Workflow_Activities;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Workflow.Activities_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Workflow_ComponentModel.Activity;

public class HandleExternalEventActivity {
    // full name: System.Workflow.Activities, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Workflow.Activities\v4.0_4.0.0.0__31bf3856ad364e35\System.Workflow.Activities.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Activities_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("HandleExternalEventActivity", declaringType);
        } else {
            result = new CSharpClass("HandleExternalEventActivity", getNamespace());
        }
        result.setBaseType(Activity.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Workflow_ComponentModel.IDependencyObjectAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Workflow_Activities.IEventActivity.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Workflow_Activities_Common.IPropertyValueProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Workflow_ComponentModel.IDynamicPropertyTypeProvider.TYPE);
        return result;
    }

}
