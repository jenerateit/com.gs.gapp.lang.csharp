package com.gs.vd.metamodel.csharp.predef;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;

public class XamlGeneratedNamespace_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("XamlGeneratedNamespace", null);
    };


}
