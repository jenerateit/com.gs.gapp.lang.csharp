package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Framework;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build.Framework_Namespace;

public class XamlTypes_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("XamlTypes", Framework_Namespace.NAMESPACE);
    };


}
