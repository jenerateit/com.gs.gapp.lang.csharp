package com.gs.vd.metamodel.csharp.predef.System_Data_Common;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.MarshalByRefObject;
import com.gs.vd.metamodel.csharp.predef.System_Data.Common_Namespace;

public class DataColumnMapping {
    // full name: System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Data\v4.0_4.0.0.0__b77a5c561934e089\System.Data.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Common_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DataColumnMapping", declaringType);
        } else {
            result = new CSharpClass("DataColumnMapping", getNamespace());
        }
        result.setBaseType(MarshalByRefObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Data.IColumnMapping.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.ICloneable.TYPE);
        return result;
    }

}
