package com.gs.vd.metamodel.csharp.predef.System_Activities_Core_Presentation_Themes;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Activities_Core_Presentation.Themes_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.ResourceDictionary;

public class DesignerStylesDictionary {
    // full name: System.Activities.Core.Presentation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Activities.Core.Presentation\v4.0_4.0.0.0__31bf3856ad364e35\System.Activities.Core.Presentation.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Themes_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DesignerStylesDictionary", declaringType);
        } else {
            result = new CSharpClass("DesignerStylesDictionary", getNamespace());
        }
        result.setBaseType(ResourceDictionary.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IDictionary.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.ICollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ISupportInitialize.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IUriContext.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.INameScope.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IComponentConnector.TYPE);
        return result;
    }

}
