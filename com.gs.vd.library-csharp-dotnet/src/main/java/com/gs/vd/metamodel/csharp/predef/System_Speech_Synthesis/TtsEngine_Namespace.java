package com.gs.vd.metamodel.csharp.predef.System_Speech_Synthesis;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Speech.Synthesis_Namespace;

public class TtsEngine_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("TtsEngine", Synthesis_Namespace.NAMESPACE);
    };


}
