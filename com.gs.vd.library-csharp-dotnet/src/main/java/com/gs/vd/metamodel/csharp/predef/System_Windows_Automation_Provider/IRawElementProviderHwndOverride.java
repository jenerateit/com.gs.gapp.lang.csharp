package com.gs.vd.metamodel.csharp.predef.System_Windows_Automation_Provider;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Automation.Provider_Namespace;

public class IRawElementProviderHwndOverride {
    // full name: UIAutomationProvider, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\UIAutomationProvider\v4.0_4.0.0.0__31bf3856ad364e35\UIAutomationProvider.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Provider_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IRawElementProviderHwndOverride", declaringType);
        } else {
            result = new CSharpInterface("IRawElementProviderHwndOverride", getNamespace());
        }
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Automation_Provider.IRawElementProviderSimple.TYPE);
        return result;
    }

}
