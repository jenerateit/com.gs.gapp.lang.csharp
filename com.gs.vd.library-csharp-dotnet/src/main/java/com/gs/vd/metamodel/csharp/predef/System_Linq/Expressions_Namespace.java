package com.gs.vd.metamodel.csharp.predef.System_Linq;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Linq_Namespace;

public class Expressions_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Expressions", Linq_Namespace.NAMESPACE);
    };


}
