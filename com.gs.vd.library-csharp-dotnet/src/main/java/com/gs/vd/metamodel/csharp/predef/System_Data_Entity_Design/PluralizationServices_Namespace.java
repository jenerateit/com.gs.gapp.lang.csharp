package com.gs.vd.metamodel.csharp.predef.System_Data_Entity_Design;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data_Entity.Design_Namespace;

public class PluralizationServices_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("PluralizationServices", Design_Namespace.NAMESPACE);
    };


}
