package com.gs.vd.metamodel.csharp.predef.System_Web_UI_Design_MobileControls;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI_Design.MobileControls_Namespace;

public class Converters_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Converters", MobileControls_Namespace.NAMESPACE);
    };


}
