package com.gs.vd.metamodel.csharp.predef.System_Windows_Shell;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Shell_Namespace;

public class ThumbButtonInfoCollection {
    // full name: PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\PresentationFramework\v4.0_4.0.0.0__31bf3856ad364e35\PresentationFramework.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Shell_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ThumbButtonInfoCollection", declaringType);
        } else {
            result = new CSharpClass("ThumbButtonInfoCollection", getNamespace());
        }
        // type has no base type
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.ISealable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Animation.IAnimatable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Composition.IResource.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IList.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.ICollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections_Specialized.INotifyCollectionChanged.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.INotifyPropertyChanged.TYPE);
        return result;
    }

}
