package com.gs.vd.metamodel.csharp.predef.Microsoft_CSharp;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.CSharp_Namespace;

public class Activities_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Activities", CSharp_Namespace.NAMESPACE);
    };


}
