package com.gs.vd.metamodel.csharp.predef.System_ServiceModel;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.ServiceModel_Namespace;

public class Activities_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Activities", ServiceModel_Namespace.NAMESPACE);
    };


}
