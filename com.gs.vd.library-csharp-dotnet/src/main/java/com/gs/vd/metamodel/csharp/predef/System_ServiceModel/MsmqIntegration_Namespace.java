package com.gs.vd.metamodel.csharp.predef.System_ServiceModel;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.ServiceModel_Namespace;

public class MsmqIntegration_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("MsmqIntegration", ServiceModel_Namespace.NAMESPACE);
    };


}
