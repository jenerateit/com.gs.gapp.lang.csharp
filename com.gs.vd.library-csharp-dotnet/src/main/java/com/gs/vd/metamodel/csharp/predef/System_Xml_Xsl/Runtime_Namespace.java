package com.gs.vd.metamodel.csharp.predef.System_Xml_Xsl;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Xml.Xsl_Namespace;

public class Runtime_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Runtime", Xsl_Namespace.NAMESPACE);
    };


}
