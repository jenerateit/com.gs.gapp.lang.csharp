package com.gs.vd.metamodel.csharp.predef.System_Runtime;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Runtime_Namespace;

public class CompilerServices_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("CompilerServices", Runtime_Namespace.NAMESPACE);
    };


}
