package com.gs.vd.metamodel.csharp.predef.System_Data_Linq_SqlClient;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data_Linq.SqlClient_Namespace;

public class Implementation_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Implementation", SqlClient_Namespace.NAMESPACE);
    };


}
