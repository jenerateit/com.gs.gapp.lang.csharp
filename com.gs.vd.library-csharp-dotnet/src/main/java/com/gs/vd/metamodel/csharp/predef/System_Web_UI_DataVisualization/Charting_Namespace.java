package com.gs.vd.metamodel.csharp.predef.System_Web_UI_DataVisualization;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI.DataVisualization_Namespace;

public class Charting_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Charting", DataVisualization_Namespace.NAMESPACE);
    };


}
