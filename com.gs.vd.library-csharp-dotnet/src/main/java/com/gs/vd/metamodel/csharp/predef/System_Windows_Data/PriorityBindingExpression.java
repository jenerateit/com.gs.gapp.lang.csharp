package com.gs.vd.metamodel.csharp.predef.System_Windows_Data;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Data_Namespace;

public class PriorityBindingExpression {
    // full name: PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\PresentationFramework\v4.0_4.0.0.0__31bf3856ad364e35\PresentationFramework.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Data_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("PriorityBindingExpression", declaringType);
        } else {
            result = new CSharpClass("PriorityBindingExpression", getNamespace());
        }
        result.setBaseType(BindingExpressionBase.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IWeakEventListener.TYPE);
        return result;
    }

}
