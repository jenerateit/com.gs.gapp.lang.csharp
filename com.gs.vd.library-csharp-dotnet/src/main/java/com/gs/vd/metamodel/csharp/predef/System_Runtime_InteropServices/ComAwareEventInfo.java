package com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Reflection.EventInfo;
import com.gs.vd.metamodel.csharp.predef.System_Runtime.InteropServices_Namespace;

public class ComAwareEventInfo {
    // full name: System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Core\v4.0_4.0.0.0__b77a5c561934e089\System.Core.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return InteropServices_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ComAwareEventInfo", declaringType);
        } else {
            result = new CSharpClass("ComAwareEventInfo", getNamespace());
        }
        result.setBaseType(EventInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Reflection.ICustomAttributeProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._MemberInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._EventInfo.TYPE);
        return result;
    }

}
