package com.gs.vd.metamodel.csharp.predef.System_Web_Configuration;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Collections_Specialized.OrderedDictionary;
import com.gs.vd.metamodel.csharp.predef.System_Web.Configuration_Namespace;

public class AdapterDictionary {
    // full name: System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Web\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Web.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Configuration_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("AdapterDictionary", declaringType);
        } else {
            result = new CSharpClass("AdapterDictionary", getNamespace());
        }
        result.setBaseType(OrderedDictionary.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections_Specialized.IOrderedDictionary.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IDictionary.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.ICollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.ISerializable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.IDeserializationCallback.TYPE);
        return result;
    }

}
