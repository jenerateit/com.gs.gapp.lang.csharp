package com.gs.vd.metamodel.csharp.predef.System_Windows;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Windows_Namespace;

public class Ink_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Ink", Windows_Namespace.NAMESPACE);
    };


}
