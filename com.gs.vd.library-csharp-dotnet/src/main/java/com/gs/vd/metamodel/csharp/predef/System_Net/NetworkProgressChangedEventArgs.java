package com.gs.vd.metamodel.csharp.predef.System_Net;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Net_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ProgressChangedEventArgs;

public class NetworkProgressChangedEventArgs {
    // full name: System.Net, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Net\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Net.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Net_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("NetworkProgressChangedEventArgs", declaringType);
        } else {
            result = new CSharpClass("NetworkProgressChangedEventArgs", getNamespace());
        }
        result.setBaseType(ProgressChangedEventArgs.TYPE);
        return result;
    }

}
