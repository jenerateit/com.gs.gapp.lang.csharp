package com.gs.vd.metamodel.csharp.predef.System_Reflection_Context;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Reflection.Context_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Reflection.ReflectionContext;

public class CustomReflectionContext {
    // full name: System.Reflection.Context, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Reflection.Context\v4.0_4.0.0.0__b77a5c561934e089\System.Reflection.Context.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Context_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("CustomReflectionContext", declaringType);
        } else {
            result = new CSharpClass("CustomReflectionContext", getNamespace());
        }
        result.setBaseType(ReflectionContext.TYPE);
        return result;
    }

}
