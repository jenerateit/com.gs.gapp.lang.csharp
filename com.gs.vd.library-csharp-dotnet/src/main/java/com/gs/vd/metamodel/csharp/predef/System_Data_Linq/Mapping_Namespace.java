package com.gs.vd.metamodel.csharp.predef.System_Data_Linq;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Data.Linq_Namespace;

public class Mapping_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Mapping", Linq_Namespace.NAMESPACE);
    };


}
