package com.gs.vd.metamodel.csharp.predef.System_CodeDom;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.CodeDom_Namespace;

public class Compiler_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Compiler", CodeDom_Namespace.NAMESPACE);
    };


}
