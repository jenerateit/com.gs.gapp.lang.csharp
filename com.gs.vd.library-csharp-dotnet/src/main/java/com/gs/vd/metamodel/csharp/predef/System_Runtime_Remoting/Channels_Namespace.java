package com.gs.vd.metamodel.csharp.predef.System_Runtime_Remoting;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Runtime.Remoting_Namespace;

public class Channels_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Channels", Remoting_Namespace.NAMESPACE);
    };


}
