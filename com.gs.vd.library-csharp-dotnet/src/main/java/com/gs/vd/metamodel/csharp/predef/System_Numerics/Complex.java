package com.gs.vd.metamodel.csharp.predef.System_Numerics;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Numerics_Namespace;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;

public class Complex {
    // full name: System.Numerics, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Numerics\v4.0_4.0.0.0__b77a5c561934e089\System.Numerics.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Numerics_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("Complex", declaringType);
        } else {
            result = new CSharpStruct("Complex", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IFormattable.TYPE);
        return result;
    }

}
