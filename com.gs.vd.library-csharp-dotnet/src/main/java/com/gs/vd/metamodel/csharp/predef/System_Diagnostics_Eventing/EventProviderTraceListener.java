package com.gs.vd.metamodel.csharp.predef.System_Diagnostics_Eventing;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Diagnostics.Eventing_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Diagnostics.TraceListener;

public class EventProviderTraceListener {
    // full name: System.Core, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Core\v4.0_4.0.0.0__b77a5c561934e089\System.Core.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Eventing_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("EventProviderTraceListener", declaringType);
        } else {
            result = new CSharpClass("EventProviderTraceListener", getNamespace());
        }
        result.setBaseType(TraceListener.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        return result;
    }

}
