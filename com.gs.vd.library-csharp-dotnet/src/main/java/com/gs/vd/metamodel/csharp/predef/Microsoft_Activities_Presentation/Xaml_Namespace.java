package com.gs.vd.metamodel.csharp.predef.Microsoft_Activities_Presentation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Activities.Presentation_Namespace;

public class Xaml_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Xaml", Presentation_Namespace.NAMESPACE);
    };


}
