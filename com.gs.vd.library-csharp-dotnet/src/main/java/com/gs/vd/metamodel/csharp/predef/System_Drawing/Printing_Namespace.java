package com.gs.vd.metamodel.csharp.predef.System_Drawing;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Drawing_Namespace;

public class Printing_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Printing", Drawing_Namespace.NAMESPACE);
    };


}
