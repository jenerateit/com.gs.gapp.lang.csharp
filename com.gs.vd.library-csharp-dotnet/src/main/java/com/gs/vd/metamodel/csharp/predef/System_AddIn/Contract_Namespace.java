package com.gs.vd.metamodel.csharp.predef.System_AddIn;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.AddIn_Namespace;

public class Contract_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Contract", AddIn_Namespace.NAMESPACE);
    };


}
