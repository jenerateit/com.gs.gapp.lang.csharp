package com.gs.vd.metamodel.csharp.predef.System_Printing_IndexedProperties;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Printing.IndexedProperties_Namespace;

public class PrintBooleanProperty {
    // full name: System.Printing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Printing\v4.0_4.0.0.0__31bf3856ad364e35\System.Printing.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return IndexedProperties_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("PrintBooleanProperty", declaringType);
        } else {
            result = new CSharpClass("PrintBooleanProperty", getNamespace());
        }
        result.setBaseType(PrintProperty.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.IDeserializationCallback.TYPE);
        return result;
    }

}
