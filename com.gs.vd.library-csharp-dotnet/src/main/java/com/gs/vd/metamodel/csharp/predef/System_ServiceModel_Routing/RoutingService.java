package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Routing;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Object;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Routing_Namespace;

public class RoutingService {
    // full name: System.ServiceModel.Routing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceModel.Routing\v4.0_4.0.0.0__31bf3856ad364e35\System.ServiceModel.Routing.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Routing_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("RoutingService", declaringType);
        } else {
            result = new CSharpClass("RoutingService", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Routing.ISimplexDatagramRouter.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Routing.ISimplexSessionRouter.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Routing.IRequestReplyRouter.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Routing.IDuplexSessionRouter.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        return result;
    }

}
