package com.gs.vd.metamodel.csharp.predef.System_Diagnostics_Eventing;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Diagnostics.Eventing_Namespace;

public class Reader_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Reader", Eventing_Namespace.NAMESPACE);
    };


}
