package com.gs.vd.metamodel.csharp.predef.System_Security_Authentication_ExtendedProtection_Configuration;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Configuration.ConfigurationElement;
import com.gs.vd.metamodel.csharp.predef.System_Security_Authentication_ExtendedProtection.Configuration_Namespace;

public class ExtendedProtectionPolicyElement {
    // full name: System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System\v4.0_4.0.0.0__b77a5c561934e089\System.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Configuration_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ExtendedProtectionPolicyElement", declaringType);
        } else {
            result = new CSharpClass("ExtendedProtectionPolicyElement", getNamespace());
        }
        result.setBaseType(ConfigurationElement.TYPE);
        return result;
    }

}
