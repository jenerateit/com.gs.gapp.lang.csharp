package com.gs.vd.metamodel.csharp.predef.System_Security_Cryptography;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Security.Cryptography_Namespace;

public class X509Certificates_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("X509Certificates", Cryptography_Namespace.NAMESPACE);
    };


}
