package com.gs.vd.metamodel.csharp.predef.System_Web_Script;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.Script_Namespace;

public class Serialization_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Serialization", Script_Namespace.NAMESPACE);
    };


}
