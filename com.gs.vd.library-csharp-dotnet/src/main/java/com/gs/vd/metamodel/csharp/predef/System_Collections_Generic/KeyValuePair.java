package com.gs.vd.metamodel.csharp.predef.System_Collections_Generic;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;
import com.gs.vd.metamodel.csharp.predef.System_Collections.Generic_Namespace;

public class KeyValuePair {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Generic_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("KeyValuePair", declaringType);
        } else {
            result = new CSharpStruct("KeyValuePair", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("TKey", result, null, null);
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TValue", result, null, null);
        result.addGenericArguments(typeParameter);
        return result;
    }

}
