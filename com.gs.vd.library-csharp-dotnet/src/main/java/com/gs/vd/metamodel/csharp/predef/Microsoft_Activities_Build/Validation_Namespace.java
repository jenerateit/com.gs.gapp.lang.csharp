package com.gs.vd.metamodel.csharp.predef.Microsoft_Activities_Build;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Activities.Build_Namespace;

public class Validation_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Validation", Build_Namespace.NAMESPACE);
    };


}
