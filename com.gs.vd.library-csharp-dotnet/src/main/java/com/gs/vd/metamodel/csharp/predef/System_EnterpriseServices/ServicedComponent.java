package com.gs.vd.metamodel.csharp.predef.System_EnterpriseServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.ContextBoundObject;
import com.gs.vd.metamodel.csharp.predef.System.EnterpriseServices_Namespace;

public class ServicedComponent {
    // full name: System.EnterpriseServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.EnterpriseServices\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.EnterpriseServices.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return EnterpriseServices_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ServicedComponent", declaringType);
        } else {
            result = new CSharpClass("ServicedComponent", getNamespace());
        }
        result.setBaseType(ContextBoundObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_EnterpriseServices.IRemoteDispatch.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_EnterpriseServices.IManagedObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_EnterpriseServices.IServicedComponentInfo.TYPE);
        return result;
    }

}
