package com.gs.vd.metamodel.csharp.predef.System_Web_UI;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Web.UI_Namespace;

public class DataVisualization_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("DataVisualization", UI_Namespace.NAMESPACE);
    };


}
