package com.gs.vd.metamodel.csharp.predef.System_Web_UI_WebControls_WebParts;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI_WebControls.WebParts_Namespace;

public class WebPart {
    // full name: System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Web\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Web.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return WebParts_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("WebPart", declaringType);
        } else {
            result = new CSharpClass("WebPart", getNamespace());
        }
        result.setBaseType(Part.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IParserAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IUrlResolutionService.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IDataBindingsAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IControlBuilderAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IControlDesignerAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IExpressionsAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.IAttributeAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI.INamingContainer.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI_WebControls.ICompositeControlDesignerAccessor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI_WebControls_WebParts.IWebPart.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI_WebControls_WebParts.IWebActionable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_UI_WebControls_WebParts.IWebEditable.TYPE);
        return result;
    }

}
