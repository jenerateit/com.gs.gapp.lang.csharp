package com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices_ComTypes;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpStruct;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.ValueType;
import com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices.ComTypes_Namespace;

public class FORMATETC {
    // full name: System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System\v4.0_4.0.0.0__b77a5c561934e089\System.dll
    public static final CSharpStruct TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return ComTypes_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpStruct getType() {
        CSharpStruct result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpStruct("FORMATETC", declaringType);
        } else {
            result = new CSharpStruct("FORMATETC", getNamespace());
        }
        result.setBaseType(ValueType.TYPE);
        return result;
    }

}
