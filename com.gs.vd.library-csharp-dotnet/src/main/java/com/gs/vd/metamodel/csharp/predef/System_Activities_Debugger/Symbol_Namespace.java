package com.gs.vd.metamodel.csharp.predef.System_Activities_Debugger;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Activities.Debugger_Namespace;

public class Symbol_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Symbol", Debugger_Namespace.NAMESPACE);
    };


}
