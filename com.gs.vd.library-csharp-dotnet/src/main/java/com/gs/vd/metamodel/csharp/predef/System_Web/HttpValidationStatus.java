package com.gs.vd.metamodel.csharp.predef.System_Web;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Enum;
import com.gs.vd.metamodel.csharp.predef.System.Web_Namespace;

public class HttpValidationStatus {
    // full name: System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Web\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.Web.dll
    public static final CSharpEnum TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Web_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpEnum getType() {
        CSharpEnum result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpEnum("HttpValidationStatus", declaringType);
        } else {
            result = new CSharpEnum("HttpValidationStatus", getNamespace());
        }
        result.setBaseType(Enum.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IComparable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IFormattable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IConvertible.TYPE);
        return result;
    }

}
