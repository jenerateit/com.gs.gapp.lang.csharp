package com.gs.vd.metamodel.csharp.predef.System_Data_OracleClient;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Data.OracleClient_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Data_Common.RowUpdatingEventArgs;

public class OracleRowUpdatingEventArgs {
    // full name: System.Data.OracleClient, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Data.OracleClient\v4.0_4.0.0.0__b77a5c561934e089\System.Data.OracleClient.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return OracleClient_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("OracleRowUpdatingEventArgs", declaringType);
        } else {
            result = new CSharpClass("OracleRowUpdatingEventArgs", getNamespace());
        }
        result.setBaseType(RowUpdatingEventArgs.TYPE);
        return result;
    }

}
