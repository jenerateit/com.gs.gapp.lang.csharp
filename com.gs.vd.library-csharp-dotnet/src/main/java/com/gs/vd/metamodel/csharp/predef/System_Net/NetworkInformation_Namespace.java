package com.gs.vd.metamodel.csharp.predef.System_Net;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Net_Namespace;

public class NetworkInformation_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("NetworkInformation", Net_Namespace.NAMESPACE);
    };


}
