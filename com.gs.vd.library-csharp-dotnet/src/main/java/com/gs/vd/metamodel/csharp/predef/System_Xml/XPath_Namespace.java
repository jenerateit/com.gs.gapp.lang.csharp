package com.gs.vd.metamodel.csharp.predef.System_Xml;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Xml_Namespace;

public class XPath_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("XPath", Xml_Namespace.NAMESPACE);
    };


}
