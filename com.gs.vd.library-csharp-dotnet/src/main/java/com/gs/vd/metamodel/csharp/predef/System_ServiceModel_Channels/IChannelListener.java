package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels;

import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpGenericParameterAttributes;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Channels_Namespace;

public class IChannelListener {
    // full name: System.ServiceModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceModel\v4.0_4.0.0.0__b77a5c561934e089\System.ServiceModel.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Channels_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IChannelListener", declaringType);
        } else {
            result = new CSharpInterface("IChannelListener", getNamespace());
        }
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.IChannelListener.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel.ICommunicationObject.TYPE);
        CSharpTypeParameter typeParameter = null;
        Set<CSharpType> constraints = null;
        constraints = new LinkedHashSet<CSharpType>();
        constraints.add(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.IChannel.TYPE);

        typeParameter = new CSharpTypeParameter("TChannel", result, constraints, EnumSet.of(CSharpGenericParameterAttributes.REFERENCE_TYPE_CONSTRAINT));
        result.addGenericArguments(typeParameter);
        return result;
    }

}
