package com.gs.vd.metamodel.csharp.predef.System_IdentityModel_Selectors;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Exception;
import com.gs.vd.metamodel.csharp.predef.System_IdentityModel.Selectors_Namespace;

public class UntrustedRecipientException {
    // full name: System.IdentityModel.Selectors, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.IdentityModel.Selectors\v4.0_4.0.0.0__b77a5c561934e089\System.IdentityModel.Selectors.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Selectors_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("UntrustedRecipientException", declaringType);
        } else {
            result = new CSharpClass("UntrustedRecipientException", getNamespace());
        }
        result.setBaseType(Exception.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.ISerializable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_InteropServices._Exception.TYPE);
        return result;
    }

}
