package com.gs.vd.metamodel.csharp.predef.System_Web_DynamicData;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Web.DynamicData_Namespace;

public class MetaForeignKeyColumn {
    // full name: System.Web.DynamicData, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Web.DynamicData\v4.0_4.0.0.0__31bf3856ad364e35\System.Web.DynamicData.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return DynamicData_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("MetaForeignKeyColumn", declaringType);
        } else {
            result = new CSharpClass("MetaForeignKeyColumn", getNamespace());
        }
        result.setBaseType(MetaColumn.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_DynamicData.IFieldFormattingOptions.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_DynamicData.IMetaColumn.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Web_DynamicData.IMetaForeignKeyColumn.TYPE);
        return result;
    }

}
