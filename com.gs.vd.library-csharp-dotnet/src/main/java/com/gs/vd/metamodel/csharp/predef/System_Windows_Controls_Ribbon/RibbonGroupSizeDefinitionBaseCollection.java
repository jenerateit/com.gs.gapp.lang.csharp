package com.gs.vd.metamodel.csharp.predef.System_Windows_Controls_Ribbon;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Controls.Ribbon_Namespace;

public class RibbonGroupSizeDefinitionBaseCollection {
    // full name: System.Windows.Controls.Ribbon, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Windows.Controls.Ribbon\v4.0_4.0.0.0__b77a5c561934e089\System.Windows.Controls.Ribbon.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Ribbon_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("RibbonGroupSizeDefinitionBaseCollection", declaringType);
        } else {
            result = new CSharpClass("RibbonGroupSizeDefinitionBaseCollection", getNamespace());
        }
        // type has no base type
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.ISealable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Animation.IAnimatable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Composition.IResource.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IList.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.ICollection.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections.IEnumerable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Collections_Specialized.INotifyCollectionChanged.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.INotifyPropertyChanged.TYPE);
        return result;
    }

}
