package com.gs.vd.metamodel.csharp.predef.Microsoft_Build;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft.Build_Namespace;

public class Conversion_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Conversion", Build_Namespace.NAMESPACE);
    };


}
