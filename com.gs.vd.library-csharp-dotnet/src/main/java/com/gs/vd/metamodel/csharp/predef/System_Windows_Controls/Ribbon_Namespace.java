package com.gs.vd.metamodel.csharp.predef.System_Windows_Controls;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Controls_Namespace;

public class Ribbon_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Ribbon", Controls_Namespace.NAMESPACE);
    };


}
