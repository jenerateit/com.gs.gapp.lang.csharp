package com.gs.vd.metamodel.csharp.predef.System_Net;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Net_Namespace;

public class Mime_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Mime", Net_Namespace.NAMESPACE);
    };


}
