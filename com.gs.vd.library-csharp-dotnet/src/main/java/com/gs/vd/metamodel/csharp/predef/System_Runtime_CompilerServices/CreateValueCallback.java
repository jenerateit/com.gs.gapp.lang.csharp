package com.gs.vd.metamodel.csharp.predef.System_Runtime_CompilerServices;

import java.util.EnumSet;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpGenericParameterAttributes;
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;
import com.gs.vd.metamodel.csharp.predef.System.MulticastDelegate;
import com.gs.vd.metamodel.csharp.predef.System_Runtime.CompilerServices_Namespace;

public class CreateValueCallback {
    // full name: mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.NET\Framework\v4.0.30319\mscorlib.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return CompilerServices_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return ConditionalWeakTable.TYPE;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("CreateValueCallback", declaringType);
        } else {
            result = new CSharpClass("CreateValueCallback", getNamespace());
        }
        result.setBaseType(MulticastDelegate.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.ICloneable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Runtime_Serialization.ISerializable.TYPE);
        CSharpTypeParameter typeParameter = null;

        typeParameter = new CSharpTypeParameter("TKey", result, null, EnumSet.of(CSharpGenericParameterAttributes.REFERENCE_TYPE_CONSTRAINT));
        result.addGenericArguments(typeParameter);

        typeParameter = new CSharpTypeParameter("TValue", result, null, EnumSet.of(CSharpGenericParameterAttributes.REFERENCE_TYPE_CONSTRAINT));
        result.addGenericArguments(typeParameter);
        return result;
    }

}
