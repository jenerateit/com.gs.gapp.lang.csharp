package com.gs.vd.metamodel.csharp.predef.System_Data;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.Data_Namespace;

public class EntityClient_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("EntityClient", Data_Namespace.NAMESPACE);
    };


}
