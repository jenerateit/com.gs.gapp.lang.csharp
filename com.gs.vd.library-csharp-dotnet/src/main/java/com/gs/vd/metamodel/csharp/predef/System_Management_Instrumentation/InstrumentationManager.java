package com.gs.vd.metamodel.csharp.predef.System_Management_Instrumentation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Object;
import com.gs.vd.metamodel.csharp.predef.System_Management.Instrumentation_Namespace;

public class InstrumentationManager {
    // full name: System.Management.Instrumentation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Management.Instrumentation\v4.0_4.0.0.0__b77a5c561934e089\System.Management.Instrumentation.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Instrumentation_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("InstrumentationManager", declaringType);
        } else {
            result = new CSharpClass("InstrumentationManager", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        return result;
    }

}
