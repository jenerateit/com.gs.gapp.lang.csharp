package com.gs.vd.metamodel.csharp.predef.System_Windows_Forms;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Forms_Namespace;

public class DataGridViewComboBoxEditingControl {
    // full name: System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Windows.Forms\v4.0_4.0.0.0__b77a5c561934e089\System.Windows.Forms.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Forms_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DataGridViewComboBoxEditingControl", declaringType);
        } else {
            result = new CSharpClass("DataGridViewComboBoxEditingControl", getNamespace());
        }
        result.setBaseType(ComboBox.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.IDisposable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleControl.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleInPlaceObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleInPlaceActiveObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IOleWindow.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IViewObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IViewObject2.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IPersist.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IPersistStreamInit.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IPersistPropertyBag.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IPersistStorage.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IQuickActivate.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.ISupportOleDropSource.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IDropTarget.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ISynchronizeInvoke.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IWin32Window.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms_Layout.IArrangedElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IBindableComponent.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Forms.IDataGridViewEditingControl.TYPE);
        return result;
    }

}
