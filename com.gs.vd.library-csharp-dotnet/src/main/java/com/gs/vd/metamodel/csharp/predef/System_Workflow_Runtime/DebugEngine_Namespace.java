package com.gs.vd.metamodel.csharp.predef.System_Workflow_Runtime;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Workflow.Runtime_Namespace;

public class DebugEngine_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("DebugEngine", Runtime_Namespace.NAMESPACE);
    };


}
