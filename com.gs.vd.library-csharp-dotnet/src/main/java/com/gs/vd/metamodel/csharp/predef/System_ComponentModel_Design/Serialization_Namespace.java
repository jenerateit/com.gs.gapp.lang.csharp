package com.gs.vd.metamodel.csharp.predef.System_ComponentModel_Design;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ComponentModel.Design_Namespace;

public class Serialization_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Serialization", Design_Namespace.NAMESPACE);
    };


}
