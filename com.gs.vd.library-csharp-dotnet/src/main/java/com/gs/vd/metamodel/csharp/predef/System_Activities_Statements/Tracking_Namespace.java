package com.gs.vd.metamodel.csharp.predef.System_Activities_Statements;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Activities.Statements_Namespace;

public class Tracking_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Tracking", Statements_Namespace.NAMESPACE);
    };


}
