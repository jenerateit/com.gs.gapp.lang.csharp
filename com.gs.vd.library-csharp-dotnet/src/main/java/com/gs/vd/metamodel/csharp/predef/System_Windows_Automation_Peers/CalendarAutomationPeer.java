package com.gs.vd.metamodel.csharp.predef.System_Windows_Automation_Peers;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Automation.Peers_Namespace;

public class CalendarAutomationPeer {
    // full name: PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\PresentationFramework\v4.0_4.0.0.0__31bf3856ad364e35\PresentationFramework.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Peers_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("CalendarAutomationPeer", declaringType);
        } else {
            result = new CSharpClass("CalendarAutomationPeer", getNamespace());
        }
        result.setBaseType(FrameworkElementAutomationPeer.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Automation_Provider.IMultipleViewProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Automation_Provider.ISelectionProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Automation_Provider.ITableProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Automation_Provider.IGridProvider.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Automation_Provider.IItemContainerProvider.TYPE);
        return result;
    }

}
