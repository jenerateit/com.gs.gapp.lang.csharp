package com.gs.vd.metamodel.csharp.predef.System_Speech_Internal;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Speech.Internal_Namespace;

public class SrgsParser_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("SrgsParser", Internal_Namespace.NAMESPACE);
    };


}
