package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Activities_Presentation;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Activities.Presentation_Namespace;

public class Factories_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Factories", Presentation_Namespace.NAMESPACE);
    };


}
