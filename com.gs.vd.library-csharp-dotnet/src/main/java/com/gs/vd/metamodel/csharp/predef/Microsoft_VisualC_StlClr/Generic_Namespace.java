package com.gs.vd.metamodel.csharp.predef.Microsoft_VisualC_StlClr;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_VisualC.StlClr_Namespace;

public class Generic_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Generic", StlClr_Namespace.NAMESPACE);
    };


}
