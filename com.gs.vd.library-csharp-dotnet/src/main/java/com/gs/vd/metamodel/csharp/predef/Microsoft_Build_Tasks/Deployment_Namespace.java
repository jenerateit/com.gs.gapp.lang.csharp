package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build.Tasks_Namespace;

public class Deployment_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Deployment", Tasks_Namespace.NAMESPACE);
    };


}
