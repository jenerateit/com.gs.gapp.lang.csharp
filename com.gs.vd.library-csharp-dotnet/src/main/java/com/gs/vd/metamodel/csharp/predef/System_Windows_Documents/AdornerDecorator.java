package com.gs.vd.metamodel.csharp.predef.System_Windows_Documents;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Documents_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Controls.Decorator;

public class AdornerDecorator {
    // full name: PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\PresentationFramework\v4.0_4.0.0.0__31bf3856ad364e35\PresentationFramework.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Documents_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("AdornerDecorator", declaringType);
        } else {
            result = new CSharpClass("AdornerDecorator", getNamespace());
        }
        result.setBaseType(Decorator.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Composition.IResource.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Media_Animation.IAnimatable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IInputElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows.IFrameworkInputElement.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ISupportInitialize.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IHaveResources.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IQueryAmbient.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.IAddChild.TYPE);
        return result;
    }

}
