package com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_ServiceModel.Channels_Namespace;

public class IDuplexSessionChannel {
    // full name: System.ServiceModel, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceModel\v4.0_4.0.0.0__b77a5c561934e089\System.ServiceModel.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Channels_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IDuplexSessionChannel", declaringType);
        } else {
            result = new CSharpInterface("IDuplexSessionChannel", getNamespace());
        }
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.IDuplexChannel.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.IInputChannel.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.IChannel.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel.ICommunicationObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ServiceModel_Channels.IOutputChannel.TYPE);
        return result;
    }

}
