package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks_Windows;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Tasks.Windows_Namespace;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Utilities.Task;

public class ResourcesGenerator {
    // full name: PresentationBuildTasks, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\PresentationBuildTasks\v4.0_4.0.0.0__31bf3856ad364e35\PresentationBuildTasks.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Windows_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ResourcesGenerator", declaringType);
        } else {
            result = new CSharpClass("ResourcesGenerator", getNamespace());
        }
        result.setBaseType(Task.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Framework.ITask.TYPE);
        return result;
    }

}
