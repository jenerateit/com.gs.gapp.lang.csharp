package com.gs.vd.metamodel.csharp.predef.System_ServiceProcess;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.ServiceProcess_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Security_Permissions.ResourcePermissionBase;

public class ServiceControllerPermission {
    // full name: System.ServiceProcess, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.ServiceProcess\v4.0_4.0.0.0__b03f5f7f11d50a3a\System.ServiceProcess.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return ServiceProcess_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("ServiceControllerPermission", declaringType);
        } else {
            result = new CSharpClass("ServiceControllerPermission", getNamespace());
        }
        result.setBaseType(ResourcePermissionBase.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.IPermission.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.ISecurityEncodable.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security.IStackWalk.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Security_Permissions.IUnrestrictedPermission.TYPE);
        return result;
    }

}
