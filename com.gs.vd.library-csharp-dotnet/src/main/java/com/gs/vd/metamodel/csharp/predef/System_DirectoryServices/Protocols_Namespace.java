package com.gs.vd.metamodel.csharp.predef.System_DirectoryServices;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System.DirectoryServices_Namespace;

public class Protocols_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Protocols", DirectoryServices_Namespace.NAMESPACE);
    };


}
