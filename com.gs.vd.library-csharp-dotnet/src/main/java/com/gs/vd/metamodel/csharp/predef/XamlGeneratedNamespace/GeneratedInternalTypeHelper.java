package com.gs.vd.metamodel.csharp.predef.XamlGeneratedNamespace;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.XamlGeneratedNamespace_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows_Markup.InternalTypeHelper;

public class GeneratedInternalTypeHelper {
    // full name: System.Activities.Presentation, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Activities.Presentation\v4.0_4.0.0.0__31bf3856ad364e35\System.Activities.Presentation.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return XamlGeneratedNamespace_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("GeneratedInternalTypeHelper", declaringType);
        } else {
            result = new CSharpClass("GeneratedInternalTypeHelper", getNamespace());
        }
        result.setBaseType(InternalTypeHelper.TYPE);
        return result;
    }

}
