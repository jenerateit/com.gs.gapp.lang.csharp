package com.gs.vd.metamodel.csharp.predef.System_Web_DynamicData_Design;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Web_DynamicData.Design_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Web_UI_Design_WebControls.DataControlFieldDesigner;

public class DynamicFieldDesigner {
    // full name: System.Web.DynamicData.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\System.Web.DynamicData.Design\v4.0_4.0.0.0__31bf3856ad364e35\System.Web.DynamicData.Design.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Design_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DynamicFieldDesigner", declaringType);
        } else {
            result = new CSharpClass("DynamicFieldDesigner", getNamespace());
        }
        result.setBaseType(DataControlFieldDesigner.TYPE);
        return result;
    }

}
