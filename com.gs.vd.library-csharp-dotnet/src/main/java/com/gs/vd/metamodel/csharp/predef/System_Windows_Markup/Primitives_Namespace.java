package com.gs.vd.metamodel.csharp.predef.System_Windows_Markup;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Windows.Markup_Namespace;

public class Primitives_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Primitives", Markup_Namespace.NAMESPACE);
    };


}
