package com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Framework;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpInterface;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.Microsoft_Build.Framework_Namespace;

public class IBuildEngine3 {
    // full name: Microsoft.Build.Framework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a
    // location: C:\Windows\Microsoft.Net\assembly\GAC_MSIL\Microsoft.Build.Framework\v4.0_4.0.0.0__b03f5f7f11d50a3a\Microsoft.Build.Framework.dll
    public static final CSharpInterface TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Framework_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpInterface getType() {
        CSharpInterface result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpInterface("IBuildEngine3", declaringType);
        } else {
            result = new CSharpInterface("IBuildEngine3", getNamespace());
        }
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Framework.IBuildEngine2.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.Microsoft_Build_Framework.IBuildEngine.TYPE);
        return result;
    }

}
