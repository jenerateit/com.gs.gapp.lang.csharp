package com.gs.vd.metamodel.csharp.predef.System_Data;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Data_Namespace;
import com.gs.vd.metamodel.csharp.predef.System.Object;

public class DataRowView {
    // full name: System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Data\v4.0_4.0.0.0__b77a5c561934e089\System.Data.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return Data_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("DataRowView", declaringType);
        } else {
            result = new CSharpClass("DataRowView", getNamespace());
        }
        result.setBaseType(Object.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.ICustomTypeDescriptor.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IEditableObject.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.IDataErrorInfo.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_ComponentModel.INotifyPropertyChanged.TYPE);
        return result;
    }

}
