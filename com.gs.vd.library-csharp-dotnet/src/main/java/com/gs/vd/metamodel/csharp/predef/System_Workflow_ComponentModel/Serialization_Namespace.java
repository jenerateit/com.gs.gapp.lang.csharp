package com.gs.vd.metamodel.csharp.predef.System_Workflow_ComponentModel;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.vd.metamodel.csharp.predef.System_Workflow.ComponentModel_Namespace;

public class Serialization_Namespace {

    public static final CSharpNamespace NAMESPACE;

    static {
        NAMESPACE = getNamespace();
    }

    private static final CSharpNamespace getNamespace() {
        return new CSharpNamespace("Serialization", ComponentModel_Namespace.NAMESPACE);
    };


}
