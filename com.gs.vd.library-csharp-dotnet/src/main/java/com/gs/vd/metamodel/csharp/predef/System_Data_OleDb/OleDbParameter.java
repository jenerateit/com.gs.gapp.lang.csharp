package com.gs.vd.metamodel.csharp.predef.System_Data_OleDb;

import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System_Data.OleDb_Namespace;
import com.gs.vd.metamodel.csharp.predef.System_Data_Common.DbParameter;

public class OleDbParameter {
    // full name: System.Data, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089
    // location: C:\Windows\Microsoft.Net\assembly\GAC_32\System.Data\v4.0_4.0.0.0__b77a5c561934e089\System.Data.dll
    public static final CSharpClass TYPE;

    static {
        TYPE = getType();
        TYPE.setGenerated(false);  // predefined elements must never be generated
    }


    private static final CSharpNamespace getNamespace() {
        return OleDb_Namespace.NAMESPACE;
    }


    private static final CSharpType getDeclaringType() {
        return null;
    }


    private static final CSharpClass getType() {
        CSharpClass result = null;
        CSharpType declaringType = getDeclaringType();
        if (declaringType != null) {
            result = new CSharpClass("OleDbParameter", declaringType);
        } else {
            result = new CSharpClass("OleDbParameter", getNamespace());
        }
        result.setBaseType(DbParameter.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Data.IDbDataParameter.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System_Data.IDataParameter.TYPE);
        result.addInterfaces(com.gs.vd.metamodel.csharp.predef.System.ICloneable.TYPE);
        return result;
    }

}
