package com.gs.vd.csharp.test;

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.csharp.*
import com.gs.gapp.metamodel.csharp.member.CSharpField
import com.gs.gapp.metamodel.csharp.member.CSharpMethod
import com.gs.gapp.metamodel.csharp.type.CSharpType
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter
import com.gs.vd.metamodel.csharp.predef.System.String

public class TestModelGenerics extends AbstractTestModel {

	private static final java.lang.String TEST_POSTFIX = "generics";
	private static final java.lang.String TEST_NESTED_POSTFIX = "nested";

	private final CSharpNamespace NS = getNamespace(TEST_POSTFIX);
	private final CSharpNamespace NS_NESTED = new CSharpNamespace(TEST_NESTED_POSTFIX, NS);

	/**
	 * @return
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		// --- 1. cs-file
		CSharpSourceFile source = CREATOR.createClassSourceFile("ClassWithGenericMethod", NS);
		this.addGenericMethods(source.getFirstNamespaceUsage().getFirstType());
		result.add( source );

		// --- 2. generic parameter on the class
		CSharpType type = source.getFirstNamespaceUsage().getFirstType();
		CSharpTypeParameter typeParameter = new CSharpTypeParameter("TClass", type);
        type.addGenericArguments(typeParameter);

		// --- 3 a method that uses the generic parameter of the class
		CSharpMethod method = CREATOR.createMethod(typeParameter, EnumSet.of(CSharpModifier.PUBLIC), "genericMethod2", type, new CSharpModelElementCreator.ParamInfo(typeParameter, "param"));

		// --- 4 a field whose type is the field's owner type
		CSharpType parameterizedOwnerType = CREATOR.makeGenericType(type, com.gs.vd.metamodel.csharp.predef.System.String.TYPE);
		CSharpField field = new CSharpField("fieldWithOwnersType", parameterizedOwnerType, type);

		return result;
	}
}