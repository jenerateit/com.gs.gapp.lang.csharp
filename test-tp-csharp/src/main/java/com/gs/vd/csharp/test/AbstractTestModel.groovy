package com.gs.vd.csharp.test;

import java.util.Collection;
import java.util.Set;

import javax.lang.model.element.TypeParameterElement;

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.csharp.*
import com.gs.gapp.metamodel.csharp.CSharpModelElementCreator.ParamInfo;
import com.gs.gapp.metamodel.csharp.member.CSharpField
import com.gs.gapp.metamodel.csharp.member.CSharpMethod
import com.gs.gapp.metamodel.csharp.member.CSharpProperty
import com.gs.gapp.metamodel.csharp.type.CSharpType
import com.gs.gapp.metamodel.csharp.type.generics.CSharpTypeParameter;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterI
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.vd.metamodel.csharp.predef.System.Single;
import com.gs.vd.metamodel.csharp.predef.System.Double;
import com.gs.vd.metamodel.csharp.predef.System.Int16;
import com.gs.vd.metamodel.csharp.predef.System.Int32;
import com.gs.vd.metamodel.csharp.predef.System.Int64;
import com.gs.vd.metamodel.csharp.predef.System.String;
import com.gs.vd.metamodel.csharp.predef.System_Collections_Generic.List;

public class AbstractTestModel implements ModelConverterI {

	private final static java.lang.String NS_AS_STRING = "com.gs.vd.csharp.test";

	protected final static CSharpModelElementCreator CREATOR = new CSharpModelElementCreator();

	public AbstractTestModel() {}

	/**
	 * @return
	 */
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = new LinkedHashSet<ModelElementI>();
		return result;
	}

	/**
	 * @param postfix
	 * @return
	 */
	private final java.lang.String getNamespaceAsString(java.lang.String postfix) {
		return NS_AS_STRING + (postfix != null && postfix.length() > 0 ? "."+postfix : "");
	}

	/**
	 * @param postfix
	 * @return
	 */
	protected CSharpNamespace getNamespace(java.lang.String postfix) {
		CSharpModelElementCreator.convertToNamespaceObject(getNamespaceAsString(postfix));
	}


    /* (non-Javadoc)
     * @see org.jenerateit.modelconverter.ModelConverterI#convert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
     */
    @Override
	public Set<Object> convert(Collection<?> elements, ModelConverterOptions options)
			throws ModelConverterException {
        Set<Object> result = new LinkedHashSet<Object>(onConvert());
		return result;
	}

	/**
     * @param type
     */
    protected void addSampleFields(CSharpType type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		CSharpField field = new CSharpField("stringField1", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("stringField2", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("stringField3", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));

		field = new CSharpField("int16Field1", com.gs.vd.metamodel.csharp.predef.System.Int16.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("int16Field2", com.gs.vd.metamodel.csharp.predef.System.Int16.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("int16Field3", com.gs.vd.metamodel.csharp.predef.System.Int16.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));

		field = new CSharpField("int32Field1", com.gs.vd.metamodel.csharp.predef.System.Int32.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("int32Field2", com.gs.vd.metamodel.csharp.predef.System.Int32.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("int32Field3", com.gs.vd.metamodel.csharp.predef.System.Int32.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));

		field = new CSharpField("int64Field1", com.gs.vd.metamodel.csharp.predef.System.Int64.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("int64Field2", com.gs.vd.metamodel.csharp.predef.System.Int64.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("int64Field3", com.gs.vd.metamodel.csharp.predef.System.Int64.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));

		field = new CSharpField("singleField1", com.gs.vd.metamodel.csharp.predef.System.Single.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("singleField2", com.gs.vd.metamodel.csharp.predef.System.Single.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("singleField3", com.gs.vd.metamodel.csharp.predef.System.Single.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));

		field = new CSharpField("doubleField1", com.gs.vd.metamodel.csharp.predef.System.Double.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("doubleField2", com.gs.vd.metamodel.csharp.predef.System.Double.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		field = new CSharpField("doubleField3", com.gs.vd.metamodel.csharp.predef.System.Double.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
	}

	/**
	 * @param type
	 */
	protected void addSampleMethods(CSharpType type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		// simple public methods returning a string
		CSharpMethod method = new CSharpMethod("publicMethod1", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		method.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		method = new CSharpMethod("publicMethod2", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		method.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		method = new CSharpMethod("publicMethod3", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		method.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		method.setMethodBody("String result = \"this value comes from a method converter and spans multiple lines\";\nreturn result;");

		// simple non-public methods returning void
		method = new CSharpMethod("method1", com.gs.vd.metamodel.csharp.predef.System.Void.TYPE, type);
		method.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		method = new CSharpMethod("method2", com.gs.vd.metamodel.csharp.predef.System.Void.TYPE, type);
		method.setModifiers(EnumSet.of(CSharpModifier.PROTECTED));
		method = new CSharpMethod("method3", com.gs.vd.metamodel.csharp.predef.System.Void.TYPE, type);
		method.setModifiers(EnumSet.of(CSharpModifier.PROTECTED_INTERNAL));

		// more complex methods
		method = CREATOR.createMethod(com.gs.vd.metamodel.csharp.predef.System.String.TYPE, EnumSet.of(CSharpModifier.PUBLIC), "methodWith3Params", type,
			new ParamInfo(com.gs.vd.metamodel.csharp.predef.System.String.TYPE, "param1", "doc for param1"),
			new ParamInfo(com.gs.vd.metamodel.csharp.predef.System.String.TYPE, "param2", "doc for param2"),
			new ParamInfo(com.gs.vd.metamodel.csharp.predef.System.String.TYPE, "param3", "doc for param3"));

	}

	/**
	 * @param type
	 */
	protected void addGenericMethods(CSharpType type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		// simple generic method, passing a generic type and returning a list of that type
		CSharpTypeParameter invariantTypeParameter = new CSharpTypeParameter("TMethod", type, null, null);
		CSharpType genericListType = CREATOR.makeGenericType(List.TYPE, invariantTypeParameter);

		CSharpTypeParameter[] typeParameterArray = new CSharpTypeParameter[1];
		typeParameterArray[0] = invariantTypeParameter;

		CSharpMethod method = CREATOR.createMethod(genericListType, EnumSet.of(CSharpModifier.PUBLIC), new HashSet<CSharpTypeParameter>(Arrays.asList(typeParameterArray)), "genericMethod", type, new CSharpModelElementCreator.ParamInfo(invariantTypeParameter, "param"));
		method.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
	}

	/**
	 * @param type
	 */
	protected void addSampleProperties(CSharpType type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		// --- automated properties of type string
		CSharpProperty property = new CSharpProperty("automatedProperty1", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		property.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		property.setAutoImplemented(true);
		property = new CSharpProperty("automatedProperty2", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		property.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		property.setAutoImplemented(true);
		property = new CSharpProperty("automatedProperty3", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		property.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		property.setAutoImplemented(true);

		// --- properties of type string
		property = new CSharpProperty("property1", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		property.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		property = new CSharpProperty("property2", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		property.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		property = new CSharpProperty("property3", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		property.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));

	}

	/**
	 * @param type
	 */
	protected void addSamplePropertiesWithFields(CSharpType type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		// --- properties with corresponding field
		CSharpField field = new CSharpField("fieldOfProperty1", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		CREATOR.convertToProperty(field);
		field = new CSharpField("fieldOfProperty2", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		CREATOR.convertToProperty(field);
		field = new CSharpField("fieldOfProperty3", com.gs.vd.metamodel.csharp.predef.System.String.TYPE, type);
		field.setModifiers(EnumSet.of(CSharpModifier.PRIVATE));
		CREATOR.convertToProperty(field);

	}

	/**
	 * @param type
	 */
	protected void addSampleMembers(CSharpType type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		this.addSampleFields(type);
		this.addSampleMethods(type);
		this.addSampleProperties(type);
		this.addSamplePropertiesWithFields(type);
	}

	@Override
	public java.util.List<java.lang.String> getErrors() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public java.util.List<java.lang.String> getInfos() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public java.util.List<java.lang.String> getWarnings() {
        return Collections.EMPTY_LIST;
	}
}
