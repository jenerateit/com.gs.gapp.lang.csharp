package com.gs.vd.csharp.test;

import java.util.Collection;

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.csharp.*
import com.gs.gapp.metamodel.csharp.type.CSharpClass;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterI
import org.jenerateit.modelconverter.ModelConverterOptions;

public class TestModelClasses extends AbstractTestModel {

	private static final String TEST_POSTFIX = "classes";
	private static final String TEST_NESTED_POSTFIX = "nested";

	private final CSharpNamespace NS = getNamespace(TEST_POSTFIX);
	private final CSharpNamespace NS_NESTED = new CSharpNamespace(TEST_NESTED_POSTFIX, NS);

	/**
	 * @return
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		// --- 1. cs-file
		CSharpSourceFile source = CREATOR.createClassSourceFile("TestClass1AndNestedClass", NS);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		result.add( source );
		CSharpClass csClass = new CSharpClass("NestedClass", source.getFirstNamespaceUsage().getFirstType());
		this.addSampleMembers(csClass);

		// --- 2. cs-file
		source = CREATOR.createClassSourceFile("TestClass2", NS);
		result.add( source );
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());

		// --- 3. cs-file
		source = CREATOR.createClassSourceFile("TestClass3InGlobalNamespace", null);
		result.add( source );
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());

		// --- 4. cs-file
		source = CREATOR.createClassSourceFile("TestClass4WithAdditionalClass", NS);
		csClass = new CSharpClass("AdditionalClass", source.getFirstNamespaceUsage());
		this.addSampleMembers(csClass);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		result.add( source );

		// --- 5. cs-file
		source = CREATOR.createClassSourceFile("TestClass5WithAdditionalClassWithNestedClass", NS);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		csClass = new CSharpClass("AdditionalClass", source.getFirstNamespaceUsage());
		this.addSampleMembers(csClass);
		csClass = new CSharpClass("NestedClass2", csClass);
		this.addSampleMembers(csClass);
		result.add( source );

		// --- 6. cs-file
		source = CREATOR.createClassSourceFile("TestClass6WithNestedNamespaceAndClass", NS);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		CSharpNamespaceUsage nestedNamespaceUsage = new CSharpNamespaceUsage(NS_NESTED, source.getFirstNamespaceUsage());
		csClass = new CSharpClass("ClassInNestedNamespace", nestedNamespaceUsage);
		this.addSampleMembers(csClass);
		csClass = new CSharpClass("NestedClass3", csClass);
		this.addSampleMembers(csClass);
		result.add( source );

		// --- 7. cs-file
		source = CREATOR.createClassSourceFile("TestClass7AndMultilevelNestingOfClasses", NS);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		result.add( source );
		csClass = new CSharpClass("NestedClass1", source.getFirstNamespaceUsage().getFirstType());
		this.addSampleMembers(csClass);
		csClass = new CSharpClass("NestedClass2", csClass);
		this.addSampleMembers(csClass);
		csClass = new CSharpClass("NestedClass3", csClass);
		this.addSampleMembers(csClass);
		csClass = new CSharpClass("NestedClass4", csClass);
		this.addSampleMembers(csClass);
		return result;
	}
}