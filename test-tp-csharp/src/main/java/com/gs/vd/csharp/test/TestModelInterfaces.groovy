package com.gs.vd.csharp.test;

import com.gs.gapp.metamodel.basic.ModelElementI
import com.gs.gapp.metamodel.csharp.*
import com.gs.gapp.metamodel.csharp.type.CSharpClass
import com.gs.gapp.metamodel.csharp.type.CSharpInterface

public class TestModelInterfaces extends AbstractTestModel {

	private static final String TEST_POSTFIX = "interfaces";
	private static final String TEST_NESTED_POSTFIX = "nested";

	private final CSharpNamespace NS = getNamespace(TEST_POSTFIX);
	private final CSharpNamespace NS_NESTED = new CSharpNamespace(TEST_NESTED_POSTFIX, NS);

	/**
	 * @return
	 */
	@Override
	protected Set<ModelElementI> onConvert() {
		Set<ModelElementI> result = super.onConvert();

		// --- 1. cs-file
		CSharpSourceFile source = CREATOR.createInterfaceSourceFile("TestInterface1AndNestedInterface", NS);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		result.add( source );
		CSharpClass csInterface = new CSharpClass("NestedInterface", source.getFirstNamespaceUsage().getFirstType());
		this.addSampleMembers(csInterface);

		// --- 2. cs-file
		source = CREATOR.createInterfaceSourceFile("TestInterface2", NS);
		result.add( source );
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());

		// --- 3. cs-file
		source = CREATOR.createInterfaceSourceFile("TestInterface3InGlobalNamespace", null);
		result.add( source );
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());

		// --- 4. cs-file
		source = CREATOR.createInterfaceSourceFile("TestInterface4WithAdditionalInterface", NS);
		csInterface = new CSharpClass("AdditionalInterface", source.getFirstNamespaceUsage());
		this.addSampleMembers(csInterface);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		result.add( source );

		// --- 5. cs-file
		source = CREATOR.createInterfaceSourceFile("TestInterface5WithAdditionalInterfaceWithNestedInterface", NS);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		csInterface = new CSharpClass("AdditionalInterface", source.getFirstNamespaceUsage());
		this.addSampleMembers(csInterface);
		csInterface = new CSharpClass("NestedInterface2", csInterface);
		this.addSampleMembers(csInterface);
		result.add( source );

		// --- 6. cs-file
		source = CREATOR.createInterfaceSourceFile("TestInterface6WithNestedNamespaceAndInterface", NS);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		CSharpNamespaceUsage nestedNamespaceUsage = new CSharpNamespaceUsage(NS_NESTED, source.getFirstNamespaceUsage());
		csInterface = new CSharpClass("InterfaceInNestedNamespace", nestedNamespaceUsage);
		this.addSampleMembers(csInterface);
		csInterface = new CSharpClass("NestedInterface3", csInterface);
		this.addSampleMembers(csInterface);
		result.add( source );

		// --- 7. cs-file
		source = CREATOR.createInterfaceSourceFile("TestInterface7AndMultilevelNestingOfInterfaces", NS);
		this.addSampleMembers(source.getFirstNamespaceUsage().getFirstType());
		result.add( source );
		csInterface = new CSharpClass("NestedInterface1", source.getFirstNamespaceUsage().getFirstType());
		this.addSampleMembers(csInterface);
		csInterface = new CSharpClass("NestedInterface2", csInterface);
		this.addSampleMembers(csInterface);
		csInterface = new CSharpClass("NestedInterface3", csInterface);
		this.addSampleMembers(csInterface);
		csInterface = new CSharpClass("NestedInterface4", csInterface);
		this.addSampleMembers(csInterface);
		return result;
	}

	/**
	 * @param type
	 */
	protected void addSampleMembers(CSharpInterface type) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		this.addSampleMethods(type);
		this.addSampleProperties(type);
	}
}