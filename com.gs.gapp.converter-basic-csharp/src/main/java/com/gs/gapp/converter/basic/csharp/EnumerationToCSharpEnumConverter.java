/**
 *
 */
package com.gs.gapp.converter.basic.csharp;

import java.util.EnumSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.EnumerationEntry;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.CSharpSourceFile;
import com.gs.gapp.metamodel.csharp.member.CSharpEnumEntry;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;

/**
 * @author rt
 *
 *
 */
public class EnumerationToCSharpEnumConverter<S extends Enumeration, T extends CSharpEnum> extends
    ComplexTypeToCSharpTypeConverter<S, T> {

	public EnumerationToCSharpEnumConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		@SuppressWarnings("unused")
		Set<CSharpProperty> properties = this.convertFieldsToProperties(originalModelElement, resultingModelElement);
		@SuppressWarnings("unused")
		Set<CSharpEnumEntry> entries = this.convertEnumerationLiteralsToEnumEntries(originalModelElement, resultingModelElement);

		// TODO add more conversion logic here

		return;
	}

	/**
	 * @param originalModelElement
	 * @param resultingModelElement
	 * @return
	 */
	protected Set<CSharpEnumEntry> convertEnumerationLiteralsToEnumEntries(S originalModelElement, T resultingModelElement) {
		Set<CSharpEnumEntry> result = new LinkedHashSet<CSharpEnumEntry>();

		for (EnumerationEntry enumEntry : originalModelElement.getEnumerationEntries()) {
			CSharpEnumEntry entry = new CSharpEnumEntry(enumEntry.getName(), resultingModelElement);
			entry.setBody(enumEntry.getBody());
			result.add(entry);
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {

		CSharpSourceFile sourceFile = this.convertWithOtherConverter(CSharpSourceFile.class, originalModelElement);

		@SuppressWarnings("unchecked")
		T result = (T) new CSharpEnum(originalModelElement.getName(), sourceFile.getFirstNamespaceUsage());
		result.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		return result;
	}
}