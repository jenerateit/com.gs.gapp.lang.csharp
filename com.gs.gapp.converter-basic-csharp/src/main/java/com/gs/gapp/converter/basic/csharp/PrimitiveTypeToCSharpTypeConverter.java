/**
 *
 */
package com.gs.gapp.converter.basic.csharp;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.csharp.type.CSharpType;

/**
 * @author mmt
 *
 */
public class PrimitiveTypeToCSharpTypeConverter<S extends PrimitiveType, T extends CSharpType> extends
		AbstractModelElementToCSharpConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public PrimitiveTypeToCSharpTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S primitiveType, T cSharpType) {
		super.onConvert(primitiveType, cSharpType);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generallyResponsible = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		
		if (generallyResponsible) {
			PrimitiveType primitiveType = (PrimitiveType) originalModelElement;
			return getModelConverter().getMappedType(primitiveType) != null;
		}
		
		return generallyResponsible;
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S primitiveType, ModelElementI previouslyResultingElement) {

		@SuppressWarnings("unchecked")
		T result = (T) getModelConverter().getMappedType(primitiveType);

		return result;

	}
}
