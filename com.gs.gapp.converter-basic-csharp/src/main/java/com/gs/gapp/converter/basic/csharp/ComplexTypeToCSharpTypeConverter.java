/**
 * 
 */
package com.gs.gapp.converter.basic.csharp;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.NamespaceI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.CSharpType;

/**
 * This abstract class serves as the parent class for converters that create an instance of a type that inherits from CSharpTypeParameter.
 * 
 * @author mmt
 *
 *
 */
public abstract class ComplexTypeToCSharpTypeConverter<S extends ComplexType, T extends CSharpType> extends
		AbstractModelElementToCSharpConverter<S, T> {

	public ComplexTypeToCSharpTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	protected CSharpNamespace getCSharpNamespace(S originalModelElement) {
		// --- namespace
		CSharpNamespace csharpNamespace = null;
		NamespaceI namespace = getNamespace(originalModelElement);
		if (namespace != null) {
			csharpNamespace = this.convertWithOtherConverter(CSharpNamespace.class, namespace);
		} else {
			// TODO a warning should be added here
		}

		return csharpNamespace;
	}
	
	/**
	 * @param originalModelElement
	 * @return
	 */
	protected NamespaceI getNamespace(S originalModelElement) {
		return originalModelElement.getModule().getNamespace();
	}
	
	/**
	 * @param originalModelElement
	 * @param resultingModelElement
	 * @return
	 */
	protected Set<CSharpProperty> convertFieldsToProperties(S originalModelElement, T resultingModelElement) {
		Set<CSharpProperty> result = new LinkedHashSet<CSharpProperty>();
		
		for (Field field : originalModelElement.getFields()) {
			CSharpProperty property = this.convertWithOtherConverter(CSharpProperty.class, field, resultingModelElement);
			if (property != null) result.add(property);
		}
		
		return result;
	}
}