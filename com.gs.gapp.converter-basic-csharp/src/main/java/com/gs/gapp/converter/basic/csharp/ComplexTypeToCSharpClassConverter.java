/**
 * 
 */
package com.gs.gapp.converter.basic.csharp;

import java.util.EnumSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.csharp.CSharpModifier;
import com.gs.gapp.metamodel.csharp.CSharpSourceFile;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;

/**
 * @author rt
 *
 *
 */
public class ComplexTypeToCSharpClassConverter<S extends ComplexType, T extends CSharpClass> extends
		ComplexTypeToCSharpTypeConverter<S, T> {

	public ComplexTypeToCSharpClassConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);
		
		@SuppressWarnings("unused")
		Set<CSharpProperty> properties = this.convertFieldsToProperties(originalModelElement, resultingModelElement);
		
		if (originalModelElement.getParent() != null) {
			CSharpClass parent = this.convertWithOtherConverter(CSharpClass.class, originalModelElement.getParent());
			if (parent != null) {
				resultingModelElement.setBaseType(parent);
			} else {
				throw new ModelConverterException("the parent '" + originalModelElement.getParent() + "' of element '" +
			                                      originalModelElement + "' was not successfully converter to a CSharpClass", originalModelElement.getParent());
			}
		}
		// TODO add more conversion logic here
		
		return;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		
		CSharpSourceFile sourceFile = this.convertWithOtherConverter(CSharpSourceFile.class, originalModelElement);
		
		@SuppressWarnings("unchecked")
		T result = (T) new CSharpClass(originalModelElement.getName(), sourceFile.getFirstNamespaceUsage());
		result.setModifiers(EnumSet.of(CSharpModifier.PUBLIC));
		return result;
	}
}