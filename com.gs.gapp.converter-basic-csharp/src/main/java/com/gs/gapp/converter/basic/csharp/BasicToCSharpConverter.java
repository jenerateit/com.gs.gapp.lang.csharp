package com.gs.gapp.converter.basic.csharp;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.converter.basic.csharp.member.FieldToCSharpPropertyConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.csharp.CSharpModelElementCache;
import com.gs.gapp.metamodel.csharp.CSharpModelElementCreator;
import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpSourceFile;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.CSharpClass;
import com.gs.gapp.metamodel.csharp.type.CSharpEnum;
import com.gs.gapp.metamodel.csharp.type.CSharpType;
import com.gs.vd.metamodel.csharp.predef.System.Byte;
import com.gs.vd.metamodel.csharp.predef.System.Char;
import com.gs.vd.metamodel.csharp.predef.System.DateTime;
import com.gs.vd.metamodel.csharp.predef.System.Decimal;
import com.gs.vd.metamodel.csharp.predef.System.Double;
import com.gs.vd.metamodel.csharp.predef.System.Int16;
import com.gs.vd.metamodel.csharp.predef.System.Int32;
import com.gs.vd.metamodel.csharp.predef.System.Int64;
import com.gs.vd.metamodel.csharp.predef.System.SByte;
import com.gs.vd.metamodel.csharp.predef.System.Single;
import com.gs.vd.metamodel.csharp.predef.System.UInt16;
import com.gs.vd.metamodel.csharp.predef.System.UInt32;
import com.gs.vd.metamodel.csharp.predef.System.UInt64;

public class BasicToCSharpConverter extends AbstractConverter {

	private final CSharpModelElementCreator modelElementCreator = new CSharpModelElementCreator();
	/**
	 * @param modelElementCache
	 */
	public BasicToCSharpConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}

	/**
	 *
	 */
	public BasicToCSharpConverter() {
		super(new CSharpModelElementCache());
	}

	@Override
	protected List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> result =
				new ArrayList<AbstractModelElementConverter<? extends Object,? extends ModelElementI>>();

		// --- types to source file
		result.add(new ComplexTypeToCSharpSourceFileConverter<ComplexType, CSharpSourceFile>(this));
		result.add(new EnumerationToCSharpSourceFileConverter<Enumeration, CSharpSourceFile>(this));

		// --- types and namespace
		result.add(new PrimitiveTypeToCSharpTypeConverter<PrimitiveType, CSharpType>(this));
		result.add(new NamespaceToCSharpNamespaceConverter<Namespace, CSharpNamespace>(this));
		result.add(new ComplexTypeToCSharpClassConverter<ComplexType, CSharpClass>(this));
		result.add(new EnumerationToCSharpEnumConverter<Enumeration, CSharpEnum>(this));

		// --- members
		result.add(new FieldToCSharpPropertyConverter<Field, CSharpProperty>(this, true));


		return result;
	}

	/**
	 * Identifies the instance of a CSharpTypeParameter that corresponds to the given primitive type.
	 *
	 * @return
	 */
	public CSharpType getMappedType(PrimitiveType primitiveType) {

		CSharpType result = null;

		if (primitiveType != null) {
			PrimitiveTypeEnum primitiveTypeEnumEntry = PrimitiveTypeEnum.fromString(primitiveType.getName());
			if (primitiveTypeEnumEntry != null) {
				switch (primitiveTypeEnumEntry) {
				case UINT1:
					result = com.gs.vd.metamodel.csharp.predef.System.Boolean.TYPE;
					break;
				case DATE:
				case TIME:
				case DATETIME:
					result = DateTime.TYPE;
					break;

				case FLOAT16:
				case FLOAT32:
					result = Single.TYPE;
					break;

				case FLOAT64:
					result = Double.TYPE;
					break;

				case FLOAT128:
				case UINT128:
				case SINT128:
					result = Decimal.TYPE;
					break;

				case SINT8:
					result = SByte.TYPE;
					break;

				case UINT8:
					result = Byte.TYPE;
					break;

				case SINT16:
					result = Int16.TYPE;
					break;

				case UINT16:
					result = UInt16.TYPE;
					break;

				case SINT32:
					result = Int32.TYPE;
					break;

				case UINT32:
					result = UInt32.TYPE;
					break;

				case SINT64:
					result = Int64.TYPE;
					break;

				case UINT64:
					result = UInt64.TYPE;
					break;

				case CHARACTER:
					result = Char.TYPE;
					break;

				case STRING:
					result = com.gs.vd.metamodel.csharp.predef.System.String.TYPE;
					break;

			    default:
					throw new RuntimeException("unhandled primitive type found during conversion from basic model to Java model:" + primitiveType.toString());
				}
			}
		}

		return result;
	}


	/**
	 * @param namespace
	 * @return
	 */
	public CSharpNamespace getOrCreateCSharpNamespace(Namespace namespace) {
		CSharpNamespace resultingModelElement = null;

        String[] nsParts = namespace.getName().split("[.]");
        StringBuilder ns = new StringBuilder();

        for (String nsPart : nsParts) {
        	CSharpNamespace nsFromCache =
        		(CSharpNamespace) getModelElementCache().findModelElement(nsPart,
			        				                  (ns.length() == 0 ?
			        				                		  CSharpNamespace.GLOBAL_NAMESPACE_NAME : ns.toString()), CSharpNamespace.class);
			if (nsFromCache == null) {
	        	resultingModelElement = new CSharpNamespace(nsPart, resultingModelElement);
	        	addModelElement(resultingModelElement, (ns.length() == 0 ? CSharpNamespace.GLOBAL_NAMESPACE_NAME : ns.toString()) );
	        	resultingModelElement.setOriginatingElement(namespace);
        	} else {
        		resultingModelElement = nsFromCache;
        	}
        	if (ns.length() > 0) {
        		ns.append(".");
        	}
        	ns.append(nsPart);
        }

		return resultingModelElement;
	}

	/**
	 * @return the modelElementCreator
	 */
	protected CSharpModelElementCreator getModelElementCreator() {
		return modelElementCreator;
	}

}
