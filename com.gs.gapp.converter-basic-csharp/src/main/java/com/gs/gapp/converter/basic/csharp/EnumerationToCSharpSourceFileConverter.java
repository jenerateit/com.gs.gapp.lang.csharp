/**
 *
 */
package com.gs.gapp.converter.basic.csharp;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.csharp.CSharpNamespace;
import com.gs.gapp.metamodel.csharp.CSharpNamespaceUsage;
import com.gs.gapp.metamodel.csharp.CSharpSourceFile;

/**
 * @author rt
 *
 *
 */
public class EnumerationToCSharpSourceFileConverter<S extends Enumeration, T extends CSharpSourceFile> extends
		AbstractModelElementToCSharpConverter<S, T> {

	public EnumerationToCSharpSourceFileConverter(AbstractConverter modelConverter) {
		super(modelConverter, false, false, true);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		CSharpNamespace csNamespace = this.convertWithOtherConverter(CSharpNamespace.class, originalModelElement.getModule().getNamespace());

		@SuppressWarnings("unused")  // namespace usage is automatically getting added to the source file object
		CSharpNamespaceUsage namespaceUsage = new CSharpNamespaceUsage(csNamespace, resultingModelElement);

	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new CSharpSourceFile(originalModelElement.getName(), originalModelElement.getModule().getNamespace().getName().replace(".", "/"));
		return result;
	}
}