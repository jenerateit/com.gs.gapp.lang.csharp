/**
 *
 */
package com.gs.gapp.converter.basic.csharp;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.csharp.CSharpModelElementCreator;
import com.gs.gapp.metamodel.csharp.type.CSharpModelElement;

/**
 * This abstract class serves as the parent class for converters that create an instance of a type that inherits from CSharpTypeParameter.
 *
 * @author mmt
 *
 *
 */
public abstract class AbstractModelElementToCSharpConverter<S extends ModelElementI, T extends CSharpModelElement> extends
		AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AbstractModelElementToCSharpConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}



	/**
	 * @param modelConverter
	 * @param previousResultingElementRequired
	 */
	public AbstractModelElementToCSharpConverter(
			AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected BasicToCSharpConverter getModelConverter() {
		return (BasicToCSharpConverter) super.getModelConverter();
	}


	protected CSharpModelElementCreator getModelElementCreator() {
		return getModelConverter().getModelElementCreator();
	}

}