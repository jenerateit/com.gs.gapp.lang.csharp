/**
 *
 */
package com.gs.gapp.converter.basic.csharp;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.csharp.CSharpNamespace;


/**
 * @author mmt
 *
 */
public class NamespaceToCSharpNamespaceConverter<S extends Namespace, T extends CSharpNamespace> extends
    AbstractModelElementToCSharpConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public NamespaceToCSharpNamespaceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {

		/* this must not be done here, otherwise we will end in an endless recursion:
		 * ClassConverter -> SourceFileConverter -> NamespaceConverter -> ClassConverter
		Set<ComplexType> complexTypes = originalModelElement.getElements(ComplexType.class);
		if (complexTypes != null) {
			for (ComplexType complexType : complexTypes) {
				@SuppressWarnings("unused")
				CSharpTypeParameter csharpType = this.convertWithOtherConverter(CSharpTypeParameter.class, complexType);
				// --- this is not required, since the conversion for complex types takes care of the assignment to a namespace
				//resultingModelElement.addTypes(csharpType);
			}
		}
		*/
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) getModelConverter().getOrCreateCSharpNamespace(originalModelElement);
		return result;
	}
}
