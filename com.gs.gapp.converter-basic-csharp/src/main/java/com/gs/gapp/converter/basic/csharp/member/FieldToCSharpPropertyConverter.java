/**
 *
 */
package com.gs.gapp.converter.basic.csharp.member;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.basic.csharp.AbstractModelElementToCSharpConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.csharp.member.CSharpProperty;
import com.gs.gapp.metamodel.csharp.type.CSharpType;

/**
 * @author rt
 *
 *
 */
public class FieldToCSharpPropertyConverter<S extends Field, T extends CSharpProperty> extends
    AbstractModelElementToCSharpConverter<S, T> {

	public FieldToCSharpPropertyConverter(AbstractConverter modelConverter,
			boolean createAndConvertInOneGo) {
		super(modelConverter, true, true, createAndConvertInOneGo);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// TODO add more conversion logic here
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement,
			ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
			if (previousResultingModelElement == null) {
				throw new ModelConverterException("Converter '" + this + "' requires a previous resulting element. Additionally pass a pervious resulting element when you call convertWithOtherConverter(...).");
			} else {
			    result = true;
			}
		}

		return result;
	}

	/**
	 * @param originalModelElement
	 * @param type
	 * @return
	 */
	protected CSharpType getCollectionType(S originalModelElement, CSharpType type) {
		CSharpType result = null;
		if (originalModelElement.getCollectionType() != null) {
			switch (originalModelElement.getCollectionType()) {
			case LIST:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.csharp.predef.System_Collections_Generic.List.TYPE, type);
				break;
			case SET:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.csharp.predef.System_Collections_Generic.HashSet.TYPE, type);
				break;
			case SORTED_SET:
				result = getModelElementCreator().makeGenericType(com.gs.vd.metamodel.csharp.predef.System_Collections_Generic.SortedSet.TYPE, type);
				break;
			case ARRAY:
				// TODO
				break;
			default:
				throw new ModelConverterException("cannot convert basic field since it has an unknown collection type assigned", originalModelElement);
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingElement) {

        if (previousResultingElement instanceof CSharpType) {
			CSharpType owner = (CSharpType) previousResultingElement;
			CSharpType type = this.convertWithOtherConverter(CSharpType.class, originalModelElement.getType());

			if (originalModelElement.getCollectionType() != null) {
				type = getCollectionType(originalModelElement, type);
			}

			@SuppressWarnings("unchecked")
			T result = (T) new CSharpProperty(originalModelElement.getName(), type, owner);
			result.setAutoImplemented(true);
			return result;
        }

		throw new ModelConverterException("original model element cannot be converted since the previous resulting model element is not of type 'CSharpType'", originalModelElement);
	}
}